export const StatusEnum = Object.freeze({
    CREATED: 'created',
    ACCEPTED: 'accepted',
    REJECTED: 'rejected',
    COMPLETED: 'completed',
})

export const Role = Object.freeze({
    TEACHER: 'teacher',
    ADMIN: 'admin',
    STUDENT: 'student',
})

export const Provider = Object.freeze({
    FACEBOOK: 'facebook',
    GOOGLE: 'google',
})

export const ErrorCode = Object.freeze({
    INVALID_TOKEN: "invalid.or.expired.token"
})
