export const BASE_URL = process.env.NEXT_PUBLIC_APP_URL
export const TEACHER_URL = process.env.NEXT_PUBLIC_APP_URL + "/teacher"
export const STUDENT_URL = process.env.NEXT_PUBLIC_APP_URL + "/student"
export const ADMIN_URL = process.env.NEXT_PUBLIC_APP_URL + "/admin"

