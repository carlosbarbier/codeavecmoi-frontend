import {getCookie} from "../util";
import {ADMIN_URL} from "../../constants/url";

export const getQas = async () => {
    try {
        const response = await fetch(`${ADMIN_URL}/qas`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "GET",
        });
        return response.json();
    } catch (e) {
    }
};