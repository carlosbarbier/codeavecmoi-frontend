import {getCookie} from "../util";
import {ADMIN_URL} from "../../constants/url";

export const finishToReviewVideo = async (body) => {
    try {
        const response = await fetch(`${ADMIN_URL}/videos`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "POST",
            body: JSON.stringify(body),
        });
        return response.json();
    } catch (e) {
    }
};