import {getCookie} from "../util";
import {ADMIN_URL} from "../../constants/url";


export const getAllCourses = async () => {
    try {
        const courses = await fetch(`${ADMIN_URL}/courses`,{
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "GET",
        });
        return courses.json();
    } catch (e) {
    }
};

export const getCourse = async (slug) => {
    try {
        const courses = await fetch(`${ADMIN_URL}/courses/${slug}`,{
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "GET",
        });
        return courses.json();
    } catch (e) {
    }
};

export const approveCourse = async (slug) => {
    try {
        const courses = await fetch(`${ADMIN_URL}/courses/${slug}/approve`,{
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "PUT",
        });
        return courses.json();
    } catch (e) {
    }
};

export const rejectCourse = async (slug,body) => {
    try {
        const courses = await fetch(`${ADMIN_URL}/courses/${slug}/reject`,{
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "PUT",
            body: JSON.stringify(body),
        });
        return courses.json();
    } catch (e) {
    }
};