import {getCookie} from "../util";
import {ADMIN_URL} from "../../constants/url";

export const getAllTheTeachers = async () => {
    try {
        const response = await fetch(`${ADMIN_URL}/teachers`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "GET",
        });
        return response.json();
    } catch (e) {
    }
};