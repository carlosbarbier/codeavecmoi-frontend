import {getCookie} from "../util";
import {TEACHER_URL} from "../../constants/url";

export const changeVideosPositions = async (data) => {
    try {
        const response = await fetch(`${TEACHER_URL}/videos`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
                "Content-Type": "application/json",
            },
            method: "PUT",
            body: JSON.stringify(data),
        });

        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const addVideo = async (data,slug) => {
    try {
        const response = await fetch(`${TEACHER_URL}/courses/${slug}/videos`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
            body: data
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const setCourseAsCompleted = async (slug) => {
    try {
        const response = await fetch(`${TEACHER_URL}/courses/${slug}/completed`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "PUT",
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const deleteVideoById = async (id) => {
    try {
        const response = await fetch(`${TEACHER_URL}/videos/${id}`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "DELETE",
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};
