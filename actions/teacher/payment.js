import {getCookie} from "../util";
import {TEACHER_URL} from "../../constants/url";

export const getPaymentsHistory = async () => {
    try {
        const response = await fetch(`${TEACHER_URL}/payments`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "GET",
        });
        return response.json();
    } catch (e) {
    }
};