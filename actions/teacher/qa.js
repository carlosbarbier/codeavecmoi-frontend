import {getCookie} from "../util";
import {TEACHER_URL} from "../../constants/url";

export const getQuestionAnswer = async (token) => {
    try {
        const response = await fetch(`${TEACHER_URL}/qas`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
            },
            method: "GET",
        });

        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const replyToQuestion = async (body) => {
    try {
        const response = await fetch(`${TEACHER_URL}/qas`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "POST",
            body: JSON.stringify(body),
        });
        return response.json();
    } catch (e) {
    }
};



