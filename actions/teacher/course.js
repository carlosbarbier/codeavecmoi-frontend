
import {getCookie} from "../util";
import {TEACHER_URL} from "../../constants/url";


export const createNewCourse = async (data) => {
    try {
        const response = await fetch(`${TEACHER_URL}/courses`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
            body: data
        });

        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const getCourses = async (token) => {
    try {
        const response = await fetch(`${TEACHER_URL}/courses`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
            },
            method: "GET",
        });
        return await response.json();
    } catch (error) {
        console.log(error.status)
        return error;
    }
};

export const getCourse = async (slug,token,videos) => {
    try {
        const response = await fetch(`${TEACHER_URL}/courses/${slug}/?videos=${videos}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            },
            method: "GET",
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const editCourse = async (slug,data) => {
    try {
        const response = await fetch(`${TEACHER_URL}/courses/${slug}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "PUT",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};
