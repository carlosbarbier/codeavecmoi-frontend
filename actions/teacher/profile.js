import {getCookie} from "../util";
import {TEACHER_URL} from "../../constants/url";

export const updateTeacherProfile = async (data) => {
    try {
        const response = await fetch(`${TEACHER_URL}/profile`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "PUT",
            body: data,
        });
        return response.json();
    } catch (e) {
    }
};

export const teacherProfileUpdate = async (data) => {
    console.log(data)
    try {
        const response = await fetch(`${TEACHER_URL}/update/profile`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "PUT",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (e) {
    }
};