import {getCookie} from "../util";
import {TEACHER_URL} from "../../constants/url";
export const saveCard = async (body) => {
    try {
        const response = await fetch(`${TEACHER_URL}/cards`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "POST",
            body: JSON.stringify(body),
        });
        return response.json();
    } catch (e) {
    }
};