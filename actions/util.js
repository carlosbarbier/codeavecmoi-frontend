import cookie from "js-cookie";
import {notification} from "antd";

const encryptpwd = require('encrypt-with-password');

export const getFromLocalstorage = (key) => {
    if (process.browser) {
        return JSON.parse(localStorage.getItem(key));
    }
};
export const storeToLocalstorage = (key, value) => {
    if (process.browser) {
        localStorage.setItem(key, JSON.stringify(value));
    }
};
export const removeFromLocalstorage = (key) => {
    if (process.browser) {
        localStorage.removeItem(key);
    }
};


export const setCookie = (key, value) => {
    if (process.browser) {
        cookie.set(key, value, {expires: 7});
    }
};

export const removeCookie = (key) => {
    if (process.browser) {
        cookie.remove(key);
    }
};

export const encryptData = (data) => {
    return encryptpwd.encryptJSON(data, process.env.NEXT_PUBLIC_PASSWORD_ENCRYPTION);

};
export const decryptData = (data) => {
    let encryptData = null
    try {
        encryptData = encryptpwd.decryptJSON(data, process.env.NEXT_PUBLIC_PASSWORD_ENCRYPTION)
    } catch (e) {

    }
    return encryptData
};

export const getCookie = (key, req) => {
    return process.browser ? getCookieFromBrowser(key) : getCookieFromServer(key, req);
};

export const getCookieFromBrowser = key => {
    return cookie.get(key);
};

export const getCookieFromServer = (key, req) => {
    if (!req.headers.cookie) {
        return undefined;
    }

    let token = req.headers.cookie.split(';').find(c => c.trim().startsWith(`${key}=`));
    if (!token) {
        return {};
    }
    return token.split('=')[1];
};

export const checkIfAccountIsActive = (errors) => {
    let isActive = true
    for (let i = 0; i < errors.length; i++) {
        if (errors[i].rule === "invalid.or.expired.token") {
            isActive = false
            break
        }
    }
    return isActive

};

export const hasAccess = (errors) => {
    let isActive = true
    for (let i = 0; i < errors.length; i++) {
        if (errors[i].rule === "account.inactive") {
            isActive = false
            removeCookie("token");
            removeFromLocalstorage("x-id");
            break
        }
    }
    return isActive

};

export const secondsToHms = (time) => {
    time = Number(time);
    const h = Math.floor(time / 3600);
    const m = Math.floor((time % 3600) / 60);
    const mDisplay = m > 0 ? m + (m === 1 ? " min " : " mins ") : "";
    const hDisplay = h > 0 ? h + (h === 1 ? " hour " : " hours ") : "";
    return hDisplay + mDisplay;
};

export const secondsMs = (time) => {
    time = Number(time);
    const m = Math.floor((time % 3600) / 60);
    const s = Math.floor((time % 3600) % 60);
    const mDisplay = m > 0 ? m + (m === 1 ? " min " : " mins ") : "";
    return mDisplay + s;
};

export const sum = (array) => {
    return array.reduce((accumulator, currentValue) => parseFloat(accumulator) + parseFloat(currentValue), 0)
}

export const openNotificationWithIcon = (type, message) => {
    notification[type]({
        message: message,
        duration: 8,
        top: 80
    });
}
export const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export const calculateRating = (rating) => {
    let sum = 0;
    if (rating.length === 0) {
        return 0;
    } else {

        for (var i = 0; i < rating.length; i++) {
            sum += rating[i].rating;
        }
        const avg = sum / rating.length;
        return avg.toFixed(1);
    }
};

export const secondsToHmsPlayer = (time) => {
    time = Number(time);
    const h = Math.floor(time / 3600);
    const m = Math.floor((time % 3600) / 60);
    const hDisplay = h > 0 ? h + (h === 1 ? " hour, " : " hours, ") : "";
    const mDisplay = m > 0 ? m + (m === 1 ? " min, " : " mins ") : "";
    return hDisplay + mDisplay
};



