import {getCookie, removeCookie, removeFromLocalstorage} from "../util";
import {STUDENT_URL} from "../../constants/url";

export const deleteUser = async (next) => {
    try {
        await fetch(`${STUDENT_URL}/account/delete`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
        });
        removeCookie("token");
        removeFromLocalstorage("user");
        next()
    } catch (error) {
        return console.log(error);
    }
};

