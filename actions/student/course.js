import {getCookie} from "../util";
import {BASE_URL, STUDENT_URL} from "../../constants/url";
import axios from "axios";

export const getCourseToWatch = async (slug, token) => {
    return await axios.get(`${BASE_URL}/courses/${slug}/watch`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
        },
    })
}



export const enrollForFreeCourse = async (slug) => {
    try {
        const courses = await fetch(`${STUDENT_URL}/courses/${slug}/enroll`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "PUT",
        });
        return courses.json();
    } catch (e) {
    }
};

export const rateCourse = async (slug, body) => {
    try {
        const courses = await fetch(`${STUDENT_URL}/courses/${slug}/rates`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "PUT",
            body: JSON.stringify(body),
        });
        return courses.json();
    } catch (e) {
    }
};

export const postQuestion = async (slug, body) => {
    try {
        const response = await fetch(`${STUDENT_URL}/courses/${slug}/questions-answers`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "POST",
            body: JSON.stringify(body),
        });
        return response.json();
    } catch (e) {
    }
};

export const getAllQuestionsAndAnswers = async (slug) => {
    try {
        const courses = await fetch(`${STUDENT_URL}/courses/${slug}/questions-answers`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "GET",
        });
        return courses.json();
    } catch (e) {
    }
};

export const finishToWatchVideo = async (slug, video_id) => {
    try {
        const response = await fetch(`${STUDENT_URL}/courses/${slug}/videos`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie('token'),
            },
            method: "PUT",
            body: JSON.stringify(video_id),
        });
        return response.json();
    } catch (e) {
    }
};

export const markedCourseAsCompleted = async (slug) => {
    try {
        const response = await fetch(`${STUDENT_URL}/courses/${slug}/completed`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "PUT",
        });
        return await response.json();
    } catch (error) {
        return console.log(error);
    }
};

export const getAllMyCourses = async (token) => {
    return await axios.get(`${STUDENT_URL}/courses`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
        },
    })
}

