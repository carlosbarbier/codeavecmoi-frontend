import {STUDENT_URL} from "../../constants/url";
import {getCookie} from "../util";

export const chargeStudent = async (data) => {
    try {
       const charge= await fetch(`${STUDENT_URL}/charge`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
           body: JSON.stringify(data),
        });
        return charge.json();
    } catch (error) {
        return console.log(error);
    }
};
