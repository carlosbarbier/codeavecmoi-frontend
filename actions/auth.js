import {
    decryptData,
    encryptData,
    getCookie,
    getFromLocalstorage,
    removeCookie,
    removeFromLocalstorage,
    setCookie,
    storeToLocalstorage
} from "./util";
import {BASE_URL} from "../constants/url";
import axios from "axios";

export const register = async (user) => {
    try {
        const response = await fetch(`${BASE_URL}/register`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(user),
        });
        return await response.json();
    } catch (error) {

    }
};

export const registerTeacher = async (teacher) => {
    try {
        const response = await fetch(`${BASE_URL}/register/teacher`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(teacher),
        });
        return await response.json();
    } catch (error) {

    }
};

export const login = async (user) => {
    try {
        const response = await fetch(`${BASE_URL}/login`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(user),
        });
        return await response.json();
    } catch (error) {

    }
};

export const loginWithSocial = async (user, provider) => {
    try {
        const response = await fetch(`${BASE_URL}/login/${provider}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(user),
        });
        return await response.json();
    } catch (error) {

    }
};
export const logout = async (next) => {
    try {
        await fetch(`${BASE_URL}/logout`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "POST",
        });
        removeCookie("token");
        removeFromLocalstorage("x-id");
        next()

    } catch (error) {

    }
};

export const forgotPassword = async (data) => {
    console.log(data)
    try {
        const response = await fetch(`${BASE_URL}/password/forgot`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {

    }
};
export const resetPassword = async (data, token) => {
    try {
        const response = await fetch(`${BASE_URL}/auth/reset/${token}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {

    }
};

export const getUserToResetPassword = async (token) => {
    try {
        const response = await fetch(`${BASE_URL}/auth/reset/${token}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "GET",
        });
        return await response.json();
    } catch (error) {

    }
};

export const updateProfile = async (data) => {
    try {
        const response = await fetch(`${BASE_URL}/account/update-profile`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "PUT",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {

    }
};

export const verifyEmail = async (token) => {
    try {
        const response = await fetch(`${BASE_URL}/account/activate/${token}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "PUT",
        });
        return await response.json();
    } catch (error) {

    }
};

export const changePassword = async (data) => {
    try {
        const response = await fetch(`${BASE_URL}/password/change`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("token"),
            },
            method: "PUT",
            body: JSON.stringify(data),
        });
        return await response.json();
    } catch (error) {

    }
};
export const verifyUserCredentials = async (token, role) => {
    return await axios.get(`${BASE_URL}/${role}`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
        },
    })
}


export const isAuthenticate = (data, next) => {
    setCookie("token", data.token);
    console.log("USER", data)
    const encrypted = encryptData(data.user)
    storeToLocalstorage("x-id", encrypted);
    next();
};

export const getUser = () => {
    if (process.browser) {
        const cookieChecked = getCookie("token");
        if (cookieChecked) {
            if (getFromLocalstorage("x-id") != null
                && getFromLocalstorage("x-id") !== 'undefined'
                && getFromLocalstorage("x-id") !== '') {
                const data = getFromLocalstorage("x-id")
                return decryptData(data.toString())
            }
        }
    }
};

export const isLogin = () => {
    if (process.browser) {
        return getUser();
    }
};
