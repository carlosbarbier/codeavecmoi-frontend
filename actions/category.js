import {BASE_URL} from "../constants/url";
import axios from "axios";

export const getAllCategories = async () => {
    return await axios.get(`${BASE_URL}/categories`);
};


