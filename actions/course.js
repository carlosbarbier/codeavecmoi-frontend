import {BASE_URL} from "../constants/url";

const querystring = require('querystring');

// courses-home
export const getAllCourses = async (params) => {
    let queryString = querystring.stringify(params);
    if (queryString.length === 0) {
        try {
            const courses = await fetch(`${BASE_URL}/courses`);
            return courses.json();
        } catch (e) {

        }
    } else {
        try {
            const courses = await fetch(`${BASE_URL}/courses?${queryString}`);
            return courses.json();
        } catch (e) {

        }
    }
};

export const getAllCoursesForHome = async () => {
    try {
        const courses = await fetch(`${BASE_URL}/courses-home`);
        return courses.json();
    } catch (e) {

    }
};


export const getCourse = async (slug) => {
    try {
        const courses = await fetch(`${BASE_URL}/courses/${slug}`);
        return courses.json();
    } catch (e) {
    }
};
