import {BASE_URL} from "../constants/url";

export const getTeacherProfileWithCourses = async (name) => {
    try {
        const courses = await fetch(`${BASE_URL}/teacher/${name}/courses`);
        return courses.json();
    } catch (e) {
    }
};