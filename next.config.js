// next.config.js
const withAntdLess = require('next-plugin-antd-less');

module.exports = withAntdLess({
    modifyVars: {
        '@primary-color': '#233D63',
        '@success-color': '#7FD9AF',
        '@warning-color': '#faad14',
        '@error-color': '#f5222d',
    },
    lessVarsFilePathAppendToEndOfContent: false,
    cssLoaderOptions: {},
    webpack: (config) => {
        return config;
    },
});
