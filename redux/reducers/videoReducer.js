import {DELETE_VIDEO} from "../constants/video";

const videoReducer = (state = {}, action) => {
    switch (action.type) {
        case DELETE_VIDEO:
            return {
                videoId: action.payload.id
            };

        default:
            return state;
    }
};
export default videoReducer;
