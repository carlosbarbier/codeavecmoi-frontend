import {ADD_COURSE_TO_CART, CLEAR_CART, REMOVE_COURSE_FROM_CART} from "../constants/cart";
import {getFromLocalstorage} from "../../actions/util";


export const cartReducer = (state = {cart: getFromLocalstorage('cart') || []}, action) => {
    switch (action.type) {
        case ADD_COURSE_TO_CART:
            return {cart: action.payload.cart};
        case REMOVE_COURSE_FROM_CART:
            return {cart: action.payload.cart};
        case CLEAR_CART:
            return {cart: []};
        default:
            return state;
    }
};
export default cartReducer;