import {ADD_QUESTION} from "../constants/question";

const getAllQuestions = (state = {}, action) => {
    switch (action.type) {
        case ADD_QUESTION:
            return {
                questions: action.payload.questions
            };
        default:
            return state;
    }
};
export default getAllQuestions;
