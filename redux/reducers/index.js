import {combineReducers} from "redux";
import getAllQuestions from "./questionReducer";
import cartReducer from "./cartReducer";
import proceedPayment from "./paymentReducer";
import videoReducer from "./videoReducer";
import courseReducer from "./courseReducer";
import userReducer from "./userReducer";


export const rootReducer = combineReducers({
    questions: getAllQuestions,
    cart: cartReducer,
    payment: proceedPayment,
    videoId: videoReducer,
    course: courseReducer,
    user: userReducer,
});

export default rootReducer;
