import {COURSE_CREATED} from "../constants/course";

const courseReducer = (state = {}, action) => {
    switch (action.type) {
        case COURSE_CREATED:
            return {
                course: action.payload.course
            };

        default:
            return state;
    }
};
export default courseReducer;
