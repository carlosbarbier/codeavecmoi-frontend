import {PAYMENT_ERROR, PAYMENT_SUCCESS, RESET_PAYMENT} from "../constants/payment";

const proceedPayment = (state = {}, action) => {
    switch (action.type) {
        case PAYMENT_ERROR:
            return {
                status: action.payload
            };
        case PAYMENT_SUCCESS:
            return {
                status: action.payload
            };
        case RESET_PAYMENT:
            return {
                status: action.payload
            };
        default:
            return state;
    }

};
export default proceedPayment;
