import {USER_LOGIN, USER_LOGOUT} from "../constants/user";

const userReducer = (state = {}, action) => {
    switch (action.type) {
        case USER_LOGIN:
            return {
                user: action.payload.user
            };
        case USER_LOGOUT:
            return {
                user: action.payload.user
            };
        default:
            return state;
    }
};
export default userReducer;
