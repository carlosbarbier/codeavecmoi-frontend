import {PAYMENT_ERROR, PAYMENT_SUCCESS, RESET_PAYMENT} from "../constants/payment";

export const successPayment = () => {
    return {
        type: PAYMENT_SUCCESS,
        payload: "ok"
    };
};
export const errorPayment = () => {
    return {
        type: PAYMENT_ERROR,
        payload: "error"
    };
};

export const resetPayment = () => {
    return {
        type: RESET_PAYMENT,
        payload: ""
    };
};