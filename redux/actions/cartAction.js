import {ADD_COURSE_TO_CART, CLEAR_CART, REMOVE_COURSE_FROM_CART} from "../constants/cart";
import {removeFromLocalstorage, storeToLocalstorage} from "../../actions/util";


export const addCourseToCart = (item) => async (dispatch, getState) => {
    const cart = getState().cart.cart.slice();
    let alreadyExists = false;
    cart.forEach((course) => {
        if (course.slug === item.slug) {
            alreadyExists = true;
        }
    });
    if (!alreadyExists) {
        cart.push(item);
        dispatch({
            type: ADD_COURSE_TO_CART,
            payload: {cart}
        })
        storeToLocalstorage("cart", cart)
    }
}

export const removeCourseFromCart = (course) => (dispatch, getState) => {
    const cart = getState().cart.cart.slice().filter(element => element.slug !== course.slug);
    dispatch({type: REMOVE_COURSE_FROM_CART, payload: {cart}});
    storeToLocalstorage("cart", cart)
};

export const clearCart = () => (dispatch) => {
    dispatch({type: CLEAR_CART});
    removeFromLocalstorage("cart")
};
