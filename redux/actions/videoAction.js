import {DELETE_VIDEO} from "../constants/video";

export const deleteVideo = (id) => async dispatch => {
    return dispatch({
        type: DELETE_VIDEO,
        payload: {id}
    })

}