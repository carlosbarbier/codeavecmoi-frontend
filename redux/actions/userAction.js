import {USER_LOGIN, USER_LOGOUT} from "../constants/user";


export const userLogin = (user) => async dispatch => {
    return dispatch({
        type: USER_LOGIN,
        payload: {user}
    })

}

export const userLogout = async dispatch => {
    return dispatch({
        type: USER_LOGOUT,
        payload: {}
    })

}