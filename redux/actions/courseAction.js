import {COURSE_CREATED} from "../constants/course";

export const newCourse = (course) => async dispatch => {
    return dispatch({
        type: COURSE_CREATED,
        payload: {course}
    })

}