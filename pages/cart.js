import React from 'react';
import Layout from "../components/layouts/MainLayout";
import CartList from "../components/cart/CartList";

const Cart = () => {
    return (
        <Layout>
            <CartList/>
        </Layout>
    );
};

export default Cart;