import React from "react";
import Layout from "../components/layouts/MainLayout";
import RegisterComponent from "../components/auth/Register";

const Register = () => {
    return (
        <Layout>
            <RegisterComponent />
        </Layout>
    );
};

export default Register;
