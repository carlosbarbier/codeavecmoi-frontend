import React from 'react';
import AdminLayout from "../../components/layouts/AdminLayout";
import ChangePassword from "../../components/commons/ChangePassword";
import withAdminRole from "./withAdmin";


const Settings = () => {


    return (
        <AdminLayout title="Settings">
            <ChangePassword/>
        </AdminLayout>
    );
}

export default withAdminRole(Settings);