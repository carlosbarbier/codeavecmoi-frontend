import React, {useEffect, useState} from 'react';
import AdminLayout from "../../components/layouts/AdminLayout";
import {Tabs} from 'antd';
import {getPayments} from "../../actions/admin/payment";
import PaymentTable from "../../components/admin/PaymentTable";
import LoadingSkeleton from "../../components/commons/LoadingSkeleton";
import withAdminRole from "./withAdmin";

const Payments = () => {
    const {TabPane} = Tabs;
    const [payments_month, setMonthPayments] = useState([])
    const [payments_week, setWeekPayments] = useState([])
    const [payments_yesterday, setYesterdayPayments] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getPayments().then(res => {
                setMonthPayments(res.payments_month)
                setYesterdayPayments(res.payments_week)
                setWeekPayments(res.payments_month)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 1500);
        return () => window.clearTimeout(timeoutID)
    }, [])
    return (
        <AdminLayout title={"Payments"}>
            {loading && <LoadingSkeleton/>}
            {!loading &&
            <Tabs defaultActiveKey="1">
                <TabPane tab="Yesterday" key="1">
                    <PaymentTable payments={payments_yesterday}/>
                </TabPane>
                <TabPane tab="Last Week" key="2">
                    <PaymentTable payments={payments_week}/>
                </TabPane>
                <TabPane tab="Last Month" key="3">
                    <PaymentTable payments={payments_month}/>
                </TabPane>
            </Tabs>

            }

        </AdminLayout>
    );
};

export default withAdminRole(Payments);
