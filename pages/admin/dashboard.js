import React, {useEffect, useState} from 'react';
import AdminLayout from "../../components/layouts/AdminLayout";
import {getCookie} from "../../actions/util";
import {getDashboardInfo} from "../../actions/admin/dashboard";
import LoadingSkeleton from "../../components/commons/LoadingSkeleton";
import withAdminRole from "./withAdmin";

const Dashboard = () => {
    const [info, setInfo] = useState({})
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getDashboardInfo(getCookie("token")).then(res => {
                setInfo(res)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 1500);
        return () => window.clearTimeout(timeoutID)
    }, [])

    return (
        <AdminLayout title="Admin">
            {loading && <LoadingSkeleton/>}
            {!loading && <>
                <div className="row">
                    <div className="col">
                        <div className="sidebar mb-3">
                            <div className="sidebar-widget sidebar-preview">
                                <div className="preview-course-content">
                                    <div className="info-content">
                                        <h4 className="info__title mb-2">Best Selling Course</h4>
                                        <span className="info__count" style={{fontSize:"28px"}}>{ info && info.max_earning?.course.title}</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <div className="sidebar mb-3">
                            <div className="sidebar-widget sidebar-preview">
                                <div className="preview-course-content">
                                    <div className="info-content">
                                        <h4 className="info__title mb-2">Total Courses</h4>
                                        <span className="info__count">{info.total_course}</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="col">
                        <div className="sidebar mb-3">
                            <div className="sidebar-widget sidebar-preview">
                                <div className="preview-course-content">
                                    <div className="info-content">
                                        <h4 className="info__title mb-2">Total Sell</h4>
                                        <span className="info__count">{info.total_sell}</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="col">
                        <div className="sidebar mb-3">
                            <div className="sidebar-widget sidebar-preview">
                                <div className="preview-course-content">
                                    <div className="info-content">
                                        <h4 className="info__title mb-2">Total Student</h4>
                                        <span className="info__count">{info.total_student}</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <div className="sidebar mb-3">
                            <div className="sidebar-widget sidebar-preview">
                                <div className="preview-course-content">
                                    <div className="info-content">
                                        <h4 className="info__title mb-2">New Teacher</h4>
                                        <span className="info__count">{info.new_teacher}</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="col">
                        <div className="sidebar mb-3">
                            <div className="sidebar-widget sidebar-preview">
                                <div className="preview-course-content">
                                    <div className="info-content">
                                        <h4 className="info__title mb-2">New Student</h4>
                                        <span className="info__count">{info.new_student}</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="col">
                        <div className="sidebar mb-3">
                            <div className="sidebar-widget sidebar-preview">
                                <div className="preview-course-content">
                                    <div className="info-content">
                                        <h4 className="info__title mb-2">Pending Course</h4>
                                        <span className="info__count">{info.total_pending}</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </>}
        </AdminLayout>
    )
};

export default withAdminRole(Dashboard);