import React, {useEffect, useState} from 'react';
import AdminLayout from "../../components/layouts/AdminLayout";
import {getAllTheTeachers} from "../../actions/admin/teacher";
import TeacherTable from "../../components/admin/TeacherTable";
import LoadingSkeleton from "../../components/commons/LoadingSkeleton";
import withAdminRole from "./withAdmin";

const Teachers = () => {
    const [teachers, setTeachers] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getAllTheTeachers().then(res => {
                setTeachers(res)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 1500);
        return () => window.clearTimeout(timeoutID)
    }, [])
    return (
        <AdminLayout title={"All the teacher"}>
            {loading && <LoadingSkeleton/>}
            {!loading && <TeacherTable teachers={teachers}/>}
        </AdminLayout>
    );
};

export default withAdminRole(Teachers);
