import React, {useEffect, useState} from 'react';
import AdminLayout from "../../components/layouts/AdminLayout";
import {getQas} from "../../actions/admin/qa";
import QuestionTable from "../../components/admin/QuestionTable";
import LoadingSkeleton from "../../components/commons/LoadingSkeleton";
import withAdminRole from "./withAdmin";

const Questions = () => {
    const [qas, setQas] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getQas().then(res => {
                console.log(res)
                setQas(res)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 1500);
        return () => window.clearTimeout(timeoutID)
    }, [])
    return (
        <AdminLayout title={"Questions and Answer"}>
            {loading && <LoadingSkeleton/>}
            {!loading && <QuestionTable qas={qas}/>}
        </AdminLayout>
    )
}

export default withAdminRole(Questions);