import React from 'react';
import AdminLayout from "../../components/layouts/AdminLayout";
import ChangeEmail from "../../components/commons/ChangeEmail";
import withAdminRole from "./withAdmin";

const layout = {
    labelCol: {span: 4},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 4, span: 16},
};
const Email = (props) => {
    const user = props.user
    return (
        <AdminLayout title="Change Email">
            <ChangeEmail user={user}/>
        </AdminLayout>
    );
}

export default withAdminRole(Email);