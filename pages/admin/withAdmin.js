import {getCookie} from "../../actions/util";
import {verifyUserCredentials} from "../../actions/auth";
import {Role} from "../../constants";

const withAdminRole = Page => {
    const WithAdminRole = props => <Page {...props} />;
    WithAdminRole.getInitialProps = async context => {
        const token = getCookie('token', context.req);
        let user = null;

        if (token) {
            try {
                const response = await verifyUserCredentials(token, Role.ADMIN)
                user = response.data;
            } catch (error) {
                if (error.response.status === 401) {
                    user = null;
                }
            }
        }

        if (user === null) {
            context.res.writeHead(302, {
                Location: '/'
            });
            context.res.end();
        } else {
            return {
                ...(Page.getInitialProps ? await Page.getInitialProps(context) : {}),
                user,
                token
            };
        }
    };

    return WithAdminRole;
};

export default withAdminRole;
