import AdminLayout from "../../../components/layouts/AdminLayout";
import React, {useEffect, useState} from "react";
import {getAllCourses} from "../../../actions/admin/course";
import {Space, Table, Tag} from 'antd';
import {StatusEnum} from "../../../constants";
import Link from 'next/link'
import LoadingSkeleton from "../../../components/commons/LoadingSkeleton";
import withAdminRole from "../withAdmin";

const Index = () => {
    const {Column} = Table;

    const [loading, setLoading] = useState(false)
    const [courses, setCourses] = useState([])
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getAllCourses().then(res => {
                setCourses(res)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 1500);
        return () => window.clearTimeout(timeoutID)
    }, [])
    return (
        <AdminLayout title={"All courses"}>
            {loading && <LoadingSkeleton/>}

            {!loading && <Table dataSource={courses} rowKey={record => record.id}>
                <Column title="Title" dataIndex="title" key="title"/>
                <Column title="Teacher Name" dataIndex="teacher_name" key="teacher_name"/>
                <Column
                    title="Status"
                    key="status"
                    render={(record) => (
                        <Space size="middle">
                            {record.status === StatusEnum.ACCEPTED && <Tag color="#87d068">{record.status}</Tag>}
                            {record.status === StatusEnum.REJECTED && <Tag color="#f50">{record.status}</Tag>}
                            {record.status === StatusEnum.COMPLETED && <Tag color="#2db7f5">{record.status}</Tag>}
                            {record.status === StatusEnum.CREATED && <Tag color="#108ee9">{record.status}</Tag>}
                        </Space>
                    )}
                />
                <Column
                    title="Action"
                    key="action"
                    render={(record) => (
                        <Space size="middle">
                            <Link href={`/admin/courses/${record.slug}/view`}>
                                <a>View</a>
                            </Link>

                        </Space>
                    )}
                />
            </Table>
            }
        </AdminLayout>
    )
}
export default withAdminRole(Index);