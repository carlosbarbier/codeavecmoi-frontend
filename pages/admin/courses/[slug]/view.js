import React, {useEffect, useState} from 'react';
import AdminLayout from "../../../../components/layouts/AdminLayout";
import ReactPlayer from "react-player";
import {approveCourse, getCourse, rejectCourse} from "../../../../actions/admin/course";
import {Button, Form, Input, Modal,} from 'antd';
import {ExclamationCircleOutlined} from '@ant-design/icons';
import {finishToReviewVideo} from "../../../../actions/admin/videos";
import {StatusEnum} from "../../../../constants";
import { useRouter } from 'next/router'
import LoadingSkeleton from "../../../../components/commons/LoadingSkeleton";
import withAdminRole from "../../withAdmin";
const {confirm} = Modal

const ReView = () => {
    const router = useRouter()
    const { slug } = router.query
    const tailLayout = {
        wrapperCol: {offset: 2, span: 4},
    };
    const [course, setCourse] = useState([])
    const [ids, setIds] = useState([])
    const [videos, setVideos] = useState([])
    const [loading, setLoading] = useState(false)
    const [state, setState] = useState({
        url: null,
        playing: false,
        video: false,
        ended: false,
        videosAlreadyWatchId: [],
    });
    useEffect(() => {
        setLoading(true)
         getCourse(slug).then(result=>{
             setCourse(result.course)
             setIds(result.ids)
             setVideos(result.course.videos)
             setLoading(false)
             let array = [];
             result.ids.map((video_id) => {
                 array.push(video_id.id);
             });
             setState(state => ({
                 ...state, videosAlreadyWatchId: array,
                 url: result.course.videos[0].url,
                 id: result.course.videos[0].id,
             }));
         })


    }, []);

    const {url, playing, id, videosAlreadyWatchId} = state;
    const [show, setShow] = useState(false)
    const [errors, setErrors] = useState(null)
    const handleToWatch = (e) => {
        const id = e.currentTarget.dataset.id;
        const url = e.currentTarget.dataset.url;
        setState({
            ...state,
            url,
            id,
            playing: true,
        });
    };
    const handleEnded = async () => {
        const video_id = parseInt(id);
        if (!videosAlreadyWatchId.includes(video_id)) {
            showPromiseConfirm(video_id, course.id)
        }

    };
    const onFinish = (values) => {
        const {reject_description} = values
        rejectCourse(course.slug, {reject_description}).then(result => {
            if (result.errors) {
                setErrors(result.errors)
            } else {

                setShow(false)
            }
        })
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    const showPromiseConfirm = (videoId, courseId) => {
        confirm({
            title: 'Do the quality and the content of the video is fine?',
            icon: <ExclamationCircleOutlined/>,
            onOk() {
                return new Promise(async (resolve, reject) => {
                    let array = videosAlreadyWatchId;
                    array.push(videoId);
                    setState({
                        ...state,
                        videosAlreadyWatchId: array,
                    });
                    const res = await finishToReviewVideo({video_id: videoId, course_id: courseId})
                    setTimeout(res.errors !== "undefined" ? resolve : reject, 3000);

                    if (videos.length === videosAlreadyWatchId.length && course.status !== StatusEnum.ACCEPTED) {
                        await approveCourse(course.slug);
                    }
                }).catch(
                    () => console.log('Oops errors!')
                );
            },
            onCancel() {

            },
        })
    }

    const handleShow = () => {
        setShow(true)
    }

    return (
        <AdminLayout title="View Course">
            {loading && <LoadingSkeleton/>}
            {!loading && <>
                <div className="row video-wrapper_admin">
                    <div className="col-lg-9 col-sm-12">
                        <div className="player-wrapper">
                            <ReactPlayer
                                className="react-player"
                                url={url}
                                width="100%"
                                height="100%"
                                controls={true}
                                muted={false}
                                playing={playing}
                                onEnded={handleEnded}
                                onError={() => console.log("Error")}
                            />
                        </div>
                    </div>
                    <div className="col-lg-3 ">
                        <div className="sidebar-content-admin">
                            {videos && (
                                <>
                                    {videos.map((video) => (
                                        <div
                                            key={video.id}
                                            onClick={handleToWatch}
                                            data-url={video.url}
                                            data-id={video.id}
                                        >
                                            <ul className="course-list view_admin">
                                                <li
                                                    className={
                                                        parseInt(id) === video.id
                                                            ? "course-item-link video_active"
                                                            : "course-item-link"
                                                    }
                                                >
                                                    <div className="course-item-content-wrap">
                                                        <div className="course-item-content d-flex justify-content-between">
                                                            <p className="font-size-12">
                                                                       <span className="playing_icon_admin">
                                                                           <i
                                                                               className={
                                                                                   parseInt(id) === video.id
                                                                                       ? "las la-play-circle playing-icon"
                                                                                       : "las la-play-circle"
                                                                               }
                                                                           /></span> {video.title}

                                                            </p>
                                                            <div className="courser-item-meta-wrap ">
                                                                        <span className="video-completed">
                                                                            <i
                                                                                className={
                                                                                    videosAlreadyWatchId.includes(
                                                                                        video.id
                                                                                    )
                                                                                        ? "las la-check"
                                                                                        : ""
                                                                                }
                                                                            />
                                                                        </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>


                                    ))}
                                </>
                            )}
                        </div>
                    </div>

                </div>
                <div style={{height: "auto", marginTop: "15px"}}>
                    {!show &&
                    <button type="button" className="btn btn-light" onClick={handleShow}>Add course denial reason
                    </button>
                    }
                    {show && <Form
                        name="observation"
                        initialValues={{reject_description: ""}}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                    >

                        <Form.Item name="reject_description"
                                   label="Denial Reason"
                                   rules={[{required: true, message: "denial reason required"}]}
                        >
                            <Input.TextArea rows={4}/>
                        </Form.Item>

                        <Form.Item {...tailLayout}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>}

                </div>
            </>}



        </AdminLayout>
    )
}
export default withAdminRole(ReView);
