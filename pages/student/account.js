import React from 'react';
import Layout from "../../components/layouts/MainLayout";
import StudentTabs from "../../components/student/StudentTabs";
import {getAllMyCourses} from "../../actions/student/course";
import {getCookie} from "../../actions/util";


const StudentHome = ({coursesCompleted, allCourses, user}) => {

    return (
        <Layout>
            <StudentTabs courses={allCourses} courses_completed={coursesCompleted} user={user}/>
        </Layout>

    );
};


StudentHome.getInitialProps = async (ctx) => {
    const token = getCookie('token', ctx.req);
    let userData = null;
    if (token) {
        try {
            const response = await getAllMyCourses(token);
            userData = response.data
        } catch (error) {
            if (error.response.status !==200) {
                userData = null;
            }
        }
    }
    if (userData === null) {
        ctx.res.writeHead(302, {Location: '/'});
        ctx.res.end();
    } else {
        const {allCourses, coursesCompleted, email, name} = userData
        const user = {email, name}
        return {coursesCompleted, allCourses, user};
    }
};

export default StudentHome;
