import Router from "next/router";
import {Provider} from "react-redux";
import {store} from "../redux/store";
import NProgress from "nprogress";

import "../scss/boostrap.scss";
import "nprogress/nprogress.css";
import "bootstrap-icons/font/bootstrap-icons.css"
import 'quill/dist/quill.snow.css'
import '../styles/style.css'
import '../styles/responsive.css'

Router.events.on("routeChangeStart", () => {
    NProgress.start();
});
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

function MyApp({Component, pageProps}) {
    return (
        <Provider store={store}>
            <Component {...pageProps} />
        </Provider>
    );
}

export default MyApp
