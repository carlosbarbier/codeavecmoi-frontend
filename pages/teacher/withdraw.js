import React from 'react';
import TeacherLayout from "../../components/layouts/TeacherLayout";
import {Tabs} from 'antd';
import withTeacherRole from "./withTeacher";

const Withdraw = () => {
    const {TabPane} = Tabs;

    function callback(key) {
        console.log(key);
    }

    return (
        <TeacherLayout title="Revenue">
            <div className="sidebar mb-3">
                <div className="sidebar-widget sidebar-preview">
                    <div className="preview-course-content">
                        <Tabs defaultActiveKey="1" onChange={callback}>
                            <TabPane tab="Last Week" key="1">
                                <div className="row mt-5">
                                    <div className="col-lg-4 column-lmd-2-half column-md-2-full">
                                        <div
                                            className="icon-box icon-box-layout-2 bg-color-1 d-flex align-items-center">
                                            <div className="icon-element flex-shrink-0">
                                                <i className="la la-dollar"/>
                                            </div>
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Total Earnings</h4>
                                                <span className="info__count">120.20</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 column-lmd-2-half column-md-2-full">
                                        <div
                                            className="icon-box icon-box-layout-2 bg-color-1 d-flex align-items-center">
                                            <div className="icon-element flex-shrink-0">
                                                <i className="la la-dollar"/>
                                            </div>
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Balance</h4>
                                                <span className="info__count">120.20</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 column-lmd-2-half column-md-2-full">
                                        <div
                                            className="icon-box icon-box-layout-2 bg-color-6 d-flex align-items-center">
                                            <div className="icon-element flex-shrink-0">
                                                <i className="la la-dollar"/>
                                            </div>
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Refunds</h4>
                                                <span className="info__count">120.20</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane tab="Last Month" key="2">
                                Content of Tab Pane 2
                            </TabPane>
                            <TabPane tab="Last Three Month" key="3">
                                Content of Tab Pane 3
                            </TabPane>
                            <TabPane tab="Last Year" key="4">
                                Content of Tab Pane 3
                            </TabPane>
                        </Tabs>
                    </div>
                </div>
            </div>

        </TeacherLayout>
    );
}

export default withTeacherRole(Withdraw);
