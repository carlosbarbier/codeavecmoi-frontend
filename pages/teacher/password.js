import React from 'react';
import TeacherLayout from "../../components/layouts/TeacherLayout";
import ChangePassword from "../../components/commons/ChangePassword";
import withTeacherRole from "./withTeacher";

const Password = () => {
    return (
        <TeacherLayout title={"Change Password"}>
            <ChangePassword/>
        </TeacherLayout>
    );
};

export default withTeacherRole(Password);