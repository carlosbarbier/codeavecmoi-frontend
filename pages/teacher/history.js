import React, {useEffect, useState} from 'react';
import TeacherLayout from "../../components/layouts/TeacherLayout";
import withTeacherRole from "./withTeacher";

import moment from "moment";
import {getCookie} from "../../actions/util";
import {Space, Table, Tag} from 'antd';
import LoadingSkeleton from "../../components/commons/LoadingSkeleton";
import {getPaymentsHistory} from "../../actions/teacher/payment";

const {Column} = Table;

const History = () => {
    const [payments, setPayments] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getPaymentsHistory(getCookie("token")).then(res => {
                setPayments(res)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 1500);
        return () => window.clearTimeout(timeoutID)
    }, [])
    return (
        <TeacherLayout title="Purchase History">
            {loading && <LoadingSkeleton/>}
            {!loading && <div className="row">
                <div className="col-lg-12">
                    <div className="card-box-shared-body">
                        <div className="statement-table purchase-table table-responsive ">
                            <Table dataSource={payments} rowKey={record => record.id}>
                                <Column title="ID" dataIndex="id" key="id"/>
                                <Column title="Last Updated"
                                        key="created_at"
                                        render={(record) => (<span>
                                        {moment(record.created_at).fromNow()}
                                    </span>)}
                                />

                                <Column title="Course Title "
                                        key="title"
                                        render={(record) => (<span>
                                        {record.course.title}
                                    </span>)}
                                />
                                <Column
                                    title="Status"
                                    key="status"
                                    render={(record, index) => (
                                        <Space size="middle" key={index}>
                                            <Tag color="#87d068">{record.status}</Tag>
                                        </Space>
                                    )}
                                />
                                <Column title="Amount" dataIndex="amount" key="amount"/>

                            </Table>
                        </div>
                    </div>
                </div>
            </div>}
        </TeacherLayout>
    )
}


export default withTeacherRole(History);
