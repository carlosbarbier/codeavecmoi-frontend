import TeacherLayout from "../../components/layouts/TeacherLayout";
import React, {useEffect, useState} from 'react';
import {Button, Col, Form, Input, Row, Upload} from 'antd';
import {UploadOutlined} from "@ant-design/icons";
import {updateTeacherProfile} from "../../actions/teacher/profile";
import {openNotificationWithIcon} from "../../actions/util";
import withTeacherRole from "./withTeacher";
import {useRouter} from 'next/router'
import Errors from "../../components/commons/Errors";

const formItemLayout = {
    labelCol: {
        sm: {
            span: 6,
        },
    },
};

const formOccupationLayout = {
    labelCol: {
        sm: {
            span: 3,
        },
    },
};


const formTexAreLayout = {
    labelCol: {
        sm: {
            span: 3,
        },

    },
};
const tailFormItemLayout = {

    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 24,
            offset: 3,
        },
    },

};


const ProfileSetting = (initialProps) => {
    const router = useRouter()
    const initialMessage = "Upload your profile picture"
    const [fileName, setFilename] = useState(initialMessage);
    const [error, setError] = useState(false);
    const [errors, setErrors] = useState(null);
    const [loading, setLoading] = useState(false);
    const [showPictureField, setShowPictureField] = useState(true);
    const [user, setUser] = useState({})
    const [form] = Form.useForm();

    useEffect(() => {
        setUser(initialProps.user)
    }, [])

    const setFormData = (values) => {
        const formData = new FormData();
        formData.append("picture", values.picture.file);
        formData.append("about", values.about);
        formData.append("last_name", values.last_name);
        formData.append("name", values.name);
        formData.append("facebook", values.facebook);
        formData.append("instagram", values.instagram);
        formData.append("twitter", values.twitter);
        formData.append("website", values.website);
        formData.append("youtube", values.youtube);
        formData.append("occupation", values.occupation);
        return formData
    }

    const onFinish = (values) => {
        setLoading(true)
        const formData = setFormData(values)
        updateTeacherProfile(formData).then(result => {
            setLoading(false)
            if (result.errors) {
                setErrors(result.errors)
            } else {
                setFilename("Upload your profile picture")
                form.resetFields(["name", "last_name", "about", "youtube", "occupation", "facebook", "twitter", "instagram", "website", "picture"])
                openNotificationWithIcon(
                    "success",
                    `Profile updated successfully`
                )
            }
        })
    };


    const props = {
        multiple: false,
        beforeUpload: file => {
            return false
        },
        onChange: info => {
            if (info.file.type !== "image/jpeg") {
                setError(true)
            }
            setFilename(info.file.name)
            return false
        },

    };

    return (

        <TeacherLayout title="Profile Settings">
            <div className="padding-right-30px padding-left-30px ">
                <Row gutter={24}>
                    <Col span={12}>
                       <Errors errors={errors}/>
                    </Col>
                </Row>
                <Form
                    form={form}
                    name="profile"
                    onFinish={onFinish}
                    initialValues={{
                        name: "",
                        picture: "",
                        last_name: "",
                        facebook: "",
                        youtube: "",
                        about: "",
                        twitter: "",
                        instagram: "",
                        website: "",
                        occupation: "",
                    }}
                >

                    <Row gutter={24}>
                        <Col span={12}>
                            <Form.Item
                                name="name"
                                label="First Name"
                                labelAlign="right"
                                rules={[
                                    {
                                        required: true,
                                        message: "First name required",
                                        type: 'string',

                                    },
                                ]}
                                {...formItemLayout}
                            >
                                <Input/>
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                name="last_name"
                                label="Last Name"
                                rules={[
                                    {
                                        required: true,
                                        message: "Last Name required",
                                        type: 'string',
                                    },
                                ]}
                                {...formItemLayout}
                            >
                                <Input className="form-control"/>
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row gutter={24}>
                        <Col span={24}>
                            <Form.Item
                                name="occupation"
                                label="Job Title"
                                labelAlign="right"
                                rules={[
                                    {
                                        required: true,
                                        message: "job title required",
                                        type: 'string',

                                    },
                                ]}
                                {...formOccupationLayout}
                            >
                                <Input/>
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row gutter={24}>
                        <Col span={24}>
                            <Form.Item
                                name="about"
                                label="About"
                                rules={[
                                    {
                                        required: true,
                                        message: "About you field required",
                                        type: 'string',
                                    },
                                ]}
                                {...formTexAreLayout}
                            >
                                <Input.TextArea rows={8}/>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={12}>
                            <Form.Item
                                name="youtube"
                                label="Youtube"
                                {...formItemLayout}
                            >
                                <Input/>
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                name="twitter"
                                label="Twitter"
                                {...formItemLayout}
                            >
                                <Input/>
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row gutter={24}>
                        <Col span={12}>
                            <Form.Item
                                name="instagram"
                                label="Instagram"
                                {...formItemLayout}
                            >
                                <Input/>
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                name="facebook"
                                label="Facebook"
                                {...formItemLayout}
                            >
                                <Input/>
                            </Form.Item>
                        </Col>
                    </Row>


                    <Row gutter={24}>
                        <Col span={12}>
                            <Form.Item
                                name="website"
                                label="Website"
                                {...formItemLayout}
                            >
                                <Input/>
                            </Form.Item>
                        </Col>
                        {showPictureField &&

                        <Col span={12}>
                            <Form.Item
                                label="Picture"
                                {...formItemLayout}
                                name="picture"
                                valuePropName=""
                                rules={[
                                    {
                                        required: true,
                                        message: "profile picture required"
                                    },
                                ]}
                            >
                                <Upload  {...props} name="picture">
                                    <Button icon={<UploadOutlined/>}>{fileName}</Button>
                                </Upload>
                            </Form.Item>
                        </Col>
                        }


                    </Row>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit" disabled={loading}>
                            {loading
                                ? <><span className="spinner-border spinner-border-sm mx-1"
                                          role="status" aria-hidden="true"/> Saving...</>
                                : `Save`}
                        </Button>
                    </Form.Item>


                </Form>
            </div>
        </TeacherLayout>
    );
};


export default withTeacherRole(ProfileSetting)
