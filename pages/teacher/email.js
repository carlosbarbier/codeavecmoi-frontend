import React from 'react';

import TeacherLayout from "../../components/layouts/TeacherLayout";
import ChangeEmail from "../../components/commons/ChangeEmail";
import withTeacherRole from "./withTeacher";

const Email = (props) => {
    const user= props.user
    return (
        <TeacherLayout title={"Change Email"}>
            <ChangeEmail user={user}/>
        </TeacherLayout>
    );
};

export default withTeacherRole(Email);