import React, {useState} from 'react';
import TeacherLayout from "../../components/layouts/TeacherLayout";
import {Steps} from "antd";
import withTeacherRole from "./withTeacher";

const {Step} = Steps;
const HowTo = () => {
    const [current, setCurrent] = useState(0)
    const onChange = current => {
        console.log('onChange:', current);
        setCurrent(current);
    };

    const First = () => (
        <div className="sidebar mb-3">
            <div className="sidebar-widget sidebar-preview">
                <div className="preview-course-content">
                    <p>Click on Course,new course, Provide the required information. Save the data</p>
                </div>
            </div>
        </div>
    )

    const Second = () => (
        <div className="sidebar mb-3">
            <div className="sidebar-widget sidebar-preview">
                <div className="preview-course-content">
                    <p>Upload the videos by clicking on add video</p>
                </div>
            </div>
        </div>

    )

    const Third = () => (
        <div className="sidebar mb-3">
            <div className="sidebar-widget sidebar-preview">
                <div className="preview-course-content">
                    <p>Yoo are done, the content reviewer will let you know when your course is approved</p>
                </div>
            </div>
        </div>

    )

    return (
        <TeacherLayout title="How to create a course">
            <Steps direction="vertical" current={1}>
                <Step title="First step" status="finish" description=<First/> />
                <Step title="Second Step" status="finish" description=<Second/> />
                <Step title="Finally" status="finish" description=<Third/> />
            </Steps>
        </TeacherLayout>
    )
}

export default withTeacherRole(HowTo)