import React, {useEffect, useState} from 'react';
import TeacherLayout from "../../../components/layouts/TeacherLayout";
import {getCourses} from "../../../actions/teacher/course";
import AllCourses from "../../../components/teacher/course/AllCourses";
import Router from 'next/router'
import {getCookie} from "../../../actions/util";
import LoadingSkeleton from "../../../components/commons/LoadingSkeleton";
import withTeacherRole from "../withTeacher";

const Index = () => {
    const [courses, setCourses] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getCourses(getCookie("token")).then(res => {
                setCourses(res)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 1100);
        return () => window.clearTimeout(timeoutID)
    }, [])
    const handleRedirect = () => {
        Router.push("/teacher/courses/new")
    }
    return (
        <TeacherLayout title={"All the Courses"}>
            {loading
                ?
                <LoadingSkeleton/>
                :
                <>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="float-right" style={{marginRight: "30px"}}>
                                <button type="button" className="btn btn-light" onClick={handleRedirect}>
                                    Create new course
                                </button>
                            </div>
                        </div>
                    </div>
                    <AllCourses courses={courses}/>
                </>
            }
        </TeacherLayout>
    );
}

export default withTeacherRole(Index);
