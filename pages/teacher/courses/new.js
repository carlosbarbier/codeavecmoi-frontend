import React, {useEffect, useState} from 'react';
import TeacherLayout from "../../../components/layouts/TeacherLayout";
import NewCourse from "../../../components/teacher/course/NewCourse";
import {getAllCategories} from "../../../actions/category";
import withTeacherRole from "../withTeacher";

const New = () => {
    const [categories, setCategories] = useState([])
    const [error, setError] = useState(false)
    useEffect(() => {
        getAllCategories().then(res => {
            setCategories(res.data)
        }).catch(err => {
            setError(true)
        })
    }, [])
    return (
        <TeacherLayout title={"Create New Course"}>
            <NewCourse categories={categories}/>
        </TeacherLayout>
    );
};

export default  withTeacherRole(New);