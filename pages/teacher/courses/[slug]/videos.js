import React from 'react';
import TeacherLayout from "../../../../components/layouts/TeacherLayout";
import AddVideos from "../../../../components/teacher/course/video";
import {getCourse} from "../../../../actions/teacher/course";
import cookies from "next-cookies";


const Videos = ({course}) => {
    return (
        <TeacherLayout title={`${course.title}`}>
            <AddVideos course={course}/>
        </TeacherLayout>
    );
};
Videos.getInitialProps = async (ctx) => {
    const {token} = cookies(ctx);
    if (token) {
        const course = await getCourse(ctx.query.slug, token,true)
        return {course}
    } else {
        ctx.res.writeHead(302, {Location: "/"});
        ctx.res.end();
    }
}

export default Videos;