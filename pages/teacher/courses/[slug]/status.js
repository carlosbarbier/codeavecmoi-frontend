import React from 'react';
import TeacherLayout from "../../../../components/layouts/TeacherLayout";
import {Steps} from 'antd';
import cookies from "next-cookies";
import {getCourse} from "../../../../actions/teacher/course";
import { UserOutlined, SolutionOutlined, LoadingOutlined, SmileOutlined } from '@ant-design/icons';

const {Step} = Steps;

const Status = ({course}) => {
    const StatusEnum = Object.freeze({
        CREATED: 'created',
        ACCEPTED: 'accepted',
        REJECTED: 'rejected',
        COMPLETED: 'completed',
    })


    const StepWrapper = ({current, title, status1, status2, status3, status4}) => (
        <Steps current={current} progressDot={false}>
            <Step title="Course Created" status={status1}/>
            <Step title="Course Completed" status={status2}/>
            <Step title={title} status={status3}/>
            <Step title="Course Live" status={status4}/>
        </Steps>
    )

    const StepStatus = ({status}) => {
        switch (status) {
            case StatusEnum.CREATED:
                return <StepWrapper current={0} title="Approval" status1="finish" status2="wait" status3="wait"
                                    status4="wait"/>
            case StatusEnum.COMPLETED:
                return <StepWrapper current={1} title="Approval" status1="finish" status2="finish" status3="process"
                                    status4="wait"/>
            case StatusEnum.REJECTED:
                return <StepWrapper current={2} title="Rejected" status1="finish" status2="finish" status3="error"
                                    status4="wait"/>
            case StatusEnum.ACCEPTED:
                return <StepWrapper current={3} status1="finish" status2="finish" status3="finish" status4="finish"/>

        }
    }
    return (
        <TeacherLayout title="Course Status">
            <div className="sidebar mb-3">
                <div className="sidebar-widget sidebar-preview">
                    <div className="preview-course-content">
                        <StepStatus status={course.status}/>
                    </div>
                </div>

            </div>

        </TeacherLayout>
    );
}

Status.getInitialProps = async (ctx) => {
    const {token} = cookies(ctx);
    console.log(ctx.res)
    if (token) {
        const course = await getCourse(ctx.query.slug, token, false);
        return {course: course}
    } else {
        ctx.res.writeHead(302, {Location: "/login"});
        ctx.res.end();
    }

};
export default Status;
