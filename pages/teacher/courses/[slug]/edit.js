import React from 'react';
import EditCourseNew from "./../../../../components/teacher/course/EditCourseNew";
import TeacherLayout from "./../../../../components/layouts/TeacherLayout";
import cookies from "next-cookies";
import {getCourse} from "../../../../actions/teacher/course";
import {getAllCategories} from "../../../../actions/category";

const Edit = ({course, categories}) => {
    return (
        <TeacherLayout title="Edit">
            <EditCourseNew course={course} categories={categories}/>
        </TeacherLayout>
    );
}

Edit.getInitialProps = async (ctx) => {
    const {token} = cookies(ctx);
    if (token) {
        const course = await getCourse(ctx.query.slug, token, false);
        const data = await getAllCategories();
        return {course, categories:data.data}
    } else {
        ctx.res.writeHead(302, {Location: "/login"});
        ctx.res.end();
    }
};

export default Edit;
