import React from 'react';
import TeacherLayout from "../../../../components/layouts/TeacherLayout";
import Overview from "../../../../components/teacher/course/preview/Overview";
import VideoList from "../../../../components/teacher/course/preview/VideoList";
import {getCourse} from "../../../../actions/teacher/course";
import cookies from "next-cookies";

const Preview = ({course}) => {


    return (
        <TeacherLayout title="Course Preview">
             <div className="row">
                 <div className="col-md-6">
                     <Overview course={course}/>
                 </div>
                 <div className="col-md-6">
                   <VideoList videoList={course.videos}/>
                 </div>
             </div>
        </TeacherLayout>
    );
}
Preview.getInitialProps = async (ctx) => {
    const {token} = cookies(ctx);
    if (token) {
        const course = await getCourse(ctx.query.slug, token,true)
        return {course}
    } else {
        ctx.res.writeHead(302, {Location: "/login"});
        ctx.res.end();
    }
}
export default Preview;
