import TeacherLayout from "../../../components/layouts/TeacherLayout";
import {List} from "antd";
import CommentBox from "../../../components/teacher/qa/CommentBox";
import withTeacherRole from "../withTeacher";
import {useEffect, useState} from "react";
import {getQuestionAnswer} from "../../../actions/teacher/qa";
import {getCookie} from "../../../actions/util";
import LoadingSkeleton from "../../../components/commons/LoadingSkeleton";

const Comments = (props) => {
    const [qas, setQas] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getQuestionAnswer(getCookie("token")).then(res => {
                setQas(res)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 2000);
        return () => window.clearTimeout(timeoutID)
    }, [])
    const user = props.user
    return (
        <TeacherLayout title={`Comments (${qas.length})`}>
            {loading ?
                <LoadingSkeleton/>
                : <List
                    itemLayout="vertical"
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 3,
                    }}
                    dataSource={qas}
                    renderItem={item => (
                        <>
                            <li
                                key={item.title}
                            >
                                <CommentBox qa={item} user={user}/>
                            </li>
                        </>
                    )}
                />

            }
        </TeacherLayout>
    );
}

export default withTeacherRole(Comments)