import {getCookie} from "../../actions/util";
import {verifyUserCredentials} from "../../actions/auth";
import {Role} from "../../constants";

const withTeacherRole = Page => {
    const WithTeacherRole = props => <Page {...props} />;
    WithTeacherRole.getInitialProps = async context => {
        const token = getCookie('token', context.req);
        let user = null;

        if (token) {
            try {
                const response = await verifyUserCredentials(token, Role.TEACHER)
                user = response.data;
            } catch (error) {
                if (error.response.status === 401) {
                    user = null;
                }
            }
        }

        if (user === null) {
            context.res.writeHead(302, {
                Location: '/'
            });
            context.res.end();
        } else {
            return {
                ...(Page.getInitialProps ? await Page.getInitialProps(context) : {}),
                user,
                token
            };
        }
    };

    return WithTeacherRole;
};

export default withTeacherRole;
