import React from 'react';
import TeacherLayout from "../../components/layouts/TeacherLayout";
import CreditCardForm from "../../components/teacher/cart/CreditCardForm";
import withTeacherRole from "./withTeacher";

const PaymentDetails = () => {
    return (
        <TeacherLayout title="Payment Settings">
            <CreditCardForm/>
        </TeacherLayout>
    );
};
export default withTeacherRole(PaymentDetails)

