import React, {useState} from 'react';
import TeacherLayout from "../../components/layouts/TeacherLayout";
import {Button, Col, Form, Input, Row} from 'antd';
import {teacherProfileUpdate} from "../../actions/teacher/profile";
import withTeacherRole from "./withTeacher";
import {openNotificationWithIcon} from "../../actions/util";
import Errors from "../../components/commons/Errors";

const formItemLayout = {
    labelCol: {
        sm: {
            span: 6,
        },
    },
};

const formTexAreLayout = {
    labelCol: {
        sm: {
            span: 3,
        },

    },
};
const tailFormItemLayout = {

    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 24,
            offset: 3,
        },
    },

};

const Settings = (props) => {
    const [errors, setErrors] = useState(null);
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const user = props.user
    const onFinish = (values) => {
        const {last_name, about, name, facebook, instagram, twitter, website, youtube} = values
        setLoading(true)
        teacherProfileUpdate({
            last_name,
            about,
            name,
            facebook,
            instagram,
            twitter,
            website,
            youtube
        }).then(result => {
            if (result.errors) {
                setTimeout(() => {
                    setLoading(false)
                    setErrors(result.errors)
                }, 1500)

            } else {
                setTimeout(() => {
                    setLoading(false)
                    form.resetFields(["name", "last_name", "about", "youtube", "facebook", "twitter", "instagram", "website"])
                    openNotificationWithIcon(
                        "success",
                        `Profile information updated successfully`
                    )
                }, 1500)

            }
        })

    };

    return (
        <TeacherLayout title="settings">
            <div className="row">
                <div className="padding-right-30px padding-left-30px ">
                    <Row gutter={24}>
                        <Errors errors={errors}/>
                    </Row>
                    <Form
                        form={form}
                        name="update"
                        onFinish={onFinish}
                        initialValues={{
                            remember: true,
                            name: user.name,
                            last_name: user.last_name,
                            facebook: user.facebook,
                            youtube: user.youtube,
                            about: user.about,
                            twitter: user.twitter,
                            website: user.website,
                            instagram: user.instagram,
                        }}
                    >

                        <Row gutter={24}>
                            <Col span={12}>
                                <Form.Item
                                    name="name"
                                    label="First Name"
                                    labelAlign="right"
                                    rules={[
                                        {
                                            required: true,
                                            message: "First name required"
                                        },
                                    ]}
                                    {...formItemLayout}
                                >
                                    <Input/>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    name="last_name"
                                    label="Last Name"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Last Name required"
                                        },
                                    ]}
                                    {...formItemLayout}
                                >
                                    <Input className="form-control"/>
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={24}>
                            <Col span={24}>
                                <Form.Item
                                    name="about"
                                    label="About"
                                    rules={[
                                        {
                                            required: true,
                                            message: "About you field required"
                                        },
                                    ]}
                                    {...formTexAreLayout}
                                >
                                    <Input.TextArea rows={8}/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={24}>
                            <Col span={12}>
                                <Form.Item
                                    name="youtube"
                                    label="Youtube"
                                    {...formItemLayout}
                                >
                                    <Input/>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    name="twitter"
                                    label="Twitter"
                                    {...formItemLayout}
                                >
                                    <Input/>
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={24}>
                            <Col span={12}>
                                <Form.Item
                                    name="instagram"
                                    label="Instagram"
                                    {...formItemLayout}
                                >
                                    <Input/>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    name="facebook"
                                    label="Facebook"
                                    {...formItemLayout}
                                >
                                    <Input/>
                                </Form.Item>
                            </Col>
                        </Row>


                        <Row gutter={24}>
                            <Col span={12}>
                                <Form.Item
                                    name="website"
                                    label="Website"
                                    {...formItemLayout}
                                >
                                    <Input/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit" disabled={loading}>
                                {loading
                                    ? <><span className="spinner-border spinner-border-sm mx-1"
                                              role="status" aria-hidden="true"/> Saving...</>
                                    : `Save`}
                            </Button>
                        </Form.Item>


                    </Form>
                </div>
            </div>
        </TeacherLayout>
    );
}

export default withTeacherRole(Settings)