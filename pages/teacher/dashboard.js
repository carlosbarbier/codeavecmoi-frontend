import React, {useEffect, useState} from 'react';
import TeacherLayout from "../../components/layouts/TeacherLayout";
import withTeacherRole from "./withTeacher";
import {getCookie} from "../../actions/util";
import {getDashboardInfo} from "../../actions/teacher/dashboard";
import LoadingSkeleton from "../../components/commons/LoadingSkeleton";

const TeacherHome = () => {
    const [info, setInfo] = useState({})
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    useEffect(() => {
        setLoading(true)
        const timeoutID = window.setTimeout(() => {
            getDashboardInfo(getCookie("token")).then(res => {
                setInfo(res)
                setLoading(false)
            }).catch(err => {
                setError(true)
            })
        }, 1500);
        return () => window.clearTimeout(timeoutID)
    }, [])

    return (
        <TeacherLayout title="Dashboard">
            {
                loading ? <LoadingSkeleton/>
                    : <>
                        <div className="row">
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Total Courses</h4>
                                                <span className="info__count">{info.total_course}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Total Student</h4>
                                                <span className="info__count">{info.total_student}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Total Earning</h4>
                                                <span className="info__count">{info.earning}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Rating</h4>
                                                <span className="info__count">{info.rating}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">New Student</h4>
                                                <span className="info__count">{info.new_student}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Drop out</h4>
                                                <span className="info__count">0</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Pending Course</h4>
                                                <span className="info__count">{info.total_pending}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">New Questions</h4>
                                                <span className="info__count">{info.new_question}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="sidebar mb-3">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="info-content">
                                                <h4 className="info__title mb-2">Refunds</h4>
                                                <span className="info__count">0</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </>

            }


        </TeacherLayout>
    );
};

export default withTeacherRole(TeacherHome);
