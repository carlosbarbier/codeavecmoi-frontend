import React, {useRef} from 'react';
import Layout from "../components/layouts/MainLayout";
import RegisterTeacher from "../components/teacher/registration/RegisterTeacher";
import BecomeComponent from "../components/become/BecomeComponent";
import HowToComponent from "../components/become/HowToComponent";
import CounterComponent from "../components/become/CounterConponemt";
import ReasonComponent from "../components/become/ReasonComponent";


const Become = () => {
    const myRef = useRef(null)

    const executeScroll = () => myRef.current.scrollIntoView()
    return (
        <Layout>
            <BecomeComponent executeScroll={executeScroll}/>
            <ReasonComponent/>
            <HowToComponent/>
            <CounterComponent/>
            <div ref={myRef}>
                <RegisterTeacher/>
            </div>
        </Layout>
    )
}

export default Become;