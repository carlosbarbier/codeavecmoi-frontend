import React from "react";
import Layout from "../components/layouts/MainLayout";
import HeroComponent from "../components/homepage/HeroComponent";
import SearchComponent from "../components/homepage/SearchComponent";
import ExploreCourses from "../components/homepage/ExploreCourses";
import {getAllCoursesForHome} from "../actions/course";
import FeatureComponent from "../components/homepage/FeatureComponent";

const Home = ({courses,latest_courses,premium_courses}) => {
    return (
        <Layout>
            <section>
                <HeroComponent/>
                <SearchComponent courses={courses}/>
                <ExploreCourses latest_courses={latest_courses} premium_courses={premium_courses}/>
                <FeatureComponent/>
            </section>
        </Layout>
    );
}
export async function getStaticProps() {
    const {courses, premium_courses, latest_courses} = await getAllCoursesForHome()
    return {
        props: {
            courses,
            premium_courses, latest_courses
        },
        revalidate: 3600 * 7,
    }
}
export default Home