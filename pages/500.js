import React from "react";
import Layout from "../components/layouts/MainLayout";

export default function Custom500() {
    return <Layout>
        <div className="d-flex justify-content-center align-items-center" style={{height: "70vh"}}>
            <h4 className="mx-auto">There was an error.Please try again later</h4>
        </div>
    </Layout>

}
