import React, {useEffect} from 'react';
import Layout from "../../../components/layouts/MainLayout";
import {verifyEmail} from "../../../actions/auth";
import {openNotificationWithIcon} from "../../../actions/util";
import Router from 'next/router'
const Index = ({token}) => {
    useEffect(() => {
        verifyEmail(token).then(result => {
            console.log(result)
            if (result.errors) {
                openNotificationWithIcon("danger", "account active")
            } else {
                openNotificationWithIcon("success", "account active")

            }
            Router.push("/login")

        })
    }, [])
    return (
        <Layout>
            <div style={{height: "85vh"}}>

            </div>
        </Layout>
    );
};

Index.getInitialProps = async (ctx) => {
    const token = ctx.query.token
    console.log(token)
    return {
        token
    }
};

export default Index;
