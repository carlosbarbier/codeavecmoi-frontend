import React from "react";
import Layout from "../components/layouts/MainLayout";
import LoginComponent from "../components/auth/Login";

const Login = () => {
    return (
        <Layout>
            <LoginComponent/>
        </Layout>
    );
};

export default Login;
