import React, {useState} from 'react';
import Layout from "../../components/layouts/MainLayout";
import MediaQuery from "react-responsive";
import queryString from "query-string";
import CourseList from "../../components/courses/CourseList";
import Sidebar from "../../components/courses/SideBar";
import {getAllCourses} from "../../actions/course";
import {getAllCategories} from "../../actions/category";
import {useRouter} from 'next/router'
import { Pagination } from 'antd';
import Course from "../../components/courses/Course";

const Courses = ({courses, categories}) => {
    const [state, setState] = useState({
        searchTerm: "",
    });
    const [active, setActive] = useState(1)
    const {searchTerm} = state;
    const [category_ids, setCategoryId] = useState([]);
    const [level, setLevel] = useState([]);
    const [price, setPrice] = useState([]);
    const [hasSubmit, setHasSubmit] = useState(false);
    const router = useRouter()
    const queryParamsBuilder = () => {
        let payload
        active === 1 ? payload = {category: category_ids, level, price} : payload = {
            category: category_ids,
            level,
            price,
            page: active
        }
        const parsed = queryString.stringify(
            payload, {arrayFormat: "comma"}
        );
        router.push({
            pathname: "/courses",
            search:
                (category_ids.length === 0 && level.length === 0 && price.length === 0) ? "" : `${parsed}`,
        });
        setActive(1)
    }
    const handleClickCategory = (e) => {
        const id = parseInt(e.currentTarget.dataset.id);
        let array = category_ids;
        if (category_ids.includes(id)) {
            let index = category_ids.indexOf(id);
            array.splice(index, 1);
            setCategoryId(array);
        } else {
            array.push(id);
            setCategoryId(array);
        }
        queryParamsBuilder()

    };

    const handleClickPrice = (e) => {
        const value = e.currentTarget.dataset.price;
        setPrice([value])
        queryParamsBuilder()

    };
    const handleClickLevel = (e) => {
        const value = e.currentTarget.dataset.level;
        setLevel([value])
        queryParamsBuilder()
    }
    const handlePageChange = (pageNumber) => {
        setActive(pageNumber)
        const parsed = queryString.stringify(
            {page: pageNumber, category: category_ids,level,price},
            {arrayFormat: "comma"}
        );
        router.push({
            pathname: "/courses",
            search: `${parsed}`,
        });

    }

    const handleSearch = (e) => {
        setState({
            searchTerm: e.target.value,
        });
    };

    const handleSearchBtn = () => {
        const parsed = queryString.stringify({search: searchTerm});
        router.push({
            pathname: "/courses",
            search: `${parsed}`,
        });
        setHasSubmit(true)
    };

    const handleRedirectAfterSearch=()=> {
        setState({
           searchTerm: "",
        });
        router.push("/courses")
    }

    const PaginationData = (responsive) => (
        <>
            {courses && courses.data.length>0 ? courses.data.map(course => (
                <Course key={course.slug} course={course} responsive={responsive}/>
            )): <div className="alert alert-light" role="alert">
                 No data found for your search. Go back to <a className="alert-link" onClick={handleRedirectAfterSearch}> courses </a>
            </div>
            }

        </>
    )

    return (
        <Layout>
            <section className="breadcrumb-area my-courses-bread">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="breadcrumb-content my-courses-bread-content">
                                <div className="section-heading">
                                    <h2 className="section__title section_title_quizz" style={{color:"#ffffff"}}>All Available Courses</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="course_area">
                    <div className="container ">
                            <div className="row flex-column-reverse flex-lg-row">
                                <div className="col-lg-8">
                                    <MediaQuery minDeviceWidth={1224} >
                                        {courses.length}
                                        <div className="row">
                                            <div className="courses_wrapper">
                                                {PaginationData(`col-lg-12`)}
                                                <div className="pagination_container">
                                                    {courses.meta.total>5 && <Pagination defaultCurrent={1} total={courses.meta.total} pageSize={5} onChange={handlePageChange} current={active}/>}
                                                </div>
                                            </div>

                                        </div>

                                    </MediaQuery>
                                    <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024}>
                                        <div className="row">
                                            <CourseList responsive={`col-md-12`}/>
                                        </div>
                                    </MediaQuery>
                                    <MediaQuery maxDeviceWidth={767}>
                                        <div className="row">
                                            <CourseList responsive={`col-sm-12`}/>
                                        </div>
                                    </MediaQuery>
                                </div>
                                <div className="col-lg-4">
                                    <div className="sidebar">
                                        <Sidebar categories={categories}
                                                 handleClickCategory={handleClickCategory}
                                                 category_ids={category_ids}
                                                 handleClickPrice={handleClickPrice}
                                                 handleClickLevel={handleClickLevel}
                                                 handleSearch={handleSearch}
                                                 handleSearchBtn={handleSearchBtn}

                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
            </section>
        </Layout>
    );
};
Courses.getInitialProps = async (ctx) => {
    const courses = await getAllCourses(ctx.query)
    const data = await getAllCategories();
    return {
        courses: courses,
        categories:data.data
    }
};

export default Courses;

