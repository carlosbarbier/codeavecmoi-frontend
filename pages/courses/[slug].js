import React, {useState} from 'react';
import Layout from "../../components/layouts/MainLayout";
import {getCourse} from "../../actions/course";
import SideBarWidget from "../../components/commons/SideBarWidget";
import {calculateRating, secondsMs, secondsToHmsPlayer} from "../../actions/util";
import Comment from "../../components/commons/Comment";
import {addCourseToCart} from "../../redux/actions/cartAction";
import {useDispatch} from "react-redux";
import Router from "next/router"
import {enrollForFreeCourse} from "../../actions/student/course";
import Rating from "react-rating";
import moment from "moment";
import Link from "next/link"
import RatingFull from "../../components/commons/RatingFull";
import RatingEmpty from "../../components/commons/RatingEmpty";

const Slug = ({course}) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const handleBuyCourse = (e) => {
        const course = JSON.parse(e.currentTarget.dataset.course)
        dispatch(addCourseToCart(course))
        Router.push("/cart")
    }
    const handleAddToCart = (e) => {
        const course = JSON.parse(e.currentTarget.dataset.course)
        dispatch(addCourseToCart(course))
    }

    const handleEnroll = async (e) => {
        const slug = e.currentTarget.dataset.slug
        setLoading(true)
        enrollForFreeCourse(slug).then(response => {
            if (response.errors) {
                console.log(response.errors)
            } else {
                setLoading(false);
                Router.push("/student/account")
            }
        })
    }

    return (
        <>
            <Layout>
                <section className="breadcrumb-area breadcrumb-detail-area">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="breadcrumb-content breadcrumb-detail-content">
                                    <div className="section-heading">
                                        <h2 className="section__title mt-1"
                                            style={{color: "#ffffff", fontSize: "52px"}}>{course.title}</h2>
                                        <div className="mb-6 mt-4" style={{color: "#ffffff"}}>{course.description}</div>
                                    </div>
                                    <div className="breadcrumb__list mt-6 d-flex">
                                        <span style={{marginRight: "12px"}}>Created by {" "}
                                            <Link href={`/teachers/${course.teacher_name_slug}`}>
                                                <span className="teacher_name">{course.teacher_name}</span>
                                            </Link>

                                        </span>
                                        <span style={{marginRight: "12px", marginLeft:"5px"}}>
                                            {course.rating_total_sum > 0

                                                ?
                                                <>
                                                    <RatingFull initialRating={course.rating / course.rating_total_sum}/>
                                                    <span>({course.rating_total_sum})</span>
                                                </>
                                                :
                                                <>
                                                    <RatingEmpty/>
                                                </>
                                            }
                                        </span>
                                        <span
                                            style={{marginRight: "12px"}}>{course.students.length} Students enrolled</span>
                                        <span style={{marginRight: "12px"}}><i className="la la-globe"/> English</span>
                                        <span
                                            style={{marginRight: "12px"}}>Last updated {moment(course.updated_at).fromNow()}</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="course-detail">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="post-overview-card ">
                                    <h3 className="widget-title">What you'll learn?</h3>
                                    <ul className="list-items mt-3">
                                        {course.to_learn &&
                                        course.to_learn.map((learning, index) => (
                                            <li key={index}>
                                                <i className="la la-check-circle" style={{color:"#54B3C0"}}/> {learning}
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                                <div className="course-detail-content-wrap">
                                    <div className="curriculum-wrap mt-5 border-0">
                                        <div className="curriculum-header d-flex align-items-center justify-content-between my-4">
                                            <div className="curriculum-header-left">
                                                <h3 className="widget-title">Curriculum</h3>
                                            </div>
                                            <div className="curriculum-header-right">
                                            <span className="curriculum-total__text ">
                                              <strong>Total:</strong> {course.videos.length}{" "}lectures</span>
                                                <span className="curriculum-total__hours mx-2">
                                              <strong>Total hours:</strong>{" "}
                                                    {secondsToHmsPlayer(course.duration)}
                                            </span>
                                            </div>
                                        </div>
                                        <div className="curriculum-content">
                                            <div className="accordion-shared">
                                                <div className="accordion">
                                                    <div className="card">
                                                        <div className="collapse show">
                                                            <div className="card-body">
                                                                <div className="list-items">
                                                                    {course.videos &&
                                                                    course.videos.map((video) => (
                                                                        <span key={video.id} style={{fontSize: "11px"}}>
                                                                            <label
                                                                                className="d-flex align-items-center justify-content-between">
                                                                              <span>
                                                                                <i className="bi bi-play" style={{
                                                                                    color: "#008da1",
                                                                                    marginRight: "2px"
                                                                                }}/> {video.title}
                                                                                  <span
                                                                                      className="badge rounded-pill bg-light mx-2"
                                                                                      style={{
                                                                                          background: "#e2e8f0",
                                                                                          color: "#718cb7"
                                                                                      }}>Locked</span>
                                                                              </span>

                                                                                <span className="course-duration">
                                                                                     {secondsMs(video.duration)}
                                                                              </span>
                                                                            </label>
                                                                        </span>
                                                                    ))}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="comment__wrapper mt-3">
                                    <hr/>
                                    <h3 className="widget-title">Reviews and Comments</h3>
                                    {course.students && course.students.filter(student => student.rating > 0).length===0 && <p>No comments for this course</p>}
                                    {course.students && course.students
                                        .filter(student => student.rating > 0)
                                        .map((comment, index) => (
                                            <Comment key={index} comment={comment}/>
                                        ))
                                    }
                                </div>

                            </div>
                            <div className="col-lg-4 margin-top-50px">
                                <div className="sidebar">
                                    <div className="sidebar-widget sidebar-preview">
                                        <div className="preview-course-content">
                                            <div className="preview-course__price d-flex align-items-center">
                                            <div className="price-current">
                                              {course.price > 0 ? `£ ${course.price}` : "Free"}
                                            </div>
                                            </div>
                                            <div className="d-grid px-3">
                                                {course.price > 0 ?
                                                    <button className="btn btn-light btn-sm mt-2" type="button"
                                                            onClick={handleAddToCart}
                                                            data-course={JSON.stringify({
                                                                slug: course.slug,
                                                                title: course.title,
                                                                price: course.price
                                                            })}
                                                    >
                                                        Add to cart
                                                    </button> :

                                                    <button className="btn btn-light btn-sm mt-2" type="button"
                                                            onClick={handleEnroll}
                                                            data-slug={course.slug}
                                                            disabled={loading}
                                                    >
                                                        {loading
                                                            ? <><span className="spinner-border spinner-border-sm mx-1"
                                                                      role="status" aria-hidden="true"/> Enrolling...</>
                                                            : `Enroll`}
                                                    </button>
                                                }
                                                <button className="btn btn-light btn-sm mt-2" type="button"
                                                        onClick={handleBuyCourse}
                                                        data-course={JSON.stringify({
                                                            slug: course.slug,
                                                            title: course.title,
                                                            price: course.price
                                                        })}
                                                >
                                                    Buy course
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <SideBarWidget title="Course Tags">
                                        {course.tags &&
                                        course.tags.map((tag) => (
                                            <div key={tag} className="categories">
                                              <span className="badge badge-info category">
                                                {tag}
                                              </span>
                                            </div>
                                        ))}
                                    </SideBarWidget>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

Slug.getInitialProps = async ({query}) => {
    const course = await getCourse(query.slug)
    return {course};
};
export default Slug;
