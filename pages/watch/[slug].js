import React from 'react';
import {getCourseToWatch} from "../../actions/student/course";
import Layout from "../../components/layouts/MainLayout";
import VideoPlayer from "../../components/videos";
import Questions from "../../components/videos/Questions";
import {getCookie} from "../../actions/util";


const WatchVideo = ({course, ids}) => {
    return (
        <Layout>
            <VideoPlayer course={course} ids={ids}/>
            <Questions slug={course.slug} student={course.students[0] || []}/>
        </Layout>
    );
};

WatchVideo.getInitialProps = async (ctx) => {
    const token = getCookie('token', ctx.req);
    let courseData = null;
    if (token) {
        try {
            const response = await getCourseToWatch(ctx.query.slug, token)
            courseData = response.data
        } catch (error) {
            if (error.response.status !== 200) {
                courseData = null;
            }
        }
    }
    if (courseData === null) {
        ctx.res.writeHead(302, {Location: '/'});
        ctx.res.end();
    } else {
        const {course, ids} = courseData
        return {course, ids};
    }
};

export default WatchVideo;


