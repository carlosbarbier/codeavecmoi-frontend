import React from 'react';
import ForgotPasswordComponent from "../components/auth/ForgotPasswordComponent";
import Layout from "../components/layouts/MainLayout";

const ForgotPassword = () => {
    return (
        <Layout>
            <ForgotPasswordComponent/>
        </Layout>
    );
};

export default ForgotPassword;
