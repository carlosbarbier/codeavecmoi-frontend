import React from 'react';
import Layout from "../../components/layouts/MainLayout";
import {getTeacherProfileWithCourses} from "../../actions/teacher";
import Link from "next/link";
import {secondsToHmsPlayer} from "../../actions/util";
import RatingFull from "../../components/commons/RatingFull";
import RatingEmpty from "../../components/commons/RatingEmpty";

const FullName = ({profile, courses, total_student}) => {
    return (
        <Layout>
            <section className="breadcrumb-area instructor-breadcrumb-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="breadcrumb-content instructor-bread-content d-flex">
                                <div className="section-heading ">
                                    <div className="section__title ">{profile.name}{" "} {profile.last_name}</div>
                                    <div className="section__desc font-size-16 mt-3"
                                         style={{color: "#fffff"}}>{profile.occupation} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div className="container ">
                <div className="row">
                    <div className="col-lg-3">
                        <div className="team-single-img">
                            {profile.picture ?
                                <img src={profile.picture}
                                     alt="team image" className="team__img"/>
                                :
                                <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/Profile-icon.jpg"
                                     alt="team image" className="team__img"/>
                            }

                        </div>
                    </div>
                    <div className="col-lg-9">
                        <div className="team-single-wrap">
                            <div className="team-single-content ">
                                <div className="row mx-4">
                                    <div className="col-lg-6">
                                        <div className="team-single-item">
                                            <ul className="address-list">
                                                {profile.facebook ?
                                                    <li><a href={profile.facebook}><i
                                                        className="bi bi-facebook"/> Facebook</a></li>
                                                    : <li><a href="#"><i className="bi bi-facebook"/> Facebook</a></li>
                                                }
                                                {profile.website ?
                                                    <li><a href={profile.website}><i
                                                        className="la la-link"/> Website</a></li>
                                                    : <li><a href="#"><i className="la la-link"/> Website</a></li>
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="team-single-item ">
                                            <ul className="address-list">
                                                {profile.twitter ?
                                                    <li><a href={profile.twitter}><i className="bi bi-twitter"/> Twitter</a>
                                                    </li>
                                                    : <li><a href="#"><i className="bi bi-twitter"/> Twitter</a></li>
                                                }
                                                {profile.twitter ?
                                                    <li><a href={profile.twitter}><i
                                                        className="bi bi-instagram"/> Instagram</a></li>
                                                    :
                                                    <li><a href="#"><i className="bi bi-instagram"/> Instagram</a></li>
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="team-single-content text-center">
                                <div className="row justify-content-start">
                                    <div className="col-lg-3">
                                        <div className="team-single-item">
                                            <h3 className="widget-title font-size-20 mb-3 widget-title-tooltip"><i
                                                className="la la-users"/> Total Students</h3>
                                            <p className="number-count">
                                                <span className="counter">{total_student.total_student}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 column-td-half">
                                        <div className="team-single-item">
                                            <h3 className="widget-title font-size-20 mb-3 widget-title-tooltip">
                                                <i className="la la-file-video-o"/>Courses</h3>
                                            <p className="number-count">
                                                <span className="counter">{courses.length}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 column-td-half">
                                        <div className="team-single-item">
                                            <h3 className="widget-title font-size-20 mb-3 widget-title-tooltip"><i
                                                className="la la-star"/>Reviews</h3>
                                            <p className="number-count">
                                                <span
                                                    className="counter">{courses.filter(course => course.rating > 0).length}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-10">
                        <div className="sidebar-widget border-0" >
                            <div className="section-tab ">
                                <ul className="nav nav-tabs" >
                                    <li role="presentation">
                                        <a href="" role="tab" data-toggle="tab"
                                           className="theme-btn active">
                                            About me
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <div className="tab-content">
                                    <div className="pane-body">
                                        <p className="pb-3 mt-2 ">
                                            {profile.about}
                                        </p>

                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container my-5">
                <div className="row">
                    <div className="col-lg-12">
                        <h3 className="widget-title pb-3">My Courses</h3>
                        <div className="section-block"/>
                        <div className="row my-3">
                            {courses.map(course => (
                                <div className="col-md-3" key={course.slug}>
                                    <div className="card  mb-4 shadow  rounded">
                                        <div className="rounded home_course_img_wrapper">
                                            <Link href="/courses/[slug]" as={`/courses/${course.slug}`}>
                                                <a>
                                                    <img className=" home_course_img " src={course.url} alt="image"/>
                                                </a>
                                            </Link>
                                        </div>
                                        <div className="card-body _card_body ">
                                            <h6 className="course_title"> {course.title}</h6>
                                            <ul className="mb-3 d-flex justify-content-between"
                                                style={{fontSize: "13px"}}>
                                                <li className="list-inline-item"><i
                                                    className="bi bi-clock"/> {secondsToHmsPlayer(course.duration)}</li>
                                                <li className="list-inline-item"><i
                                                    className="bi bi-bar-chart"/> {course.level}</li>
                                            </ul>
                                            <div className="">
                                          <span className="rating">
                                    {course.rating_total_sum > 0

                                        ?
                                        <>
                                            <RatingFull initialRating={course.rating / course.rating_total_sum}/>
                                            <span>({course.rating_total_sum})</span>
                                        </>
                                        :
                                        <>
                                            <RatingEmpty/>
                                        </>
                                    }

                                </span>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            ))}

                        </div>
                    </div>
                </div>
            </div>

        </Layout>
    )
}

FullName.getInitialProps = async (ctx) => {
    const {courses, profile, total_student} = await getTeacherProfileWithCourses(ctx.query.name)
    return {courses, profile, total_student}

}
export default FullName;