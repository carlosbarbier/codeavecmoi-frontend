import React, {useEffect, useState} from 'react';
import arrayMove from "array-move";
import Input from "../../../commons/Input";
import SortableList from "./sortable/SortableList";
import {addVideo, changeVideosPositions, setCourseAsCompleted} from "../../../../actions/teacher/video";


import {Button, Switch, Upload} from 'antd';
import {InboxOutlined} from '@ant-design/icons';
import {useSelector} from "react-redux";
import Errors from "../../../commons/Errors";
import {openNotificationWithIcon} from "../../../../actions/util";
import {StatusEnum} from "../../../../constants";

const AddVideos = ({course}) => {
    const {Dragger} = Upload;
    const [fileName, setFilename] = useState("Choose File");
    const [state, setState] = useState({
        errors: null,
        title: "",
        loading: false,
        fileList: null,
        videos: course.videos || []
    });

    const {videoId} = useSelector(state => state.videoId)

    useEffect(() => {
        const arr = videos.filter(video => video.id !== videoId)
        setState(state => ({...state, videos: arr}));
    }, [videoId]);
    const {errors, title, loading, videos, fileList} = state;

    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value});
    };

    const onSortEnd = async ({oldIndex, newIndex}) => {
        if (oldIndex !== newIndex) {
            const newData = arrayMove([].concat(videos), oldIndex, newIndex).filter(el => !!el);
            newData.map((item, index) => {
                item.position = index + 1

            })
            setState({...state, videos: newData});
            await changeVideosPositions({positions: newData})
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("video", fileList);
        formData.append("title", title);
        setState({...state, loading: true});
        const slug = course.slug
        addVideo(formData, slug).then((data) => {
            if (data.errors) {
                setState({...state, errors: data.errors, lading: false});
            } else {
                setState({...state, title: "", lading: false, videos: [...videos, data]});
                setFilename("Choose File")
            }
        });
    };
    const handleLastVideo = (checked) => {
        setCourseAsCompleted(course.slug).then(res => {
            if (res.errors) {
                openNotificationWithIcon(
                    "error",
                    "Oops something went wrong please try later",
                )
            } else {
                openNotificationWithIcon(
                    "success",
                    "Course marked as completed, waiting for approval to be live",
                )
            }
        })
    }

    const props = {
        multiple: false,
        beforeUpload: file => {
            setState({...state, fileList: file})
            setFilename(file.name);
            return false;
        },
    };

    return (
        <>
            <div className="row">
                <div className="col-lg-12 col-sm-12 py-3">
                    <Errors errors={errors}/>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-6 col-sm-6">
                    <div className="sidebar mb-3">
                        <div className="sidebar-widget sidebar-preview">
                            <div className="preview-course-content">
                                <form onSubmit={handleSubmit}>
                                    <div className="input-box mb-2 ">
                                        <Input
                                            type="text"
                                            label="Videos Title"
                                            name="title"
                                            value={title}
                                            required={true}
                                            onchange={handleChange("title")}
                                        />
                                    </div>
                                    <Dragger {...props} >
                                        <p className="ant-upload-drag-icon">
                                            <InboxOutlined/>
                                        </p>
                                        <p className="ant-upload-text">{fileName}</p>

                                    </Dragger>

                                    <Button type="primary mt-3" htmlType="submit" disabled={loading}>
                                        {loading
                                            ? <><span className="spinner-border spinner-border-sm mx-1"
                                                      role="status" aria-hidden="true"/> Saving...</>
                                            : `Save`}
                                    </Button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-sm-6">

                    <div className="sidebar mb-3">
                        <div className="sidebar-widget sidebar-preview">
                            <div className="preview-course-content">
                                <p className=" d-flex align-items-center preview_course_title">
                                    Last Video?
                                </p>
                                <Switch checkedChildren="Yes" unCheckedChildren="No"
                                        onChange={handleLastVideo} disabled={course.status === StatusEnum.COMPLETED}/>
                            </div>
                        </div>
                    </div>
                    <div className="sidebar mb-3">
                        <div className="sidebar-widget sidebar-preview">
                            <div className="preview-course-content">
                                {videos.length > 0 ? <>
                                    <p className=" d-flex align-items-center preview_course_title">
                                        Videos
                                    </p>
                                    <SortableList videos={videos} onSortEnd={onSortEnd}/>
                                </> : <div>No videos uploaded</div>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AddVideos;
