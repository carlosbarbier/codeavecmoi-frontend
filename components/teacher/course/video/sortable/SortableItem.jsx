import React, {useState} from 'react';
import {SortableElement} from "react-sortable-hoc";
import {Card, Dropdown, Menu, Modal} from "antd";
import {ExclamationCircleOutlined} from '@ant-design/icons';
import ReactPlayer from "react-player";
import {useDispatch} from "react-redux";
import {deleteVideoById} from "../../../../../actions/teacher/video";
import {deleteVideo} from "../../../../../redux/actions/videoAction";

const SortableItem = SortableElement(({id, position, title, url}) => {
        const dispatch = useDispatch();
        const {confirm} = Modal;
        const [visible, setModalVisible] = useState(false)
        const [videoUrl, setVideoUrl] = useState(null)

        const showDeleteConfirm = (e) => {
            confirm({
                title: 'Are you sure you want to delete this Video?',
                icon: <ExclamationCircleOutlined/>,
                okText: 'Yes',
                okType: 'danger',
                cancelText: 'No',
                async onOk() {
                    const videoId = parseInt(e.target.getAttribute("data-id"))
                    const video = await deleteVideoById(videoId)
                    if(video.errors){
                        return
                    }
                    dispatch(deleteVideo(videoId))
                    return new Promise((resolve, reject) => {
                        setTimeout(video.errors === "undefined"
                            ? resolve :

                            reject,
                            5000);
                    }).catch(() => console.log('Oops errors!'));
                },
                onCancel() {
                    console.log('Cancel');
                },
            });
        }

        const handleOk = (e) => {
            console.log('URL', e.target.getAttribute("data-url"));
        }

        const handleCancel = (e) => {
            setModalVisible(false)
        }



        const showVideo = (e) => {
            const url = e.target.getAttribute("data-url")
            setVideoUrl(url)
            setModalVisible(true)
        }

        const menu = (
            <Menu>
                <Menu.Item key="1"><span onClick={showVideo} data-url={url}> View </span></Menu.Item>
                <Menu.Item key="2"> <span onClick={showDeleteConfirm} data-id={id}>Delete</span></Menu.Item>
            </Menu>
        );
        return (
            <>
                <Modal
                    bodyStyle={{padding: "5px",background:"#233d63"}}
                    visible={visible}
                    width={660}
                    centered
                    destroyOnClose={true}
                    closable={false}
                    footer={null}
                    onOk={handleOk}
                    onCancel={handleCancel}
                >
                    <ReactPlayer
                        height="auto"
                        style={{marginLeft:"3px"}}
                        onEnded={()=>{setModalVisible(false)}}
                        playing={true}
                        url={videoUrl}
                    />
                </Modal>
                <Card
                    data-id={id}
                    data-position={position}
                    className={` _ant-card-body`}
                    style={{marginBottom:"4px"}}
                >
                    <div className="d-inline float-start " style={{fontSize: "15px", cursor: "move"}}>
                        <i className="bi bi-list"/></div>
                    <div className="d-inline mx-2" style={{fontSize:"12px"}}>{title}</div>
                    <div className="d-inline  float-end  ">
                        <Dropdown overlay={menu} placement="bottomCenter" arrow>
                            <label style={{fontSize: "15px", cursor: "pointer"}}>...</label>
                        </Dropdown>
                    </div>

                </Card>
            </>


        )
    }
)


export default SortableItem;
