import React, {useEffect, useState} from 'react';
import arrayMove from 'array-move';
import SortableList from '../video/sortable/SortableList';
import {changeVideosPositions} from "../../../../actions/teacher/video";
import {useSelector} from "react-redux";


const VideoList = ({videoList}) => {

    const [state, setState] = useState({
        errors: null,
        title: "",
        loading: false,
        videos: videoList || []
    });
    const { videos} = state;
    const {videoId} = useSelector(state => state.videoId)

    useEffect(() => {
        const arr = videos.filter(video => video.id !== videoId)
        setState(state => ({...state, videos: arr}));
    }, [videoId]);

    const onSortEnd = async ({oldIndex, newIndex}) => {
        if (oldIndex !== newIndex) {
            const newData = arrayMove([].concat(videos), oldIndex, newIndex).filter(el => !!el);
            newData.map((item,index)=>{
               item.position=index+1

            })
            setState({...state, videos: newData});
            await changeVideosPositions({positions: newData})
        }
    };


    return (

        <div className="sidebar mb-3">
            <div className="sidebar-widget sidebar-preview">
                <div className="preview-course-content">
                    <p className=" d-flex align-items-center preview_course_title">
                        Videos
                    </p>
                    <SortableList videos={videos} onSortEnd={onSortEnd}/>
                </div>
            </div>

        </div>

    );
};

export default VideoList;
