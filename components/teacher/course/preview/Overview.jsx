import React from 'react';
import {Collapse} from "antd";

const {Panel} = Collapse;

const Overview = ({course}) => {
    return (
        <>
            <div className="sidebar mb-3">
                <div className="sidebar-widget sidebar-preview">
                    <div className="preview-course-content"><p
                        className=" d-flex align-items-center preview_course_title">
                        {course.title}
                    </p>
                    </div>
                </div>

            </div>
            <div className="sidebar mb-3">
                <div className="sidebar-widget sidebar-preview">
                    <div className="preview-course-content">
                        <Collapse defaultActiveKey={['2']} ghost>
                            <Panel header="Description" key="2">
                                <p className="collapse_para">  {course.description}</p>
                            </Panel>
                            <Panel header="Price" key="3">
                                <p className="collapse_para">  {course.price}</p>
                            </Panel>
                            <Panel header="Level" key="4">
                                <p className="collapse_para">  {course.level}</p>
                            </Panel>
                            <Panel header="Category" key="5">
                                <p className="collapse_para">  {course.category.name}</p>
                            </Panel>
                        </Collapse>
                    </div>
                </div>

            </div>

        </>
    );
};

export default Overview;
