import React, {useState} from 'react';
import {Button, Form, notification, Steps} from "antd";
import BasicInfo from "../steps/BasicInfo";
import ThingToLearn from "../steps/ThingToLearn";
import DetailsEdit from "../steps/DetailsEdit";
import {editCourse} from "../../../actions/teacher/course";
import Router from 'next/router'
import {openNotificationWithIcon} from "../../../actions/util";

const {Step} = Steps;
const steps = [
    {title: 'Basic Info'},
    {title: 'Details '},
    {title: 'Thing to learn '},
    {title: "Finish Editing"}];
const layout = {
    labelCol: {
        span: 4,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 8,
    },
};

const EditCourseNew = ({categories, course}) => {

    const [current, setCurrent] = useState(0);
    const [state, setState] = useState({
        item: null,
        tags: course.tags,
        inputVisible: false,
        tag: "",
        category: "",
        category_id: course.category_id,
        errors: null,

    });
    const [loading, setLoading] = useState(false);

    const [form] = Form.useForm();

    const {item, tags, inputVisible, tag, category_id, errors,} = state
    const next = () => {
        if (item !== null) {
            setState({...state, item: null})
        }
        setCurrent(current + 1);
    };

    const prev = () => {
        if (item !== null) {
            setState({...state, item: null})
        }
        setCurrent(current - 1);
    };

    const {title, description, price, learning_edit, level} = form.getFieldsValue()

    const mapLearning = () => {
        const arr = []
        learning_edit.map((item) => {
            arr.push(item.item)
        })
        return arr
    }
    const onFinish = async (values) => {
        setLoading(true)
        const result = await editCourse(course.slug, {
            level,
            price,
            title,
            to_learn: mapLearning().toString(),
            tags: tags.toString(),
            category_id: category_id.toString(),
            description
        })
        if (result.errors) {
            setLoading(false)
            setState({...state, errors: result.errors})
        } else {
            setTimeout(async () => {
                setLoading(false)

                openNotificationWithIcon(
                    "success",
                    `${title} has been updated successfully`
                )
                await Router.push(`/teacher/courses`)
            }, 4000)

        }
    };

    const handleChange = (e) => {
        setState({...state, [e.target.name]: e.target.value});
    };

    const handleClose = removedTag => {
        setState({...state, tags: [...tags.filter(tag => tag !== removedTag)]});
    };

    const showInput = () => {
        setState({...state, inputVisible: true});
    };


    const handleInputConfirm = () => {
        if (tag && tags.indexOf(tag) === -1) {
            setState({
                ...state,
                inputVisible: false,
                inputValue: '',
                tags: [...tags, tag]
            })

        }
    };

    const transformLearningArray = () => {
        const arr = []
        course.to_learn && course.to_learn.map((item) => {
            arr.push({item})
        })
        return arr
    }


    const onClickCategory = (e) => {
        setState({...state, category_id: e.currentTarget.dataset.id})
    }

    return (
        <div className="mt-3">
            <Steps current={current}>
                {steps.map(item => (
                    <Step key={item.title} title={item.title}/>
                ))}
            </Steps>
            <div className="steps-content">

                <Form
                    initialValues={{
                        title: course.title,
                        description: course.description,
                        price: course.price,
                        learning_edit: transformLearningArray(),
                        level: course.level
                    }}
                    {...layout}
                    form={form}
                    name="edit"
                    onFinish={onFinish}

                >

                    <div className={` ${current === 0 ? "step" : "hidden"}`}>
                        <BasicInfo/>
                    </div>

                    <div className={` ${current === 1 ? "step" : "hidden"}`}>
                        <DetailsEdit
                            handleChange={handleChange}
                            tags={tags}
                            handleClose={handleClose}
                            handleInputConfirm={handleInputConfirm}
                            inputVisible={inputVisible}
                            showInput={showInput}
                            inputValue={tag}
                            categories={categories}
                            id={category_id}
                            onClickCategory={onClickCategory}
                        />
                    </div>

                    <div className={` ${current === 2 ? "step" : "hidden"}`}>
                        <ThingToLearn tailLayout={tailLayout} id="learning_edit"/>
                    </div>

                    <div className={` ${current === 3 ? "step" : "hidden"}`}>

                        <div className="d-flex justify-content-center">
                            {(errors || []).map((error, index) => (
                                <p key={index} className="ant-form-item-extra">
                                    {error.message}
                                </p>
                            ))}

                        </div>
                        <>
                            <div className="padding-top-150px">
                                <Form.Item
                                    wrapperCol={{
                                        span: 12,
                                        offset: 11,
                                    }}
                                >
                                    <Button type="primary" htmlType="submit" disabled={loading} style={{marginTop:"96px"}}>
                                        {loading
                                            ? <><span className="spinner-border spinner-border-sm mx-1"
                                                      role="status" aria-hidden="true"/> Editing...</>
                                            : `Edit`}
                                    </Button>
                                </Form.Item>
                            </div>

                        </>
                    </div>

                </Form>
            </div>
            <div className="steps-action">
                {current > 0 && (
                    <Button style={{margin: '0 8px'}} onClick={() => prev()}>
                        Previous
                    </Button>
                )}
                {current < steps.length - 1 && (
                    <Button type="primary" disabled={current === steps.length - 1} onClick={() => next()}>
                        Next
                    </Button>
                )}

            </div>
        </div>
    );
}
export default EditCourseNew;