import React, {useState} from 'react';
import Link from "next/link";
import moment from "moment";
import { Table, Tag, Space } from 'antd';
import {StatusEnum} from "../../../constants";

const AllCourses = ({courses}) => {

    const { Column} = Table;
    return (

        <div className="row">
            <div className="col-lg-12">
                <div className="card-box-shared-body">
                    <div className="statement-table purchase-table table-responsive ">
                        <Table dataSource={courses} rowKey={record => record.id}>
                            <Column title="Title" dataIndex="title" key="title" />
                            <Column title="Price" dataIndex="price" key="price" />
                            <Column title="Last Updated"
                                    key="updated_at"
                                    render={(record)=>(<span>
                                        {moment(record.updated_at).fromNow()}
                                    </span>)}
                            />
                            <Column
                                title="Status"
                                key="status"
                                render={(record,index) => (
                                    <Space size="middle" key={index} >
                                        {record.status ===StatusEnum.ACCEPTED &&  <Tag color="#87d068">{record.status}</Tag> }
                                        {record.status ===StatusEnum.REJECTED &&  <Tag color="#f50">{record.status}</Tag> }
                                        {record.status ===StatusEnum.COMPLETED &&  <Tag color="#2db7f5">{record.status}</Tag> }
                                        {record.status ===StatusEnum.CREATED &&  <Tag color="#108ee9">{record.status}</Tag> }
                                    </Space>
                                )}
                            />
                            <Column
                                title="Action"
                                key="action"
                                render={(record ,index) => (
                                    <Space size="middle" key={index}>
                                        <Link href="/teacher/courses/[slug]/preview"
                                              as={`/teacher/courses/${record.slug}/preview`}>
                                            <a>View</a>
                                        </Link>
                                        <Link href="/teacher/courses/[slug]/edit"
                                              as={`/teacher/courses/${record.slug}/edit`}>
                                            <a>Edit</a>
                                        </Link>
                                        <Link href="/teacher/courses/[slug]/videos"
                                              as={`/teacher/courses/${record.slug}/videos`}>
                                            <a>Add Videos</a>
                                        </Link>
                                        <Link href="/teacher/courses/[slug]/status"
                                              as={`/teacher/courses/${record.slug}/status`}>
                                            <a>status</a>
                                        </Link>
                                    </Space>
                                )}
                            />
                        </Table>
                    </div>
                </div>
            </div>
        </div>


    )

}

export default AllCourses;
