import React, {useState} from 'react'
import Input from "../../commons/Input";
import TagsInput from 'react-tagsinput'
import {editCourse} from "../../../actions/teacher/course";
import cogoToast from "cogo-toast";
import Router from "next/router";
import LoadingButton from "../../commons/LoadingButton";

const EditCourse = ({categories, course}) => {
        const levels = ["All Levels", "Intermediary", "Beginner", "Advanced"]
        const [state, setState] = useState({
            title: course.title,
            level: course.level,
            description: course.description,
            category_id: course.category_id,
            price: course.price,
            errors: null,
        });
        const [loading,setLoading]=useState(false)

        const [tags, setTags] = useState(course.tags);
        const {category_id, title, description, price, errors, level} = state;
        const handleChange = (name) => (e) => {
            setState({...state, [name]: e.target.value});
        };

        const handleTagsInput = (tags) => {
            setTags( [...new Set(tags)])
        };

        const handleSubmit = async (e) => {
            e.preventDefault()
            setLoading(true)
            const result = await editCourse(course.slug, {
                level,
                price,
                title,
                tags: tags.toString(),
                category_id: category_id.toString(),
                description
            })
            if (result.errors) {
                setLoading(false)
                setState({...state, errors: result.errors})
            } else {
                setTimeout(async () => {
                    setState({...state, title: "", level: "All Levels", description: "", errors: null, price: ""});
                    setTags([])
                    cogoToast.success(`The course has been updated successfully`, {hideAfter: 6})
                    setLoading(false)
                    await Router.push(`/teacher/courses`)
                }, 4000)

            }
        }

        return (
            <div style={{margin: "0 30px"}}>
                <div className="row">
                    <div className="col-lg-12 col-sm-12">
                        {(errors || []).map((error, index) => (
                            <p key={index} style={{color: "red"}}>
                                {error.message}
                            </p>
                        ))}
                    </div>
                </div>
                <form onSubmit={handleSubmit}>
                    <div className="row my-2">
                        <div className="col-lg-12 col-sm-12">
                            <Input
                                type="text"
                                label="Title"
                                required={true}
                                name="name"
                                value={title}
                                onchange={handleChange("title")}
                            />
                        </div>

                    </div>
                    <div className="row mb-3">
                        <div className="col-lg-12 col-sm-12">
                            <label className="mr-sm-2 label-text">
                                Tags<span className="primary-color-2">*</span>
                            </label>
                            <TagsInput onChange={handleTagsInput} value={tags}/>
                        </div>
                    </div>
                    <div className="row my-2">
                        <div className="col-lg-4 col-sm-4">
                            <label className="mr-sm-2 label-text">
                                Level<span className="primary-color-2 ml-1">*</span>
                            </label>
                            <select
                                className="form-select"
                                name="level"
                                onChange={handleChange("level")}
                                defaultValue={course.level}
                            >
                                {levels &&
                                levels.map((level, index) => (
                                    <option key={index} value={level}>
                                        {level}
                                    </option>
                                ))}
                            </select>

                        </div>
                        <div className="col-lg-6 col-sm-6">

                            <label className="mr-sm-2 label-text">
                                Categories<span className="primary-color-2 ml-1">*</span>
                            </label>
                            <select
                                className="form-select "
                                name="category_id"
                                onChange={handleChange("category_id")}
                                defaultValue={course.category_id}
                            >
                                {categories &&
                                categories.map((category) => (
                                    <option key={category.id} value={category.id}>
                                        {category.name}
                                    </option>
                                ))}
                            </select>

                        </div>
                        <div className="col-lg-2 col-sm-2 ">
                            <Input
                                type="text"
                                label="Price"
                                name="price"
                                value={price}
                                onchange={handleChange("price")}
                            />
                        </div>
                        <div className="col-lg-12 my-2">
                            <div className="input-box">
                                <label className="label-text">
                                    Description
                                    <span className="primary-color-2 ml-1">*</span>
                                </label>
                                <div className="form-group">
                                  <textarea
                                      className="message-control form-control"
                                      name="description"
                                      value={description}
                                      onChange={handleChange("description")}
                                  />
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-12 col-sm-12 my-2"/>
                        <div className="col-lg-12" style={{marginBottom: "15px"}}>
                            <LoadingButton loading={loading} message={`Edit course`} loading_message={`Editing course`}/>
                        </div>
                    </div>
                </form>
            </div>

        );
    }
;

export default EditCourse;
