import {Button, Form, Steps,} from 'antd';
import React, {useState} from "react";
import BasicInfo from "../../../components/teacher/steps/BasicInfo";
import Details from "../../../components/teacher/steps/Details";
import Image from "../../../components/teacher/steps/Image";
import ThingToLearn from "../../../components/teacher/steps/ThingToLearn";
import Summary from "../../../components/teacher/steps/Summary";
import {createNewCourse} from "../../../actions/teacher/course";
import {useDispatch} from "react-redux";
import {newCourse} from "../../../redux/actions/courseAction";
import {openNotificationWithIcon} from "../../../actions/util";
import { useRouter } from 'next/router'
import Errors from "../../commons/Errors";

const {Step} = Steps;
const steps = [
    {title: 'Basic Info'},
    {title: 'Details '},
    {title: 'Thing to learn '},
    {title: 'Upload Image'},
    {title: "Finish"}];
const layout = {
    labelCol: {
        span: 4,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 8,
    },
};

const NewCourse = ({categories}) => {
    const [current, setCurrent] = useState(0);
    const dispatch = useDispatch()
    const [state, setState] = useState({
        item: null,
        tags: [],
        items: [],
        inputVisible: false,
        tag: "",
        category: "",
        category_id: "",
        isChecked: false,
        errors: null,
    });
    const router = useRouter()
    const [fileName, setFileName] = useState(null);
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const {item, tags, inputVisible, tag, category_id, errors,} = state
    const next = () => {
        if (item !== null) {
            setState({...state, item: null})
        }
        setCurrent(current + 1);
    };

    const prev = () => {
        if (item !== null) {
            setState({...state, item: null})
        }
        setCurrent(current - 1);
    };
    const {title, description, price, learning, image, level} = form.getFieldsValue()
    const mapLearning = () => {
        const arr = []
        learning.map((item) => {
            arr.push(item.item)
        })
        return arr
    }
    const onFinish = (values) => {
        setLoading(true)
        const formData = new FormData();
        formData.append("image", image.file);
        formData.append("level", level);
        formData.append("tags", tags.toString());
        formData.append('category_id', category_id);
        formData.append('description', description);
        formData.append('title', title);
        formData.append('price', price);
        formData.append('to_learn', mapLearning().toString());
        createNewCourse(formData).then(result => {
            if (result.errors) {
                setState({...state, errors: result.errors})
                setTimeout(function () {
                    setLoading(false)
                    setState({...state, errors: null});
                }, 3000,)

            } else {
                setTimeout(function () {
                    setLoading(false)
                    dispatch(newCourse({title, description, price}))
                    openNotificationWithIcon(
                        "success",
                        `${title} has been created successfully`
                    )
                    form.resetFields(["title", "description", "price", "learning","to_learn"])
                    setState({...state,tags: [],items: []})
                    router.back()
                }, 3000,)
            }
        })
    };

    const handleChange = (e) => {
        setState({...state, [e.target.name]: e.target.value, errors: null});
    };

    const handleClose = removedTag => {
        setState({...state, tags: [...tags.filter(tag => tag !== removedTag)]});
    };

    const showInput = () => {
        setState({...state, inputVisible: true});
    };


    const handleInputConfirm = () => {
        if (tag && tags.indexOf(tag) === -1) {
            setState({
                ...state,
                inputVisible: false,
                inputValue: '',
                tags: [...tags, tag]
            })

        }
    };

    const onClickCategory = (e) => {
        setState({...state, category_id: e.currentTarget.dataset.id})
    }
    const onChange = () => {
        setState({...state, errors: null})
    }

    const imageProps = {
        multiple: false,
        beforeUpload: file => {
            return false
        },
        onChange: info => {
            if (info.file.type !== "image/jpeg") {

            }
            setFileName(info.file.name)
            return false
        },

    };

    return (
        <div className="mt-3">
            <Steps current={current}>
                {steps.map((item, index) => (
                    <Step key={index} title={item.title}/>
                ))}
            </Steps>


            <div className="steps-content">

                <Form
                    {...layout}
                    form={form}
                    name="create"
                    onFinish={onFinish}
                    initialValues={{
                        title: "",
                        description: "",
                        price: "",
                        about: "",
                        learning: null,
                        image: null,
                        level: ''
                    }}
                >

                    <div className={` ${current === 0 ? "step" : "hidden"}`}>
                        <BasicInfo isToEdit={false}/>
                    </div>

                    <div className={` ${current === 1 ? "step" : "hidden"}`}>
                        <Details
                            handleChange={handleChange}
                            tags={tags}
                            handleClose={handleClose}
                            handleInputConfirm={handleInputConfirm}
                            inputVisible={inputVisible}
                            showInput={showInput}
                            inputValue={tag}
                            categories={categories}
                            id={category_id}
                            onClickCategory={onClickCategory}
                        />
                    </div>


                    <div className={` ${current === 2 ? "step" : "hidden"}`}>
                        <ThingToLearn tailLayout={tailLayout} id="learning"/>
                    </div>


                    <div className={` ${current === 3 ? "step" : "hidden"}`}>
                        <Image fileName={fileName} imageProps={imageProps}/>
                    </div>


                    <div className={` ${current === 4 ? "step" : "hidden"}`}>
                       <Errors errors={errors}/>
                        <>
                            <Summary
                                title={title}
                                description={description}
                                price={price}
                                tags={tags}
                                items={learning || []}
                                category={categories.filter(category => category.id === parseInt(category_id))}/>
                            <Form.Item
                                wrapperCol={{
                                    span: 12,
                                    offset: 11,
                                }}
                            >
                                <Button type="primary" htmlType="submit" disabled={loading}>
                                    {loading
                                        ? <><span className="spinner-border spinner-border-sm mx-1"
                                                  role="status" aria-hidden="true"/> Saving...</>
                                        : `Save`}
                                </Button>
                            </Form.Item>
                        </>


                    </div>

                </Form>
            </div>
            <div className="steps-action">
                {current > 0 && (
                    <Button style={{margin: '0 8px'}} onClick={() => prev()}>
                        Previous
                    </Button>
                )}
                {current < steps.length - 1 && (
                    <Button type="primary" disabled={current === steps.length - 1} onClick={() => next()}>
                        Next
                    </Button>
                )}

            </div>
        </div>
    );
};

export default NewCourse