import React, {useState} from 'react';
import AllCourses from "./AllCourses";
import NewCourse from "./NewCourse";


const TeacherCourse = ({courses}) => {
    const [show, setShow] = useState(false)
    const handleShow = () => {
        setShow(!show)
    }

    return (
        <>
            <div className="row">
                <div className="col-lg-12">
                    <div className="float-right" style={{marginRight: "30px"}}>
                        <button type="button" className="btn btn-light" onClick={handleShow}>
                            {show ? "All courses" : "Create new course"}
                        </button>
                    </div>
                </div>
            </div>
             <AllCourses courses={courses}/>

        </>
    );
};

export default TeacherCourse;
