import React, {useState} from 'react';
import Input from "../../commons/Input";
import {registerTeacher} from "../../../actions/auth";
import Notification from "../../commons/Notification";
import {notification} from "antd";
import Router from "next/router";
import Errors from "../../commons/Errors";
import {openNotificationWithIcon} from "../../../actions/util";

const RegisterTeacher = () => {
    const [state, setState] = useState({
        name: "",
        password: "",
        last_name: "",
        email: "",
        errors: null,
    });
    const [loading,setLoading]=useState(false)
    const {name, password, email, errors, last_name} = state;



    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true)
        registerTeacher({name, password, last_name, email}).then(teacher => {
            if(teacher.errors){
                setTimeout(() => {
                    setLoading(false);
                    setState({...state,errors: teacher.errors})
                }, 1500);

            }else{
                setTimeout(() => {
                    setLoading(false);
                    openNotificationWithIcon(
                        "success",
                        `Please verify your email to verify you account`
                    )
                    setState({name: "", email: "", password: "", last_name: "",errors: null});
                }, 1500);


            }
        })

    };
    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value});
    };

    return (

        <div className="row">
            <div className="col-lg-6 mx-auto">
                <div className="card-box-shared-body section-bg margin-bottom-40px">
                    <div className="contact-form-action">
                        <form onSubmit={handleSubmit}>
                            <div className="row">
                                <div className="col-lg-12">
                                  <Errors errors={errors}/>
                                </div>
                                <div className="col-lg-12">
                                    <Input
                                        label="First Name"
                                        type="text"
                                        name="name"
                                        onchange={handleChange("name")}
                                        value={name}
                                    />
                                </div>

                                <div className="col-lg-12">
                                    <Input
                                        label="Last Name"
                                        type="text"
                                        name="last_name"
                                        onchange={handleChange("last_name")}
                                        value={last_name}
                                    />
                                </div>
                                <div className="col-lg-12">
                                    <Input
                                        label="Email"
                                        type="email"
                                        name="email"
                                        onchange={handleChange("email")}
                                        value={email}
                                    />
                                </div>
                                <div className="col-lg-12">
                                    <Input
                                        label="Password"
                                        type="password"
                                        name="password"
                                        onchange={handleChange("password")}
                                        value={password}
                                    />
                                </div>
                                <div className="col-lg-12" style={{marginTop: "15px"}}>
                                    <button className="theme-btn" type="submit" disabled={loading}>
                                        {loading
                                            ? <><span className="spinner-border spinner-border-sm mx-1"
                                                      role="status" aria-hidden="true"/> Registering...</>
                                            : `Register`}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default RegisterTeacher;