import React from 'react';

import {Form, Input, Tag} from "antd";
import {TweenOneGroup} from 'rc-tween-one';
import {PlusOutlined} from '@ant-design/icons';

const {CheckableTag} = Tag;
const DetailsEdit = ({
                     handleChange,
                     handleClose,
                     inputVisible,
                     showInput,
                     handleInputConfirm,
                     tags,
                     tag,
                     categories,
                     onClickCategory,
                     id
                 }) => {
    const forMap = tag => {
        const tagElem = (
            <Tag
                closable
                onClose={e => {
                    e.preventDefault();
                    handleClose(tag);
                }}
                style={{marginBottom: "8px", minWidth: "75px"}}
            >
                {tag}
            </Tag>
        );
        return (
            <span key={tag} style={{display: 'inline-block'}}>
             {tagElem}
           </span>
        );
    };

    const tagChild = tags.map(forMap);
    return (
        <div className="row" style={{marginTop: "10%", marginBottom: "10%"}}>
            <div className="col-md-10 offset-md-1">
                <div className="row">
                    <div className="col">

                            <span style={{marginBottom: "16px"}}>
                                <TweenOneGroup
                                    enter={{
                                        scale: 0.8,
                                        opacity: 0,
                                        type: 'from',
                                        duration: 100,
                                        onComplete: e => {
                                            e.target.style = '';
                                        },
                                    }}
                                    leave={{opacity: 0, width: 0, scale: 0, duration: 200}}
                                    appear={false}
                                >
                                    {tagChild}
                                </TweenOneGroup>
                            </span>
                        {inputVisible && (
                            <Input
                                type="text"
                                size="small"
                                name="tag"
                                value={tag}
                                style={{minWidth: "80px"}}
                                onChange={handleChange}
                                onBlur={handleInputConfirm}
                                onPressEnter={handleInputConfirm}
                            />
                        )}
                        {!inputVisible && (
                            <Tag onClick={showInput} className="site-tag-plus my-3">
                                <PlusOutlined/> Add course tag
                            </Tag>
                        )}
                    </div>
                    <div className="col-6">
                        <div className="row">
                            <div className="col-3">
                                <span style={{marginRight: 8}}>Categories:</span>
                            </div>
                            <div className="col">
                                <span>
                                    {categories.map(category => (
                                        <CheckableTag
                                            key={category.slug}
                                            checked={parseInt(id) === category.id}
                                            className="default"
                                            onClick={onClickCategory}
                                            data-id={category.id}
                                            style={{marginBottom: "8px", background: "#eeeeee"}}
                                        >
                                            {category.name}
                                        </CheckableTag>
                                    ))}
                                </span>

                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <Form.Item
                            label="Price"
                            name="price"
                        >
                            <Input name="price" onChange={handleChange} style={{marginLeft: "5px"}}/>
                        </Form.Item>
                    </div>
                </div>
            </div>

        </div>

    );
};

export default DetailsEdit;