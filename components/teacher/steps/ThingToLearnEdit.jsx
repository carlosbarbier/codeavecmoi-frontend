import React from 'react';
import {Button, Form, Input, Space} from "antd";
import {MinusCircleOutlined, PlusOutlined} from "@ant-design/icons";

const ThingToLearnEdit = ({tailLayout,id}) => {
    return (
        <div  style={{marginTop: "3%", marginBottom: "5%"}}>
            <Form.List name={id} >
                {(fields, {add, remove}) => (
                    <div className="">
                        {fields.map((field,index)=> (

                            <Space key={field.key}
                                   style={{display: 'flex', justifyContent:"center", paddingInline: "12px"}}
                                   align="baseline">
                                <Form.Item
                                    style={{width:"50vw"}}
                                    {...field}
                                    label="Thing to learn"
                                    name={[field.name, "item"]}
                                    fieldKey={[field.fieldKey, "item"]}

                                    rules={[{ required: true, message: 'Missing items' }]}
                                >
                                    <Input  />
                                </Form.Item>
                                <MinusCircleOutlined onClick={() => remove(field.name)} />
                            </Space>
                        ))}
                        <Form.Item {...tailLayout}>
                            <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                Add Item
                            </Button>
                        </Form.Item>
                    </div>
                )}
            </Form.List>
        </div>
    );
};

export default ThingToLearn;