import React from 'react';
import {Form, Upload} from "antd";
import {InboxOutlined} from "@ant-design/icons";

const Image = ({fileName,imageProps}) => {
    return (
        <div className="row" style={{marginTop:"12%",marginBottom:"10%"}}>

                <Form.Item name="image"  label="Image"
                           rules={[
                               {
                                   required: true,
                                   message: "image required"
                               },
                           ]}
                           valuePropName=""
                >
                    <Upload.Dragger name="files" {...imageProps} >
                        <p className="ant-upload-drag-icon">
                            <InboxOutlined/>
                        </p>
                        <p className="ant-upload-text">
                            {fileName ? fileName : " Click or drag file to this area to upload"}
                        </p>
                    </Upload.Dragger>
                </Form.Item>

        </div>
    );
};

export default Image;