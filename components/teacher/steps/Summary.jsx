import React from 'react';
import {Descriptions, Tag} from "antd";

const Summary = ({title, description, price, tags, items, category}) => {

    const displayTags = () => (
        tags.map(tag => (
            <Tag key={tag}>{tag}</Tag>
        ))
    )
    return (
        <div style={{paddingInline: "33px"}}>
            <Descriptions title="Course Summary">
                <Descriptions.Item label="Title">{title === "" ? "no title provided" : title}</Descriptions.Item>
            </Descriptions>
            <Descriptions>
                <Descriptions.Item
                    label="Tags">{tags && tags.length > 0 ? displayTags() : "no tags provided"}</Descriptions.Item>
            </Descriptions>
            <Descriptions>
                <Descriptions.Item label="Price">{price === "" ? "no price provided" : price}</Descriptions.Item>
            </Descriptions>
            <Descriptions>
                <Descriptions.Item
                    label="Category">{category && category.length === 0 ? "no category provided" : category[0].name}</Descriptions.Item>
            </Descriptions>
            <Descriptions>
                <Descriptions.Item
                    label="Description">{description === "" ? "no description provided" : description}</Descriptions.Item>
            </Descriptions>
            <Descriptions>
                <Descriptions.Item style={{flexDirection: "row"}} label="Things to learn">{items && items.length > 0
                    ? items.map((item,index) => (<div key={index}><Tag style={{marginBottom: "8px"}} >{item.item}</Tag> <br/></div>))
                    : "no description provided"}
                </Descriptions.Item>
            </Descriptions>

        </div>

    );
};

export default Summary;