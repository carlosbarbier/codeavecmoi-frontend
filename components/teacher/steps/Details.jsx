import React from 'react';

import {Form, Input, Tag} from "antd";
import {TweenOneGroup} from 'rc-tween-one';
import {PlusOutlined} from '@ant-design/icons';

const {CheckableTag} = Tag;
const Details = ({
                     handleChange,
                     handleClose,
                     inputVisible,
                     showInput,
                     handleInputConfirm,
                     tags,
                     tag,
                     categories,
                     onClickCategory,
                     id,
                 }) => {
    const formItemLayout = {
        labelCol: {
            sm: {
                span: 6,
            },
        },
    };

    return (
        <div className="row" style={{marginTop: "10%", marginBottom: "10%"}}>
            <div className="col-md-10 offset-md-1">
                <div className="row">
                    <div className="col">
                            <span style={{marginBottom: "16px"}}>
                                <TweenOneGroup
                                    enter={{
                                        scale: 0.8,
                                        opacity: 0,
                                        type: 'from',
                                        duration: 100,
                                        onComplete: e => {
                                            e.target.style = '';
                                        },
                                    }}
                                    leave={{opacity: 0, width: 0, scale: 0, duration: 200}}
                                    appear={false}
                                >
                                    {tags && tags.map((tag, index) => (
                                        <span style={{display: 'inline-block'}} key={index}>
                                              <Tag closable onClose={e => {
                                                  e.preventDefault();
                                                  handleClose(tag);
                                              }}
                                                   key={tag}
                                                   style={{marginBottom: "8px", minWidth: "75px"}}
                                              >
                                                {tag}
                                            </Tag>
                                        </span>
                                    ))}
                                </TweenOneGroup>
                            </span>
                        {inputVisible && (
                            <Input
                                type="text"
                                size="small"
                                name="tag"
                                value={tag}
                                style={{minWidth: "80px"}}
                                onChange={handleChange}
                                onBlur={handleInputConfirm}
                                onPressEnter={handleInputConfirm}
                            />
                        )}
                        {!inputVisible && (
                            <Tag onClick={showInput} className="site-tag-plus my-3">
                                <PlusOutlined/> Add course tag
                            </Tag>
                        )}
                    </div>
                    <div className="col-6">

                        <Form.Item
                            label="Categories"
                            {...formItemLayout}
                            name="category"
                        >
                        <span>
                                    {categories.map(category => (
                                            <CheckableTag
                                                key={category.slug}
                                                checked={parseInt(id) === category.id}
                                                className="default"
                                                onClick={onClickCategory}
                                                data-id={category.id}
                                                style={{marginBottom: "8px", background: "#eeeeee"}}
                                            >
                                                {category.name}
                                            </CheckableTag>
                                    ))}
                                </span>
                        </Form.Item>
                    </div>
                    <div className="col">
                        <Form.Item
                            label="Price"
                            name="price"
                            {...formItemLayout}
                            rules={[
                                {
                                    required: true,
                                    message: "price required"
                                },
                            ]}
                        >
                            <Input name="price" style={{marginLeft: "5px"}}/>
                        </Form.Item>
                    </div>
                </div>
            </div>

        </div>

    );
};

export default Details;