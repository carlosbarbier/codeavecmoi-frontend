import React from "react"
import {Form, Input} from "antd";


React.useLayoutEffect = React.useEffect

const BasicInfo = () => {
        const levels = ["All Levels", "Intermediary", "Beginner", "Advanced"]
        return (
            <>
                <div style={{marginTop: "2%", marginBottom: "4%"}}>
                    <Form.Item
                        label="Title"
                        name="title"
                        rules={[
                            {
                                required: true,
                                message: "title required"
                            },
                        ]}
                    ><Input/>

                    </Form.Item>
                    <Form.Item
                        name="description"
                        label="Description"
                        rules={[
                            {
                                required: true,
                                message: "description required"
                            },
                        ]}
                    >
                        <Input.TextArea rows={9}/>
                    </Form.Item>
                    <Form.Item label="Level" name="level">
                        <select name="level" className="form-select">
                            {levels.map((level, index) => (
                                <option key={index} value={level}>
                                    {level}
                                </option>
                            ))}
                        </select>
                    </Form.Item>

                </div>
            </>

        );
    }
;

export default BasicInfo;