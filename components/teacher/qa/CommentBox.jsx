import React, {useEffect, useState} from 'react';
import {Button, Form, Input, Tooltip} from "antd";
import moment from "moment";
import {replyToQuestion} from "../../../actions/teacher/qa";
import {CheckCircleOutlined} from "@ant-design/icons";
import {getUser} from "../../../actions/auth";
import { Avatar } from 'antd';
import Errors from "../../commons/Errors";
const CommentBox = ({qa,user}) => {
    const {replies} = qa
    const [state, setState] = useState({
        body: "",
        isReply: false
    })
    useEffect(() => {
        for (let i = 0; i < replies.length; i++) {
            if (replies[i].user.id === user.id) {
                setState({
                    ...state,
                    isReply: true

                })
                break
            }
        }
    }, [])
    const [show, setShow] = useState(false)
    const [id, setCommentId] = useState(null)
    const [loading, setLoading] = useState(false)
    const [success, setSuccess] = useState(false)
    const [errors, setErrors] = useState(null)

    const onFinish = async () => {
        setLoading(true)
        await replyToQuestion({course_id: qa.course_id, parent_id: id, body: state.body}).then(result => {
            if (result.errors) {
                setTimeout(() => {
                    setLoading(false)
                    setErrors(result.errors)
                }, 1500)

            } else {
                setTimeout(() => {
                    setLoading(false);
                    setState({body: ""})
                    setCommentId(null)
                    setSuccess(true)
                }, 1500)

            }
        })
    };


    const handleReply = (e) => {
        const id = e.currentTarget.dataset.id
        setCommentId(parseInt(id))
        setShow(true)

    }

    const handleOnchange = (e) => {
        setState({...state, body: e.target.value})
    }

    return (
        <div className="row d-flex justify-content-center mb-3">
            <div className="col-md-10">
                <Errors errors={errors}/>
                <div className="comment_box card p-1 px-3">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="user d-flex flex-row align-items-center">
                            <Avatar size={40} style={{ backgroundColor: "#49AEBC" }}>{qa.user.name.charAt(0).toUpperCase()}</Avatar>
                            <small className="font-weight-bold  mx-2">{qa.user.name}</small>

                        </div>
                        <small>{moment(qa.created_at).fromNow()}</small>
                    </div>
                    <div className="comment_body" style={{marginLeft:"47px"}}>
                        {qa.body}
                    </div>
                    {id !== qa.id &&
                    <div className="action d-flex justify-content-between py-2 align-items-center">
                        <div className="reply" onClick={handleReply} data-id={qa.id} style={{marginLeft:"47px"}}><small>Reply</small></div>
                        <div className="icons align-items-center">
                            {state.isReply || success
                                ? <Tooltip placement="top" title="Replied"> <CheckCircleOutlined className="replied"
                                                                                                 style={{fontSize: "16px"}}/></Tooltip>
                                : <Tooltip placement="top" title="Not Replied"><CheckCircleOutlined/> </Tooltip>
                            }
                        </div>
                    </div>
                    }

                    {show && id === qa.id && <div className="reply_comment_form mt-3">
                        <Form
                            name="basic"
                            onFinish={onFinish}
                        >
                            <Form.Item style={{marginBottom: "13px"}}>
                                <Input.TextArea rows={4} name="body" onChange={handleOnchange} value={state.body}/>
                            </Form.Item>

                            <Form.Item>
                                <Button htmlType="submit" disabled={loading} className="antd_btn">
                                    {loading
                                        ? <><span className="spinner-border spinner-border-sm mx-1"
                                                  role="status" aria-hidden="true"/> Replying...</>
                                        : `Reply`}
                                </Button>
                            </Form.Item>
                        </Form>

                    </div>}
                </div>
            </div>
        </div>
    );
};

export default CommentBox;
