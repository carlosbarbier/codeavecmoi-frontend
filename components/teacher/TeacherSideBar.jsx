import React from 'react';
import Link from "next/link";
import {logout} from "../../actions/auth";
import Router from "next/router";
const TeacherSideBar = () => {
    return (
        <div className="card-header">
            <div className="side-menu-wrap">
                <ul className="side-menu-ul">
                    <li className="item">
                        <Link href="/teacher/dashboard">
                            <a className="admin-item">
                                <i className="la la-dashboard"/>Dashboard
                            </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/teacher/how-to">
                            <a className="admin-item">
                                <i className="bi bi-patch-question"/>How to
                            </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/teacher/courses">
                            <a className="admin-item">
                                <i className="bi bi-book"/>Courses
                            </a>
                        </Link>
                    </li>

                    <li className="item">
                        <Link href="/teacher/comments">
                            <a className="admin-item">
                                <i className="bi bi-chat-left"/>Comments
                            </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/teacher/history" >
                            <a className="admin-item">
                                <i className="bi bi-clipboard"/>Purchase History
                            </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/teacher/payments" >
                        <a className="admin-item">
                            <i className="la la-shopping-cart"/>Payment Settings
                        </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/teacher/settings" >
                            <a className="admin-item">
                                <i className="bi bi-person-x"/>Update Profile
                            </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/teacher/password" >
                            <a className="admin-item">
                                <i className="bi bi-key"/>Change Password
                            </a>
                        </Link>
                    </li>

                    <li className="item">
                        <Link href="/teacher/email" >
                            <a className="admin-item">
                                <i className="bi bi-envelope"/>Change Email
                            </a>
                        </Link>
                    </li>
                    <Link href="/teacher/profile" >
                    <li className="item">
                        <a className="admin-item">
                            <i className="la la-cog"/>Profile
                        </a>
                    </li>
                    </Link>


                    <li className="item">
                        <a className="admin-item" onClick={() =>
                            logout(() => Router.replace("/login"))
                        }>
                            <i className="la la-power-off"/>Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default TeacherSideBar;
