import React, {Component} from 'react';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import {saveCard} from "../../../actions/teacher/cart";
import {notification} from 'antd';
import {openNotificationWithIcon} from "../../../actions/util";

class CreditCardForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cvc: '',
            focus: '',
            expiry: '',
            name: '',
            number: '',
            errors: null,
            loading: false,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleInputFocus = (e) => {
        this.setState({focus: e.target.name});
    }

    handleInputChange = (e) => {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    closeLoaderIn5Seconds = () =>
        setTimeout(() => {
            this.setState({cvc: '', expiry: '', focus: '', name: '', number: '', loading: false})
            notification.success(({
                message: 'Payment Information',
                description: "Card Information Save Successfully"
            }))
        }, 5000);

    handleSubmit = async (e) => {
        e.preventDefault()
        this.setState({...this.state, loading: true})
        await saveCard({
            cvc: this.state.cvc,
            name: this.state.name,
            expiry: this.state.expiry,
            number: this.state.number
        }).then(result => {
            if (result.errors) {
                setTimeout(() => {
                    this.setState({...this.state,loading: false})
                    openNotificationWithIcon(
                        "error",
                        `We encounter a problem with the card`
                    )
                }, 2000);

            } else {
                setTimeout(() => {
                    this.setState({cvc: '', expiry: '', focus: '', name: '', number: '', loading: false})
                    openNotificationWithIcon(
                        "success",
                        `Card Information Save Successfully`
                    )
                }, 2000);

            }
        })
    }

    render() {
        return (
            <div className="row">
                <div className="col-lg-9 mx-auto">
                    <div className="card card-body  cart_item">
                        <div className="my-4">
                            <Cards
                                cvc={this.state.cvc}
                                expiry={this.state.expiry}
                                focused={this.state.focus}
                                name={this.state.name}
                                number={this.state.number}
                            />
                        </div>

                        <form onSubmit={this.handleSubmit} className="mb-4 mt-4">
                            <div className="row">
                                <div className="col-md-7 mb-3">
                                    <label htmlFor="name">Name on card</label>
                                    <input type="text" className="form-control "
                                           name="name"
                                           required=""
                                           inputMode="text"
                                           value={this.state.name}
                                           onChange={this.handleInputChange}
                                           onFocus={this.handleInputFocus}
                                    />

                                </div>
                                <div className="col-md-4 px-6 mb-2">
                                    <label htmlFor="number">Credit card number</label>
                                    <input className="form-control"
                                           id="number"
                                           type="tel"
                                           inputMode="numeric"
                                           pattern="[0-9\s]{13,19}"
                                           autoComplete="number"
                                           maxLength="19"
                                           name="number"
                                           value={this.state.number}
                                           onChange={this.handleInputChange}
                                           onFocus={this.handleInputFocus}

                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3 mb-2">
                                    <label htmlFor="expiry">Expiration</label>
                                    <input type="text"
                                           className="form-control"
                                           id="expiry"
                                           name="expiry"
                                           onChange={this.handleInputChange}
                                           onFocus={this.handleInputFocus}
                                           value={this.state.expiry}
                                           maxLength="4"
                                           inputMode="numeric"
                                           required=""

                                    />
                                </div>
                                <div className="col-md-2 mb-2">
                                    <label htmlFor="cvc">CVC</label>
                                    <input type="text" className="form-control"
                                           id="cvc"
                                           name="cvc"
                                           onChange={this.handleInputChange}
                                           onFocus={this.handleInputFocus}
                                           value={this.state.cvc}
                                           required=""
                                           inputMode="numeric"
                                           maxLength="3"
                                    />
                                </div>
                            </div>

                            <button type="submit" className="btn category_selected"
                                    style={{fontWeight: "bold", marginTop: "14px"}} disabled={this.state.loading}>
                                {this.state.loading &&
                                <span className="spinner-border spinner-border-sm mx-1" role="status"
                                      aria-hidden="true"/>}
                                {this.state.loading ? "Saving..." : "Save "}
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        );
    }
}


export default CreditCardForm


