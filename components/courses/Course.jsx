import React, {useState} from 'react';
import Link from "next/link";
import {useDispatch} from "react-redux";
import {addCourseToCart} from "../../redux/actions/cartAction";
import {enrollForFreeCourse} from "../../actions/student/course";
import Router from "next/router";
import {getUser} from "../../actions/auth";
import {secondsToHmsPlayer} from "../../actions/util";
import RatingEmpty from "../commons/RatingEmpty";
import RatingFull from "../commons/RatingFull";

const Course = ({responsive, course}) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);

    const handleLikeACourse = (e) => {
        const id = e.currentTarget.dataset.id;

    };
    const handleEnroll = async (e) => {
        const user = getUser();
        const slug = e.currentTarget.dataset.slug
        setLoading(true)
        if (user) {
            enrollForFreeCourse(slug).then(response => {
                if (response.errors) {
                    console.log(response.errors)
                } else {
                    setTimeout(() => {
                        setLoading(false);
                        Router.push("/student/account")
                    }, 3000);
                }
            })
        } else {
            Router.replace('/login')
        }
    }


    const handleAddToCart = (e) => {
        const course = JSON.parse(e.currentTarget.dataset.course)
        dispatch(addCourseToCart(course))

    }
    return (

        <div className={responsive}>
            <div className="row p-2 mb-3 bg-white  rounded cart_item">

                <div className="col-md-3 mt-1">
                    <Link href="/courses/[slug]" as={`/courses/${course.slug}`}>
                        <div className="rounded courses_img_wrapper">
                            <a>
                                <img className="rounded course-image" src={course.url} alt="image"/>
                            </a>
                        </div>

                    </Link>

                </div>
                <div className="col-md-6 mt-2">
                    <div>
                        <Link href="/courses/[slug]" as={`/courses/${course.slug}`}>
                            <h5 className="course_title"> {course.title}</h5>
                        </Link>

                        <div className="d-flex flex-row">
                            <div className="ratings mr-2 mt-1">
                                <span className="rating">
                                    {course.rating_total_sum > 0

                                        ?
                                        <>
                                            <RatingFull initialRating={course.rating / course.rating_total_sum}/>
                                            <span>({course.rating_total_sum})</span>
                                        </>
                                        :
                                        <>
                                            <RatingEmpty/>
                                        </>
                                    }


                                </span>

                            </div>
                        </div>
                    </div>
                    <div className="mt-4 mb-1 spec-1">
                        <span className="card__author">By {course.teacher_name}</span><br/>
                    </div>
                    <div className="mt-1 mb-1 spec-1 d-flex justify-content-between">
                        <span className="meta__date">
                            <i className="la la-play-circle"/> {course.meta.total_video} Classes
                        </span>
                        <span className="meta__date">
                            <i className="la la-clock-o"/> {secondsToHmsPlayer(course.duration)}
                        </span>
                    </div>
                </div>
                {course.price == 0
                    ?
                    <div className="align-items-center align-content-center col-md-3 border-left mt-1">
                        <div className="d-flex flex-row align-items-center  justify-content-between">
                            <h4 className="mr-1 font-weight-bold">Free</h4>
                        </div>
                        <div className="d-flex flex-column mt-4">
                            <button className="btn btn-light btn-sm btn__details" type="button">Details</button>
                            <button className="btn btn-light btn-sm mt-2" type="button"
                                    onClick={handleEnroll}
                                    data-slug={course.slug}
                                    disabled={loading}
                            >
                                {loading
                                    ? <><span className="spinner-border spinner-border-sm mx-1"
                                              role="status" aria-hidden="true"/> Enrolling...</>
                                    : `Enroll`}
                            </button>

                        </div>
                    </div>
                    :
                    <div className="align-items-center align-content-center col-md-3 border-left mt-1">
                        <div className="d-flex flex-row align-items-center  justify-content-between">
                            <h4 className="mr-1 font-weight-bold">£{course.price}</h4>
                            <span className="badge bg-danger " style={{fontSize: "12px"}}>-15%</span>
                            <span className="strike-text" style={{fontSize: "15px"}}>${((100*course.price)/85).toPrecision(2)}</span>
                        </div>
                        <div className="d-flex flex-column mt-4">
                            <button className="btn btn-light btn-sm btn__details" type="button">Details</button>
                            <button className="btn btn-light btn-sm mt-2" type="button"
                                    onClick={handleAddToCart}
                                    data-course={JSON.stringify({
                                        slug: course.slug,
                                        title: course.title,
                                        price: course.price
                                    })}
                            >
                                Add to cart
                            </button>
                        </div>
                    </div>

                }

            </div>
        </div>
    );
}

export default Course;
