import React, {useState} from "react";
import Rating from "react-rating";
import Link from "next/link";

const CourseList = ({ responsive,course}) => {

    const [, setEnrolled] = useState(false);
    const [, setLike] = useState(false);

    const handleEnroll = (e) => {

    };
    const handleLikeACourse = (e) => {

    };
    const handleAddToCart = (e) => {


    }
    return (
        <div className={responsive}>
            <div className="card-item card-list-layout course_ipad">
                    <div className="card-image">
                        <Link href="/courses/[slug]" as={`/courses/${course.slug}`}>
                            <a className="card__img">
                                <img src={course.url} alt="image"/>
                            </a>
                        </Link>
                    </div>
                    <div className="card-content responsive_card_content">
                        <p className="card__label">
                            <span className="card__label-text">{course.level}</span>
                            <a href="#" className="card__collection-icon"><span className="la la-heart-o"/></a>
                        </p>
                        <h4 className="card__title">
                            <Link href="/courses/[slug]" as={`/courses/${course.slug}`}>
                                <a>{course.title}</a>
                            </Link>

                        </h4>
                        <p className="card__author">
                            <a href="teacher-detail.html">{course.teacher[0].details.name}</a>
                        </p>
                        <div className="rating-wrap d-flex mt-2 mb-3">
                            <>
                            <span className="rating">
                              <Rating
                                  emptySymbol={<i className="las la-star"/>}
                                  fullSymbol={
                                      <i
                                          className="las la-star"
                                          style={{color: "red"}}
                                      />
                                  }
                                  start={0}
                                  stop={5}
                                  step={1}
                                  initialRating={4}
                                  readonly={true}
                              />
                            </span>

                                {/*<span className="star-rating-wrap">*/}
                                {/*  <span className="star__rating" style={{fontSize: "12px"}}>*/}
                                {/*    No rating yet*/}
                                {/*  </span>*/}
                                {/*</span>*/}
                            </>
                        </div>

                            <ul className="card-duration d-flex justify-content-between align-items-center">
                                <li>
                                    <span className="meta__date">
                                        <i className="la la-play-circle"/> {course.meta.totalClasses} Classes
                                    </span>
                                </li>
                                <li>
                                    <span className="meta__date">
                                        <i className="la la-clock-o" /> 3 hours 20 min
                                    </span>
                                </li>
                            </ul>

                        <div className="card-price-wrap d-flex justify-content-between align-items-center" style={{marginTop:"8px"}}>
                            <span className="card__price">{course.price}</span>
                            <a href="#" className="text-btn">Enroll</a>
                        </div>
                    </div>
                </div>
        </div>
    );
};

export default CourseList;
