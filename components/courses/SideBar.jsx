import SideBarWidget from "../commons/SideBarWidget";


const Sidebar = ({
                     categories,
                     handleClickCategory,
                     category_ids,
                     handleClickPrice,
                     handleClickLevel,
                     handleSearch,
                     handleSearchBtn,
                 }) => {

    return (
        <>
            <SideBarWidget title="Search for  courses">
                <div className="input-group">
                    <input
                        type="search"
                        className="form-control search_courses"
                        placeholder="Votre recherche ..."
                        onChange={handleSearch}
                    />
                    <span className="input-group-btn">
                    <button className="btn btn-default searchable_btn" type="button" onClick={handleSearchBtn}>
                        <i className="las la-search"/>
                    </button>
                  </span>
                                </div>
            </SideBarWidget>

            <SideBarWidget title="Price">
                <label className="radio__wrapper" data-price="free" onClick={handleClickPrice}>
                    <span className="radio__value badge badge-info">
                          Free
                    </span>
                    <input type="radio" name="radio"/>
                    <span className="checkmark"/>
                </label>
                <label className="radio__wrapper" data-price="paid" onClick={handleClickPrice}>
                    <span className="radio__value badge badge-info">
                         Paid
                    </span>
                    <input type="radio" name="radio"/>
                    <span className="checkmark"/>
                </label>
            </SideBarWidget>
            <SideBarWidget title="Level">
                <label className="radio__wrapper" data-level="all" onClick={handleClickLevel}>
                    <span className="radio__value badge badge-info">
                          All Level
                    </span>
                    <input type="radio" name="level"/>
                    <span className="checkmark"/>
                </label>
                <label className="radio__wrapper" data-level="beginner" onClick={handleClickLevel}>
                    <span className="radio__value badge badge-info">
                          Beginner
                    </span>
                    <input type="radio" name="level"/>
                    <span className="checkmark"/>
                </label>
                <label className="radio__wrapper" data-level="intermediary" onClick={handleClickLevel}>
                    <span className="radio__value badge badge-info">
                        Intermediary
                    </span>
                    <input type="radio" name="level"/>
                    <span className="checkmark"/>
                </label>
                <label className="radio__wrapper" data-level="advanced" onClick={handleClickLevel}>
                    <span className="radio__value badge badge-info">
                        Advanced
                    </span>
                    <input type="radio" name="level"/>
                    <span className="checkmark"/>
                </label>
            </SideBarWidget>

            <SideBarWidget title="Categories">
                {categories &&
                categories.map((category) => (
                    <span
                        key={category.id}
                        data-id={category.id}
                        className="categories"
                        onClick={handleClickCategory}
                    >
                        <span
                            className={
                                category_ids.includes(category.id)
                                    ? "badge badge-info category_selected"
                                    : "badge badge-info category"
                            }
                        >
                {category.name}
              </span>
            </span>
                ))}
            </SideBarWidget>

        </>
    );
};

export default Sidebar;
