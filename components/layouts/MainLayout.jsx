import Header from "../header-footer/Header";
import Footer from "../header-footer/Footer";
import React from "react";

const Layout = ({children}) => {

    return (
        <>
            <Header/>
            {children}
            <Footer/>
        </>
    );
};

export default Layout;


