import React from 'react';
import Header from "../header-footer/Header";
import Footer from "../header-footer/Footer";
import SmallLoader from "../commons/BigLoader";
import TeacherSideBar from "../teacher/TeacherSideBar";

const TeacherLayout = ({children, title}) => {
    return (
        <>
            <Header/>
            <section id="admin-area">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-3">
                            <TeacherSideBar/>
                        </div>
                        <div className="col-md-9">
                            <div className="card shadow-sm p-3 mb-5 bg-body rounded"
                                 style={{padding: "0px", minHeight: "100vh", border: "none"}}>
                                <div className="card-body" style={{borderRadius: "5px"}}>
                                    <div className="card-box-shared-title">
                                        <h3 className="widget-title">
                                            {title ? title : <SmallLoader/>}
                                        </h3>
                                    </div>
                                    <div className="card-box-shared-body">{children}</div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <Footer/>
        </>

    );
};

export default TeacherLayout;