import React from 'react';

const FeatureComponent = () => {
    return (

        <div className="container px-4 py-1">
            <div className="row g-4 py-5 row-cols-1 row-cols-lg-3">
                <div className="feature col">
                    <div className="d-flex align-items-center justify-content-center">
                        <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/icon_home1.png" alt="icon" width={100} height={100}/>
                    </div>
                    <h4 className="text-center my-3 font-weight-bold">LEARN FROM <br/>TOP PROFESSIONALS</h4>
                    <p className="text-center">Learn the latest skills in programming in your own language from experts in the field.</p>

                </div>
                <div className="feature col ">
                    <div className="d-flex align-items-center justify-content-center">
                        <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/icon_home2.png" alt="icon" width={100} height={100}/>
                    </div>
                    <h4 className="text-center my-3 font-weight-bold">LEARN ONLINE <br/> BY DOING</h4>
                    <p className="text-center">Apply what you learn with hands-on projects. Get feedback from a global community of learners.</p>
                </div>
                <div className="feature col">
                    <div className="d-flex align-items-center justify-content-center">
                        <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/icon_home3.png" alt="icon" width={100} height={100}/>
                    </div>
                    <h4 className="text-center my-3 font-weight-bold">GET READY FOR <br/> YOUR DREAM JOB</h4>
                    <p className="text-center">Start learning online  to get ready for a career in high-demand fields like IT, AI and cloud.</p>

                </div>
            </div>
        </div>
    );
};

export default FeatureComponent;