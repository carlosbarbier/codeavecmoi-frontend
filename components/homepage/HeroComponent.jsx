import React from 'react';
import Router from "next/router";

const HeroComponent = () => {
    return (
        <div className="container-fluid" style={{backgroundColor: "#F7FAFD"}}>
            <div className="container">
                <div className="row flex-lg-row-reverse align-items-center">
                    <div className="col-10 col-sm-8 col-lg-8">
                        <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/banner2.png"
                             className="d-block mx-lg-auto img-fluid" alt="image"/>
                    </div>
                    <div className="col-lg-4">
                        <h1 className="display-5 fw-bold lh-1 mb-1">Let's do it</h1>
                        <h1 className="display-5 fw-bold lh-1 mb-4" style={{color: "#008DA1"}}>Together</h1>
                        <p className="lead my-2">Learn any coding program from anywhere any time supported by
                            experts</p>
                        <div className="d-grid gap-2 d-md-flex justify-content-md-start">
                            <button type="button" className="btn btn-primary btn-lg px-4 me-md-2"
                                    style={{backgroundColor: "#008DA1", border: "none"}} onClick={() => {
                                Router.push("/register")
                            }}>Join for Free
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    )
}

export default HeroComponent;