import React from 'react';
import Rating from "react-rating";
import Link from "next/link";
import RatingFull from "../commons/RatingFull";
import RatingEmpty from "../commons/RatingEmpty";
import {secondsToHmsPlayer} from "../../actions/util";

const ExploreCourses = ({latest_courses, premium_courses}) => {
    return (
        <div className="container-fluid py-5" style={{backgroundColor: "#F8F9FA"}}>
            <div className="container">
                <h4 className="fw-bold lh-1 mb-3">The latest courses</h4>
                <hr/>
                <div className="row my-2">
                    {premium_courses && latest_courses.map(course => (
                        <div className="col-md-3" key={course.slug}>
                            <div className="card  mb-4 shadow  bg-body rounded">
                                <div className="rounded home_course_img_wrapper">
                                    <Link href="/courses/[slug]" as={`/courses/${course.slug}`}>
                                        <a>
                                            <img className=" home_course_img " src={course.url} alt="image"/>
                                        </a>
                                    </Link>
                                </div>
                                <div className="card-body _card_body ">
                                    <h6 className="course_title"> {course.title}</h6>
                                    <ul className="mb-3 d-flex justify-content-between" style={{fontSize: "13px"}}>
                                        <li className="list-inline-item"><i className="bi bi-clock"/> {secondsToHmsPlayer(course.duration)}</li>
                                        <li className="list-inline-item"><i className="bi bi-bar-chart"/> {course.level}</li>
                                    </ul>
                                    <div className="">
                                          <span className="rating">
                                    {course.rating_total_sum > 0

                                        ?
                                        <>
                                            <RatingFull initialRating={course.rating / course.rating_total_sum}/>
                                            <span>({course.rating_total_sum})</span>
                                        </>
                                        :
                                        <>
                                            <RatingEmpty/>
                                        </>
                                    }

                                </span>

                                    </div>
                                </div>

                            </div>
                        </div>
                    ))}

                </div>

            </div>

            <div className="container mt-4">
                <h4 className="fw-bold lh-1 mb-3">Trending Courses</h4>
                <hr/>
                <div className="row my-2">
                    {premium_courses && premium_courses.map(course => (
                        <div className="col-md-3" key={course.slug}>
                            <div className="card  mb-4 shadow  bg-body rounded">
                                <div className="rounded home_course_img_wrapper">
                                    <Link href="/courses/[slug]" as={`/courses/${course.slug}`}>
                                        <a>
                                            <img className=" home_course_img " src={course.url} alt="image"/>
                                        </a>
                                    </Link>
                                </div>
                                <div className="card-body _card_body ">
                                    <h6 className="course_title"> {course.title}</h6>
                                    <ul className="mb-3 d-flex justify-content-between" style={{fontSize: "13px"}}>
                                        <li className="list-inline-item"><i className="bi bi-clock"/> {secondsToHmsPlayer(course.duration)}</li>
                                        <li className="list-inline-item"><i className="bi bi-bar-chart"/> {course.level}</li>
                                    </ul>
                                    <div className="">
                                          <span className="rating">
                                    {course.rating_total_sum > 0

                                        ?
                                        <>
                                            <RatingFull initialRating={course.rating / course.rating_total_sum}/>
                                            <span>({course.rating_total_sum})</span>
                                        </>
                                        :
                                        <>
                                            <RatingEmpty/>
                                        </>
                                    }

                                </span>

                                    </div>
                                </div>

                            </div>
                        </div>
                    ))}
                </div>

            </div>
        </div>
    );
};

export default ExploreCourses;