import Link from "next/link";
import {secondsToHmsPlayer} from "../../actions/util";
const SearchItem = ({course}) => {
    console.log(course)
    return (
        <div
            className="card card-body"
            style={{
                border: "0",
                backgroundColor: "#fff",
            }}
        >
            <p
                style={{
                    opacity: ".7",
                }}
            >
                <Link href={`/courses/${course.slug}`}>
                    <a style={{color: "#1e1e3f "}}>{course.title}</a>
                </Link>
            </p>

            <div
                className="card-duration d-flex"
                style={{
                    backgroundColor: "#fff",
                    opacity: ".5",
                }}
            >
        <span className="meta__date">
          <i className="la la-clock-o"/> {secondsToHmsPlayer(course.duration)}
        </span>
                <span className="meta__date" style={{marginLeft: "15px"}}>
          <i className="las la-folder-plus"/>
                    {course.category.name}
        </span>
            </div>
        </div>
    );
};

export default SearchItem;