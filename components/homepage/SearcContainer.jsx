import React from "react";
import SearchItem from "./SearchItem";

const SearchContainer = ({ courses }) => {
    return (
        <>
            {courses && courses.map((course) => <SearchItem key={course.id} course={course} />)}
            { courses && courses.length === 0 && <p>No data found for your search</p>}
        </>
    );
};

export default SearchContainer;