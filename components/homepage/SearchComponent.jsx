import React, {useState} from 'react';
import SearchContainer from "./SearcContainer";

const SearchComponent = ({courses}) => {
    const [state, setState] = useState({
        searchTerm: "",
        searchable: false,
    });
    const {searchTerm, searchable} = state;
    const handleSearch = (e) => {
        setState({
            ...state,
            searchTerm: e.target.value,
            searchable: true,
        });
    };
    const filteredCourses = courses && courses.filter((course) => {
        return course.title.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;
    });
    return (
        <div className="container-fluid py-5" style={{backgroundColor: "#233D63"}}>
            <div className="container">
                <div className="hero-search-form">
                    <div className="hero-search-form">
                        <div className="row">
                            <div className="col-md-7 mx-auto">
                                <input
                                    className="form-control mx-1"
                                    style={{ width:"48vw"}}
                                    type="text"
                                    name="search"
                                    placeholder="Search courses by keywords"
                                    onChange={handleSearch}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-8 mx-auto" style={{backgroundColor:"red"}}>
                                {searchable && searchTerm !== "" && (
                                    <div
                                        style={{
                                            position: "absolute",
                                            zIndex: "100",
                                            left: 0,
                                            right: 0,
                                            margin: "auto",
                                            background: "#fff",
                                            color: "#1e1e3f",
                                            width:"48vw",
                                            fontWeight: "500",
                                            marginTop: "8px",
                                        }}
                                    >
                                        <SearchContainer courses={filteredCourses}/>
                                    </div>
                                )}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default SearchComponent;