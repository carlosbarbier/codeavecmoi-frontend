const SmallLoader = () => {
  return (
    <div className="d-flex justify-content-center">
      <img src="/images/small_loader.gif" alt="loading" />
    </div>
  );
};

export default SmallLoader;
