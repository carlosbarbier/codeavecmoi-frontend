import React from "react";

const LoadingButton = ({loading, message,loading_message}) => {
    return (
        <>
            <button
                type="submit"
                className="btn btn-dark btn-submit"
                disabled={loading}
            >
                {loading && (
                    <span
                        className="spinner-border spinner-border-sm"
                        role="status"
                        aria-hidden="true"
                    />
                )}
                {loading && <span>{loading_message} </span>}
                {!loading && <span>{message}</span>}
            </button>
        </>
    );
};

export default LoadingButton;
