import React from 'react';
import Rating from "react-rating";

const RatingFull = ({initialRating}) => {
    return (
        <Rating
            emptySymbol={
                <i className="bi bi-star-fill"
                   style={{color: "#EEEEEE"}}
                />

            }
            fullSymbol={
                <i
                    className="bi bi-star-fill"
                    style={{color: "#FF7A18"}}
                />
            }
            start={0}
            stop={5}
            step={1}
            initialRating={initialRating}
            readonly={true}
        />


    );
};

export default RatingFull;