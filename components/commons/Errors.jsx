import React from 'react';

const Errors = ({errors}) => {
    return (
        <div>
            {(errors || []).map((error, index) => (
                <div key={index} style={{color: "#FF96B5"}}>
                    {error.message}
                </div>
            ))}
        </div>
    );
};

export default Errors;