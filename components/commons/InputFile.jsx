import React from "react";

const InputFile = ({ fileName, onChange }) => {
  return (
    <div className="custom-file">
      <input
        type="file"
        className="custom-file-input"
        id="customFile"
        onChange={onChange}
      />
      <label className="custom-file-label" htmlFor="customFile">
        {fileName}
      </label>
    </div>
  );
};

export default InputFile;
