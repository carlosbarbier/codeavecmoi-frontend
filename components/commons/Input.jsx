const Input = ({label, type, name, onchange, value, required}) => {
    return (
        <div className="input-box ">
            <label className="label-text label-y">
                {label}
                <span className="primary-color-2 ml-1">{required ? "*" : ""}</span>
            </label>
            <div className="form-group">
                <input
                    className="form-control shadow-none search_quizz"
                    type={type}
                    name={name}
                    onChange={onchange}
                    value={value}
                />
            </div>
        </div>
    );
};

export default Input;
