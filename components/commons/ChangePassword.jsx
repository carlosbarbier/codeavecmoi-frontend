import React, {useState} from 'react';
import {Button, Form, Input} from "antd";
import {changePassword} from "../../actions/auth";
import {openNotificationWithIcon} from "../../actions/util";
import Errors from "./Errors";
const layout = {
    labelCol: {span: 4},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 4, span: 16},
};
const ChangePassword = () => {
    const [errors, setErrors] = useState(null)
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const onFinish = (values) => {
        setLoading(true)
        changePassword({
            password: values.password,
            password_confirmation: values.confirm,
            current_password: values.current_password
        })
            .then(response => {
                if (response.errors) {
                    setTimeout(function () {
                        setLoading(false)
                        setErrors(response.errors)
                    }, 2000)

                } else {
                    setTimeout(function () {
                        setLoading(false)
                        openNotificationWithIcon(
                            "success",
                            `Password has been  updated Successfully`
                        )
                        form.resetFields(["password", "confirm", "current_password"])
                    }, 2000)

                }
            })
    };
    return (
        <>
            <Errors errors={errors}/>
            <Form
                {...layout}
                name="change_password"
                form={form}
                initialValues={{remember: true}}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Current Password"
                    name="current_password"
                    rules={[{required: true, message: 'current password required'}]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item
                    label="New Password"
                    name="password"
                    rules={[{required: true, message: 'new password required'}]}
                >
                    <Input.Password/>
                </Form.Item>
                <Form.Item
                    name="confirm"
                    label="Confirm Password"
                    dependencies={['password']}
                    rules={[
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        ({getFieldValue}) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                            },
                        }),
                    ]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit" disabled={loading}>
                        {loading
                            ? <><span className="spinner-border spinner-border-sm mx-1"
                                      role="status" aria-hidden="true"/> Saving...</>
                            : ` Save`}
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default ChangePassword;