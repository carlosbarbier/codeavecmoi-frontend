import React from 'react';
import {Skeleton} from "antd";

const LoadingSkeleton = () => {
    return   <Skeleton  paragraph={{ rows: 15 }} title={false} active />
};

export default LoadingSkeleton;