import React from "react";

const FlashMessage = ({ message, alert, onClose }) => {
  return (
    <div
      className={`alert ${alert} alert-dismissible fade show flash-container`}
      role="alert"
    >
      {message}

      <div aria-hidden="true" className="close" onClick={onClose}>
        &times;
      </div>
    </div>
  );
};

export default FlashMessage;
