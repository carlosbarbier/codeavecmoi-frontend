import React from 'react';
import Rating from "react-rating";
import moment from "moment";


const Comment = ({comment}) => {
    return (
        <div className="card card-inner">
            <div className="card-body">
                <div className="row">
                    <div className="col-md-1">
                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" className="img img-rounded img-fluid"/>
                    </div>
                    <div className="col-md-11">
                        <p>
                            <span className="float-left font-weight-bold">{comment.author} </span>
                            <span className="float-right  mx-2">{moment(comment.created_at).fromNow()}</span>
                            <Rating
                                emptySymbol={<i className="las la-star"/>}
                                fullSymbol={
                                    <i
                                        className="las la-star"
                                        style={{color: "red"}}
                                    />
                                }
                                start={0}
                                stop={5}
                                step={1}
                                initialRating={comment.rating}
                                readonly={true}
                            />

                        </p>
                        <div className="clearfix"/>
                        <div style={{fontSize:"13px"}}>{comment.comment_body} </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default Comment;
