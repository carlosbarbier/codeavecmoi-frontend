import React from "react";

const SideBarWidget = ({children, title}) => {
    return (
        <div className="sidebar-widget border-0">
            <h3 className="widget-title">{title}</h3>
            <div className="filter-by-category">{children}</div>
        </div>
    );
};

export default SideBarWidget;
