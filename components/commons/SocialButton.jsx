import SocialLogin from "react-social-login";
import { Component } from "react";
class SocialButton extends Component {
  render() {
    return <div onClick={this.props.triggerLogin}>{this.props.children}</div>;
  }
}

export default SocialLogin(SocialButton);
