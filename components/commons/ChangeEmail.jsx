import React, {useState} from 'react';
import {Button, Form, Input} from "antd";
import {updateProfile} from "../../actions/auth";
import {openNotificationWithIcon} from "../../actions/util";
import Errors from "./Errors";
const layout = {
    labelCol: {span: 4},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 4, span: 16},
};
const ChangeEmail = ({user}) => {
    const [errors, setErrors] = useState(null)
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const onFinish = (values) => {
        setLoading(true)
        updateProfile({email: values.email, name: user.name}).then(response => {
            if (response.errors) {
                setTimeout(function () {
                    setLoading(false)
                    setErrors(response.errors)
                }, 2000)

            } else {
                setTimeout(function () {
                    setLoading(false)
                    openNotificationWithIcon(
                        "success",
                        `Email updated Successfully`
                    )
                    form.resetFields(["email"])

                }, 2000)

            }
        })

    };
    return (
        <>
            <Errors errors={errors}/>
            <Form
                {...layout}
                name="reset_password"
                form={form}
                initialValues={{
                    remember: true,
                    email: user.email
                }}
                onFinish={onFinish}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{
                        required: true,
                        message: 'Please input your a valid email!',
                        type: 'email',
                    }]}
                >
                    <Input/>
                </Form.Item>


                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit" disabled={loading}>
                        {loading
                            ? <><span className="spinner-border spinner-border-sm mx-1"
                                      role="status" aria-hidden="true"/> Saving...</>
                            : ` Save`}
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default ChangeEmail;