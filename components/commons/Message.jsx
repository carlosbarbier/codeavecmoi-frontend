const Message = ({ message, alert }) => {
  return (
    <div
      className={`alert ${alert}`}
      role="alert"
      style={{
        paddingTop: "5px",
        paddingBottom: "5px",
        alignContent: "center",
      }}
    >
      {message}
    </div>
  );
};

export default Message;
