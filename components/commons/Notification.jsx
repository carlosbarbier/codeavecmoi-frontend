import React from 'react';
import {notification, Space} from 'antd';

const Notification = ({type,message,description}) => {
   const openNotificationWithIcon = type => {
      notification[type]({
         message,
         description

      });
   };
   return(
       <>
          <Space type={type} >
             {openNotificationWithIcon(type)}
          </Space>,
       </>
   )

};

export default Notification;