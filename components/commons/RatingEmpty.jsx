import React from 'react';
import Rating from "react-rating";

const RatingEmpty = () => {
    return (
        <Rating
            emptySymbol={<i className="bi bi-star"
                            style={{color: "gray"}}
            />}
            start={0}
            step={1}
            readonly={true}
        />
    );
};

export default RatingEmpty;