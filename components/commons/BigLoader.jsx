const SmallLoader = () => {
  return (
    <div className="d-flex justify-content-center">
      <img src="/images/loader.gif" alt="loading" />
    </div>
  );
};

export default SmallLoader;
