const CheckBox = ({ label, value, handleClickCategory }) => {
  return (
    <div className="custom-control custom-checkbox">
      <input
        type="checkbox"
        className="custom-control-input"
        id={value}
        value={value}
        onChange={handleClickCategory}
      />
      <label className="custom-control-label" htmlFor={value}>
        {label}
      </label>
    </div>
  );
};

export default CheckBox;
