import Link from "next/link";

const Footer = () => {
    return (
        <>
            <section className="footer-area section-bg-2 ">
                <div className="container ">
                    <div className="copyright-content">
                        <div className="row align-items-center">
                            <div className="col-lg-6 ">
                                <div className="d-flex">
                                    <Link href="/">
                                        <a>
                                            <img
                                                src="/images/logo-white.png"
                                                alt="footer logo"
                                                className="footer__logo"
                                            />
                                        </a>
                                    </Link>
                                    <div className="copy__desc mx-2 my-2">  © {new Date().getFullYear()}</div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="d-flex flex-row-reverse" style={{color:"#ffffff"}}>
                                    <div className="p-2 ">Help and Support</div>
                                    <div className="p-2">Privacy policy and cookie policy</div>
                                    <div className="p-2 ">Terms</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="copyright-content-inner responsive-footer">
                    <div className="container footer_container">
                        <Link href="/">
                            <a>
                                <img
                                    src="/images/logo-white.png"
                                    alt="footer logo"
                                    className="footer__logo"
                                    style={{width: "128px", marginRight: "15px"}}
                                />
                            </a>
                        </Link>
                        <span style={{fontSize: "13px"}}>
                           {" "}
                            Copyright © {new Date().getFullYear()} CodeAvecMoi
                       </span>
                    </div>
                </div>
            </section>
        </>
    );
};

export default Footer;
