import React from 'react';
import Link from "next/link";
import {logout} from "../../actions/auth";
import Router from "next/router";

const UserDashboardItem = ({url}) => {
    return (
        <div className="dropdown m-2">
            <button className="btn btn-light dropdown-toggle" type="button"
                    id="dropdownMenu2" data-bs-toggle="dropdown"
                    aria-expanded="false">
            </button>
            <ul className="dropdown-menu" aria-labelledby="dropdownMenu2">
                <li>
                    <Link href={`${url}`}>
                        <a className="dropdown-item" style={{backgroundColor:"#ffffff",color:"#233D63"}}>Dashboard</a>
                    </Link>

                </li>

                <li>
                    <a
                        className="dropdown-item"
                        style={{backgroundColor:"#ffffff",color:"#233D63"}}
                        onClick={() =>
                            logout(() => Router.replace("/login"))
                        }
                    >
                        Logout
                    </a>

                </li>

            </ul>
        </div>
    );
};

export default UserDashboardItem;