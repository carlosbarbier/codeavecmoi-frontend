import React, {useState} from "react";
import Link from "next/link";
import {getUser, isLogin} from "../../actions/auth";
import {useSelector} from "react-redux";
import {Role} from "../../constants";
import UserDashboardItem from "./UserDashboardItem";
import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';
const Header = () => {
    const [show, setShow] = useState(false);
    const cart = useSelector(state => state.cart.cart)
    const handleShowMenu = () => {
        setShow(!show);
    };
    const handleItem = () => {
        setShow(false);
    };
    const user = getUser()
    return (
        <>
            <div className="header-menu-area shadow-sm">
                <div className="header-menu-fluid">
                        <div className="container">
                            <div className="main-menu-content ">
                                <div className="row align-items-center h-100">
                                    <div className="col-lg-3">
                                        <div className="logo-box">
                                            <Link href="/">
                                                <a className="logo">
                                                    <img src="/images/logo.png" alt="logo"/>
                                                </a>
                                            </Link>
                                        </div>
                                    </div>
                                    <div className="col-lg-9">
                                        <div className="menu-wrapper">
                                            <nav className="main-menu">
                                                <ul>
                                                    <li>
                                                        <Link href="/courses">
                                                            <a className="nav-item">Courses</a>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link href="/become">
                                                            <a className="nav-item">Become Instructor</a>
                                                        </Link>
                                                    </li>

                                                </ul>
                                            </nav>

                                            {!isLogin() && (
                                                <div className="logo-right-button logo-right-button-2">
                                                    <div className="user-action">
                                                        <Link href="/login" key="login">
                                                            <a className="nav-item login">Login</a>
                                                        </Link>

                                                        <Link href="/register" key="register">
                                                            <a className="theme-btn ms-3">Register</a>
                                                        </Link>
                                                    </div>
                                                </div>
                                            )}

                                            {user && user.role === Role.STUDENT && (
                                                <UserDashboardItem url="/student/account"/>
                                            )}

                                            {user && user.role === Role.TEACHER && (
                                                <UserDashboardItem url="/teacher/dashboard"/>
                                            )}
                                            {user && user.role === Role.ADMIN && (
                                                <UserDashboardItem url="/admin/dashboard"/>
                                            )}


                                            <Link href="/cart">
                                                <div className="shopping_cart_wrapper " style={{marginLeft: "24px"}}>
                                                           <span
                                                               className="total">{cart && cart.length > 0 && cart.length}</span>
                                                    <span>
                                                                <i className="las la-shopping-bag"
                                                                   style={{fontSize: "24px"}}/>
                                                           </span>
                                                </div>


                                            </Link>

                                        </div>
                                    </div>
                                </div>
                                <div className="reponsive_header d-flex justify-content-between pt-3 ">
                                    <div className="logo-box" onClick={handleItem}>
                                        <Link href="/">
                                            <a className="logo">
                                                <img src="/images/logo.png" alt="logo"/>
                                            </a>
                                        </Link>
                                    </div>
                                    <div className="nav_icon" onClick={handleShowMenu}>
                                        {show ? (
                                            <i
                                                className="las la-times"
                                                style={{fontSize: "45px"}}
                                            />
                                        ) : (
                                            <i
                                                className="las la-bars"
                                                style={{fontSize: "45px"}}
                                            />
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            {show && (
                <div className="menu_responsive d-flex align-items-center flex-column">
                    <p onClick={handleItem}>
                        <Link href="/">
                            <a className="nav-item">Home</a>
                        </Link>
                    </p>
                    <p onClick={handleItem}>
                        <Link href="/courses">
                            <a className="nav-item">courses</a>
                        </Link>
                    </p>
                    <p onClick={handleItem}>
                        <Link href="/quizzes">
                            <a className="nav-item">quizzes</a>
                        </Link>
                    </p>
                    {!isLogin() && (
                        <>
                            <p onClick={handleItem}>
                                <Link href="/login" key="login">
                                    <a className="nav-item ">Login</a>
                                </Link>
                            </p>
                            <p onClick={handleItem}>
                                <Link href="/register" key="register">
                                    <a className="theme-btn">Go Pro</a>
                                </Link>
                            </p>
                        </>
                    )}
                </div>
            )}
        </>
    );
};

export default Header;
