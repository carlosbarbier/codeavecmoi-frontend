import React from 'react';
import {FacebookLoginButton} from "react-social-login-buttons";
import SocialButton from "../commons/SocialButton";
import {isAuthenticate, loginWithSocial} from "../../actions/auth";
import {Provider, Role} from "../../constants";
import Router from "next/router";

const FacebookAuthentication = () => {
    const handleFacebookLogin = (data) => {
        const user = data._profile;
        loginWithSocial({email: user.email, password: user.id, name: user.lastName}, Provider.FACEBOOK).then((response) => {
            if (response.errors) {
                console.log(errors)
            } else {
                isAuthenticate(response, () => {
                    if (response.user.role === Role.TEACHER) {
                        Router.push("/teacher/dashboard")
                    } else {
                        Router.push("/student/account")
                    }
                });
            }
        });

    };

    const handleFacebookLoginFailure = (err) => {
        console.error(err);
    };
    return (
        <SocialButton
            provider="facebook"
            appId={process.env.NEXT_PUBLIC_FACEBOOK_CLIENT_ID}
            onLoginSuccess={handleFacebookLogin}
            onLoginFailure={handleFacebookLoginFailure}

        >
            <FacebookLoginButton text="Facebook"/>
        </SocialButton>

    );
};

export default FacebookAuthentication;
