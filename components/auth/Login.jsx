import React, {useEffect, useState} from "react";
import Link from "next/link";
import Input from "../commons/Input";
import {getUser, isAuthenticate, login,} from "../../actions/auth";
import Router from "next/router";
import FacebookAuthentication from "./FacebookAuthentication";
import {useDispatch} from "react-redux";
import {userLogin} from "../../redux/actions/userAction";
import GoogleAuthentication from "./GoogleAuthentication";
import {Role} from "../../constants";
import Errors from "../commons/Errors";

const LoginComponent = () => {
    const [state, setState] = useState({
        password: "",
        email: "",
        message: null,
        errors: null,
    });

    const [loading, setLoading] = useState(false)
    const dispatch = useDispatch()
    const {password, email, errors,} = state;
    const handleSubmit = async (e) => {
        setLoading(true)
        e.preventDefault()
        login({email, password}).then(data => {
            if (data.errors) {
                setTimeout(() => {
                    setLoading(false)
                    setState({...state, errors: data.errors});

                }, 1500)

            } else {
                setTimeout(() => {
                    setLoading(false)
                    isAuthenticate(data, () => {
                        dispatch(userLogin(data.user))
                        if (data.user.role === Role.TEACHER) {
                            Router.push("/teacher/dashboard")
                        } else if (data.user.role === Role.STUDENT) {
                            Router.push("/student/account")
                        } else if (data.user.role === Role.ADMIN) {
                            Router.push("/admin/dashboard")
                        }
                    });
                }, 1500)

            }
        });


    };
    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value, errors: null});
    };


    return (
        <section className="sign-up auth--margin">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 mx-auto">
                        <div className="card-box-shared">
                            <div className="card-box-shared-title text-center">
                                <h3 className="widget-title font-size-30">
                                    Login to your account
                                </h3>
                            </div>

                            <div className="card-box-shared-body">
                                <div className="contact-form-action">
                                    <div className="row mx-auto social_button_wrapper">
                                        <div className="col">
                                            <FacebookAuthentication/>
                                        </div>
                                        <div className="col ">
                                            <GoogleAuthentication/>
                                        </div>
                                    </div>
                                    <form onSubmit={handleSubmit}>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="account-assist mt-3 margin-bottom-35px text-center">
                                                    <p className="account__desc">or</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div>
                                                    <Errors errors={errors}/>
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <Input
                                                    label="Email"
                                                    type="email"
                                                    name="email"
                                                    onchange={handleChange("email")}
                                                    value={email}
                                                />
                                            </div>
                                            <div className="col-lg-12">
                                                <Input
                                                    label="Password"
                                                    type="password"
                                                    name="password"
                                                    onchange={handleChange("password")}
                                                    value={password}
                                                />
                                            </div>
                                            <div className="col-lg-12" style={{marginTop: "15px"}}>
                                                <div className="btn-box">
                                                    <button type="submit" className="btn theme-btn" disabled={loading}>
                                                        {loading &&
                                                        <span className="spinner-border spinner-border-sm mx-1"
                                                              role="status" aria-hidden="true"/>}
                                                        {loading ? " Login..." : " Login"}
                                                    </button>

                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <p className="mt-4">
                                                    Don't have an account?
                                                    <Link href="/register">
                                                        <a className="primary-color-2"> Register</a>
                                                    </Link>
                                                </p>
                                            </div>
                                            <div className="col-lg-12">
                                                <Link href="/forgot-password">
                                                    <a className="primary-color-2"
                                                       style={{fontSize: "12px", textDecoration: "underline"}}> Forgot
                                                        password</a>
                                                </Link>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default LoginComponent;
