import React, {useEffect, useState} from 'react';
import {validateAll} from "indicative/validator";
import Input from "../commons/Input";
import {resetPassword} from "../../actions/auth";
import cogoToast from "cogo-toast";
import Router from "next/router";
import {openNotificationWithIcon} from "../../actions/util";
import Errors from "../commons/Errors";

const ResetPassword = ({token, data}) => {
    const [state, setstate] = useState({
        password: "",
        password_confirmation: "",
        message: null,
        errors: null,
    });
    const {message, password_confirmation, errors, password} = state;
    useEffect(() => {
        setstate({...state, errors: data.errors})
    }, [])


    const handleChange = (name) => (e) => {
        setstate({...state, [name]: e.target.value});
    };
    const handleSubmit = async (e) => {
        e.preventDefault()
            resetPassword({password: valid.password, user_id: data.user_id}, token).then(data => {
                if (data.errors) {
                    setstate({...state, errors: data.errors});
                    setTimeout(() => {
                        setstate({...state, errors: null});
                    }, 3000)
                } else {
                    setstate({...state, password: "", password_confirmation: "", errors: null});
                    openNotificationWithIcon(
                        "success",
                        "Password updated successfully",
                    )
                }
            })

    };
    return (
        <section className="sign-up auth--margin forgot-password">
            <div className="container" style={{marginTop: "45px"}}>
                <div className="row">
                    <div className="col-lg-6 mx-auto">
                        <div className="card-box-shared rounded">
                            <div className="card-box-shared-title text-center">
                                <h3 className="widget-title font-size-30">
                                    Reset Your Password
                                </h3>
                            </div>

                            <div className="card-box-shared-body">
                                <div className="contact-form-action">
                                    <form onSubmit={handleSubmit}>
                                        <div className="row">

                                            <div className="col-lg-12">
                                                <div>
                                                   <Errors errors={errors}/>
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <Input
                                                    label="Password"
                                                    type="password"
                                                    name="password"
                                                    onchange={handleChange("password")}
                                                    value={password}
                                                />
                                            </div>
                                            <div className="col-lg-12">
                                                <Input
                                                    label="Confirm "
                                                    type="password"
                                                    name="password_confirmation"
                                                    onchange={handleChange("password_confirmation")}
                                                    value={password_confirmation}
                                                />
                                            </div>
                                            <div className="col-lg-12" style={{marginTop: "15px"}}>
                                                <div className="btn-box">
                                                    <button className="theme-btn" type="submit">
                                                        Submit
                                                    </button>

                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default ResetPassword;