import React, {useState} from 'react';
import Input from "../commons/Input";

import {openNotificationWithIcon} from "../../actions/util";
import {forgotPassword} from "../../actions/auth";
import Errors from "../commons/Errors";

const ForgotPasswordComponent = () => {
    const [state, setstate] = useState({
        email: "",
        message: null,
        errors: null,
    });
    const {message, email, errors,} = state;
    const handleChange = (name) => (e) => {
        setstate({...state, [name]: e.target.value});
    };
    const handleSubmit = async (e) => {
        e.preventDefault()
        forgotPassword({email}).then(data => {
            if (data.errors) {
                setstate({...state, errors: data.errors});
            } else {
                setstate({...state, email: "", errors: null});
                openNotificationWithIcon(
                    "success",
                    "Verify your email account",
                )
            }
        })
    };
    return (
        <section className="sign-up auth--margin forgot-password">
            <div className="container" style={{marginTop: "45px"}}>
                <div className="row">
                    <div className="col-lg-6 mx-auto">
                        <div className="card-box-shared">
                            <div className="card-box-shared-title text-center">
                                <h3 className="widget-title font-size-30">
                                    Forgot Password
                                </h3>
                            </div>

                            <div className="card-box-shared-body">
                                <div className="contact-form-action">
                                    <form onSubmit={handleSubmit}>
                                        <div className="row">

                                            <div className="col-lg-12">
                                                <div>
                                                   <Errors errors={errors}/>
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <Input
                                                    label="Email"
                                                    type="email"
                                                    name="email"
                                                    onchange={handleChange("email")}
                                                    value={email}
                                                />
                                            </div>

                                            <div className="col-lg-12" style={{marginTop: "15px"}}>
                                                <div className="btn-box">
                                                    <button className="theme-btn" type="submit">
                                                        Submit
                                                    </button>

                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default ForgotPasswordComponent;