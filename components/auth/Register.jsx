import React, {useEffect, useState} from "react";
import Input from "../commons/Input";
import {getUser, register,} from "../../actions/auth";
import Router from "next/router";
import GoogleAuthentication from "./GoogleAuthentication";
import FacebookAuthentication from "./FacebookAuthentication";
import {openNotificationWithIcon, sleep} from "../../actions/util";
import Errors from "../commons/Errors";

const RegisterComponent = () => {
    const [state, setState] = useState({
        name: "",
        password: "",
        email: "",
        errors: null,
    });
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getUser() && Router.push("/");
    }, []);

    const {name, password, email, errors} = state;
    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true)
        await sleep(1700)
        register({email, password, name}).then((data) => {
            if (data.errors) {
                setLoading(false)
                setState({...state, errors: data.errors});

            } else {
                setLoading(false)
                setState({
                    ...state,
                    name: "",
                    email: "",
                    password: "",
                });
                openNotificationWithIcon(
                    "success",
                    "Account created successfully, please verify your email to activate your account",
                )

            }
        })


    };
    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value, errors: null});
    };


    return (
        <section className="sign-up auth--margin">

            <div className="container">
                <div className="row">
                    <div className="col-lg-6 mx-auto">
                        <div className="card-box-shared">
                            <div className="card-box-shared-title text-center">
                                <h3 className="widget-title font-size-30">Create an account</h3>
                            </div>

                            <div className="card-box-shared-body">
                                <div className="contact-form-action">
                                    <div className="row mx-auto social_button_wrapper">
                                        <div className="col">
                                            <FacebookAuthentication/>
                                        </div>
                                        <div className="col ">
                                            <GoogleAuthentication/>
                                        </div>
                                    </div>
                                    <form onSubmit={handleSubmit}>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="account-assist mt-3 margin-bottom-35px text-center">
                                                    <p className="account__desc">or</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                             <Errors errors={errors}/>
                                            </div>
                                            <div className="col-lg-12">
                                                <Input
                                                    label="Name"
                                                    type="text"
                                                    name="name"
                                                    onchange={handleChange("name")}
                                                    value={name}
                                                />
                                            </div>
                                            <div className="col-lg-12">
                                                <Input
                                                    label="Email"
                                                    type="email"
                                                    name="email"
                                                    onchange={handleChange("email")}
                                                    value={email}
                                                />
                                            </div>
                                            <div className="col-lg-12">
                                                <Input
                                                    label="Password"
                                                    type="password"
                                                    name="password"
                                                    onchange={handleChange("password")}
                                                    value={password}
                                                />
                                            </div>
                                            <div className="col-lg-12" style={{marginTop: "15px"}}>
                                                <div className="btn-box">
                                                    <button type="submit" className="btn theme-btn" disabled={loading}>
                                                        {loading &&
                                                        <span className="spinner-border spinner-border-sm mx-1"
                                                              role="status" aria-hidden="true"/>}
                                                        {loading ? " Creating account..." : " Create account"}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default RegisterComponent;
