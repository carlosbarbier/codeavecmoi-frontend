import React from 'react';
import {GoogleLoginButton} from "react-social-login-buttons";
import SocialButton from "../commons/SocialButton";
import {isAuthenticate, loginWithSocial} from "../../actions/auth";
import Router from "next/router";
import {Provider, Role} from "../../constants";

const GoogleAuthentication = () => {
    const handleGoogleLogin = (data) => {
        const user = data._profile;
        loginWithSocial({email: user.email, password: user.id, name: user.name}, Provider.GOOGLE).then((response) => {
            if (response.errors) {
                console.log(errors)
            } else {
                isAuthenticate(response, () => {
                    if (response.user.role === Role.TEACHER) {
                        Router.push("/teacher/dashboard")
                    } else {
                        Router.push("/student/account")
                    }
                });
            }
        });
    };

    const handleGoogleLoginFailure = (err) => {
        console.error(err);
    };
    return (
        <SocialButton
            provider="google"
            appId={process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID}
            onLoginSuccess={handleGoogleLogin}
            onLoginFailure={handleGoogleLoginFailure}
        >
            <GoogleLoginButton text="Google"/>
        </SocialButton>
    );
};
const redirect = (user) => {
    if (user.role === "student") {
        Router.push("/student/account")
    } else if (user.role === "teacher") {
        Router.push("/teacher/my")
    } else if (user.role === "admin") {
        Router.push("/dashboard")
    }

}
export default GoogleAuthentication;
