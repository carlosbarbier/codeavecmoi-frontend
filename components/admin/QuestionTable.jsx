import React from 'react';
import {Table, Tag} from 'antd';
import moment from "moment";

const QuestionTable = ({qas}) => {
    const {Column} = Table;
    return (

        <div className="statement-table purchase-table table-responsive ">
            <Table dataSource={qas} rowKey={record => record.id}
                   expandable={{
                       expandedRowRender: record => (
                           <>
                               {record.replies.map((reply)=>(
                                   <div key={reply.id}>
                                       <p style={{ paddingLeft: "100px",paddingRight:"100px" }} >{reply.body}</p>
                                       <p style={{ paddingLeft: "100px",fontWeight:"600", marginTop:"8px"}} > <i className="bi bi-person"/> {reply.user.name}</p>
                                   </div>

                               ))}
                           </>
                       ),
                       rowExpandable: record => record.replies.length > 0,
                   }}
            >
                <Column title="Question" dataIndex="body" key="body"/>
                <Column title="Last Updated"
                        key="created_at"
                        render={(record) => (<span>
                                        {moment(record.created_at).fromNow()}
                                    </span>)}
                />
                <Column title="Owner"
                        key="owner"
                        render={(record) => (<span>
                                        {record.user.name}
                                    </span>)}
                />

            </Table>
        </div>


    )
};

export default QuestionTable;