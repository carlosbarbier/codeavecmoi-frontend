import React from 'react';
import {Space, Table, Tag} from 'antd';
import moment from "moment";

const PaymentTable = ({payments}) => {
    const {Column} = Table;
    return (

        <div className="row">
            <div className="col-lg-12">
                <div className="card-box-shared-body">
                    <div className="statement-table purchase-table table-responsive ">
                        <Table dataSource={payments} rowKey={record => record.id}>
                            <Column title="ID" dataIndex="id" key="id"/>
                            <Column title="Last Updated"
                                    key="created_at"
                                    render={(record) => (<span>
                                        {moment(record.created_at).fromNow()}
                                    </span>)}
                            />

                            <Column title="Course Title "
                                    key="title"
                                    render={(record) => (<span>
                                        {record.course.title}
                                    </span>)}
                            />
                            <Column
                                title="Status"
                                key="status"
                                render={(record, index) => (
                                    <Space size="middle" key={index}>
                                        <Tag color="#87d068">{record.status}</Tag>
                                    </Space>
                                )}
                            />
                            <Column title="Amount" dataIndex="amount" key="amount"/>

                        </Table>
                    </div>
                </div>
            </div>
        </div>


    )
};

export default PaymentTable;