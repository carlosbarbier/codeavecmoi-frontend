import React from 'react';
import {Table} from 'antd';
import moment from "moment";

const TeacherTable = ({teachers}) => {
    const {Column} = Table;
    return (

        <div className="statement-table purchase-table table-responsive ">
            <Table dataSource={teachers} rowKey={record => record.id}
            >
                <Column title="First Name" dataIndex="name" key="name"/>
                <Column title="Last Name" dataIndex="last_name" key="last_name"/>
                <Column title="Occupation" dataIndex="occupation" key="occupation"/>

                <Column title="Starting"
                        key="created_at"
                        render={(record) => (<span>
                                        {moment(record.created_at).fromNow()}
                                    </span>)}
                />

            </Table>
        </div>

    )
};

export default TeacherTable;