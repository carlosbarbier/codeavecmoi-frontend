import React from 'react';
import Link from "next/link";
import {logout} from "../../actions/auth";
import Router from "next/router";
const AdminSidebar = () => {
    return (
        <div className="card-header">
            <div className="side-menu-wrap">
                <ul className="side-menu-ul">
                    <li className="item">
                        <Link href="/admin/dashboard"
                              as={`/admin/dashboard`}>
                            <a className="admin-item">
                                <i className="la la-dashboard"/>Dashboard
                            </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/admin/courses">
                            <a className="admin-item">
                                <i className="la la-bolt"/>Courses
                            </a>
                        </Link>
                    </li>

                    <li className="item">
                        <Link href="/admin/questions">
                            <a className="admin-item">
                                <i className="bi bi-question-circle"/>Questions
                            </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/admin/teachers">
                            <a className="admin-item">
                                <i className="bi bi-tablet-landscape"/>Teachers
                            </a>
                        </Link>
                    </li>
                    <li className="item">
                        <Link href="/admin/payments" >
                            <a className="admin-item">
                                <i className="la la-shopping-cart"/>Payment History
                            </a>
                        </Link>
                    </li>
                    <Link href="/admin/settings" >
                        <li className="item">
                            <a className="admin-item">
                                <i className="la la-cog"/>Settings
                            </a>
                        </li>
                    </Link>

                    <Link href="/admin/email" >
                        <li className="item">
                            <a className="admin-item">
                                <i className="bi bi-envelope"/>Email
                            </a>
                        </li>
                    </Link>
                    <li className="item">
                        <a className="admin-item" onClick={() =>
                            logout(() => Router.replace("/login"))
                        }>
                            <i className="la la-power-off"/>Logout
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    );
};

export default AdminSidebar;