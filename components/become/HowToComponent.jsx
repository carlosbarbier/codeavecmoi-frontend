import React from 'react';

const HowToComponent = () => {
    return (
        <section className="info-area section-bg text-center">
            <div className="container">
                <div className="row my-3">
                    <div className="col-lg-12 mb-5">
                        <div className="section-heading  ">
                            <h1 className="display-5 fw-bold lh-1 mb-1">Create Online Course <br/>  Earn Money</h1>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-4">
                        <div className="info-box before-none">
                            <div className="icon-element mx-auto">
                                <i className="la la-lightbulb-o"/>
                            </div>
                            <h3 className="info__title mt-4 mb-2">01. Plan Your Course</h3>
                            <p className="info__text mb-0">Whether you want to develop as a professional or discover
                                a new hobby, there's an online course for that.</p>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="info-box before-none">
                            <div className="icon-element mx-auto">
                                <i className="la la-video-camera"/>
                            </div>
                            <h3 className="info__title mt-4 mb-2">02. Record Your Video</h3>
                            <p className="info__text mb-0">Whether you want to develop as a professional or discover
                                a new hobby, there's an online course for that.</p>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="info-box before-none ">
                            <div className="icon-element mx-auto">
                                <i className="la la-users"/>
                            </div>
                            <h3 className="info__title mt-4 mb-2">03. Build Community</h3>
                            <p className="info__text mb-0">Whether you want to develop as a professional or discover
                                a new hobby, there's an online course for that.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default HowToComponent;