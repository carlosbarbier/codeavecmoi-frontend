import React from 'react';

const ReasonComponent = () => {
    return (
        <div className="container px-4 py-1">
            <h1 className="display-5 fw-bold lh-1 text-center mt-5">So Many Reasons to Start</h1>
            <div className="row g-4 py-5 row-cols-1 row-cols-lg-3">
                <div className="feature col">
                    <div className="d-flex align-items-center justify-content-center">
                        <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/Icon-teacher1a.png" alt="icon" width={100}/>
                    </div>

                    <h4 className="text-center my-3 font-weight-bold">INSPIRE OTHERS</h4>
                    <p className="text-center">Teach what you know and help learners explore their interests to gain new skills.</p>

                </div>
                <div className="feature col ">
                    <div className="d-flex align-items-center justify-content-center">
                        <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/Icon-teacher2a.png" alt="icon"
                             width={100}/>
                    </div>
                    <h4 className="text-center my-3 font-weight-bold">TEACH YOUR WAY</h4>
                    <p className="text-center">Publish the course you want, in the way you want, and always have of control your own content.</p>
                </div>
                <div className="feature col">
                    <div className="d-flex justify-content-center">
                        <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/Icon-teacher3a.png" alt="icon"
                             width={100}
                        />
                    </div>
                    <h4 className="text-center my-3 font-weight-bold">GET REWARDED</h4>
                    <p className="text-center">Expand your network,
                        build your expertise, and earn extra
                        money.</p>

                </div>
            </div>
        </div>
    );
};

export default ReasonComponent;