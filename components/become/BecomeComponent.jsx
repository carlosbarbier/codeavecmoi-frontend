import React from 'react';
import Router from "next/router";

const BecomeComponent = ({executeScroll}) => {

    return (
        <div className="container-fluid" style={{backgroundColor: "#233D63"}}>
                <div className="row flex-lg-row-reverse align-items-center">
                    <div className="col-10 col-sm-8 col-lg-6" style={{padding:0}}>
                        <img src="https://codeavecmoi.s3-eu-west-1.amazonaws.com/static/Instructor-1.png"
                             className="d-block mx-lg-auto img-fluid" alt="image" height={425}/>
                    </div>
                    <div className="col-lg-6" >
                        <div style={{marginLeft:"80px"}}>
                            <h1 className="display-5 fw-bold lh-1 mb-1" style={{color: "#ffffff"}}>Come Teach With Us</h1>
                            <div className="lead my-4" style={{color: "#ffffff", fontSize:"18px"}}>Become an instructor and change lives, <br/> including your own</div>
                            <div className="d-grid gap-2 d-md-flex justify-content-md-start">
                                <button type="button" className="btn btn-primary btn-lg px-4 me-md-2"
                                        style={{backgroundColor: "#008DA1", border: "none"}} onClick={executeScroll}>Join for Free
                                </button>
                            </div>
                        </div>

                    </div>
                </div>

        </div>

    )
}

export default BecomeComponent;