import React from 'react';

const CounterComponent = () => {
    return (
        <section className="funfact-area text-center overflow-hidden ">
            <div className="container">
                <div className="row">
                    <div className="col-lg-4">
                        <div className="counter-item">
                            <span className="la la-bullhorn count__icon"/>
                            <h4 className="count__title counter">4520</h4>
                            <p className="count__meta">Instructors</p>
                        </div>
                    </div>
                    <div className="col-lg-4 ">
                        <div className="counter-item">
                            <span className="la la-globe count__icon"/>
                            <h4 className="count__title counter text-color">5452</h4>
                            <p className="count__meta">Courses</p>
                        </div>
                    </div>
                    <div className="col-lg-4 ">
                        <div className="counter-item">
                            <span className="la la-users count__icon"/>
                            <h4 className="count__title counter text-color-2">9720</h4>
                            <p className="count__meta">Students enrolled</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    );
};

export default CounterComponent;