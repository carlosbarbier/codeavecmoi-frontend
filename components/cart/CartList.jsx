import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getFromLocalstorage, sum} from "../../actions/util";
import {getUser} from "../../actions/auth";
import Router from "next/router";
import {removeCourseFromCart} from "../../redux/actions/cartAction";
import {resetPayment} from "../../redux/actions/paymentAction";
import CartItem from "./CartItem";
import PaymentForm from "./PaymentForm";

const CartList = () => {
    const dispatch = useDispatch();
    const [state, setState] = useState({
        showCart: false,
        prices: [],
        items: [],
        slugs: [],
        total: 0,
        courses: []
    })
    const [user, ] = useState(getUser())
    const {status} = useSelector(state => state.payment)

    const {showCart, total, items, prices, slugs} = state
    useEffect(() => {
        const arr = []
        const arr_course_slug = []
        const items = getFromLocalstorage('cart') || []
        items.map(item => {
            arr.push(item.price)
            arr_course_slug.push(item.slug)
        })
        setState({
            ...state,
            slugs: arr_course_slug,
            items,
            prices: arr,
        })
    }, [total, status])

    const handleCheckout = () => {
        if (!user) {

            Router.replace('/login')
        } else {
            setState({...state, showCart: true})
        }

    }
    const handleRemove = (e) => {
        const course = JSON.parse(e.currentTarget.dataset.course)
        dispatch(removeCourseFromCart(course))
        setState({...state, total: sum(prices)})
    }
    const handleRedirectToUserDashboard = () => {
        Router.replace("/student/account").then(r => dispatch(resetPayment()))
    }
    return (
        <div className="container">
            <div className="cart  shopping-cart-detail-item"
                 style={{minHeight: "60vh",background:"#F7FAFD",paddingTop:"23px", marginTop:"14px"}}>

                <div className="row">
                    <div className="col-lg-7 mx-auto">
                        {status === "ok" && <div className="alert alert-info" role="alert">

                            Congratulation {user.name},your enrollment has been complete successfully
                            <a className="font-weight-bold mx-1 go_to" onClick={handleRedirectToUserDashboard}>Go to
                                course</a>
                        </div>}

                        {
                            prices.length > 0 &&
                            <div className="font-weight-bold mb-3" style={{color: "#233d63", fontSize: "22px"}}>
                                {!showCart ? "Summary" : "Checkout"}
                            </div>
                        }

                        {items && items.map(course => (
                            <CartItem key={course.slug} course={course} handleRemove={handleRemove}/>
                        ))}

                        {prices.length === 0 &&
                        <p className="font-weight-bold">Empty shopping cart</p>}

                        {prices.length > 0 && !showCart &&
                        <div className="cart-total d-flex justify-content-between">
                                 <span className="font-weight-bold"
                                       style={{color: "#233d63", fontSize: "22px"}}>Total: £{sum(prices)}
                                 </span>
                            <button className="btn category_selected" style={{fontWeight: "bold"}}
                                    onClick={handleCheckout}>Checkout
                            </button>
                        </div>
                        }

                        {prices.length > 0 && showCart &&
                        <PaymentForm price={sum(prices)} slugs={slugs}/>
                        }

                    </div>

                </div>

            </div>
        </div>

    );
};

export default CartList;