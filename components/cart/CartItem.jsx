import React from 'react';

const CartItem = ({course, handleRemove}) => {
    return (
        <div className="card card-body mb-4 cart_item">
            <div className="d-flex justify-content-between">
                <p>{course.title}</p>
                <div>
                    <span className="font-weight-bold">Price: £{course.price}</span>
                    <button className="btn btn-sm btn-default"
                            style={{fontSize: "12px", padding: "5px", marginLeft: "22px"}}
                            data-course={JSON.stringify(course)} onClick={handleRemove}>remove
                    </button>
                </div>

            </div>
        </div>
    );
};

export default CartItem;