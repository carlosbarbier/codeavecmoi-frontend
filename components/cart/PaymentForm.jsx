import React, {Component} from 'react';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import {chargeStudent} from "../../actions/student/payment";
import {clearCart} from "../../redux/actions/cartAction";
import {errorPayment, successPayment} from "../../redux/actions/paymentAction";
import {connect} from 'react-redux';
import {openNotificationWithIcon} from "../../actions/util";

class PaymentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cvc: '',
            expiry: '',
            focus: '',
            name: '',
            number: '',
            errors: null,
            loading: false,
            success: false
        };

        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleInputFocus = (e) => {
        this.setState({focus: e.target.name});
    }

    handleInputChange = (e) => {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }
    handleSubmit = async (e) => {
        e.preventDefault()
        this.setState({...this.state, loading: true})
        await chargeStudent({
            cvc: this.state.cvc,
            name: this.state.name,
            exp_month: this.state.expiry.slice(0, 2),
            exp_year: "20" + this.state.expiry.slice(-2),
            number: this.state.number,
            amount: this.props.price,
            slugs: this.props.slugs,
        }).then(response => {
            if (response.errors) {
                this.setState({...this.state, loading: false, success: false})
                this.props.errorPayment();
                openNotificationWithIcon(
                    "error",
                    "Something went wrong, please try again",
                )
            } else {
                setTimeout(() => {
                    this.setState({cvc: '', expiry: '', focus: '', name: '', number: '', loading: false, success: true})
                    this.props.clearCart()
                    this.props.successPayment()
                }, 6000)

            }
        })
    }

    render() {

        return (
            <div className="card card-body my-4 cart_item">
                <div className="my-4">
                    <Cards
                        cvc={this.state.cvc}
                        expiry={this.state.expiry}
                        focused={this.state.focus}
                        name={this.state.name}
                        number={this.state.number}
                    />
                </div>

                <form onSubmit={this.handleSubmit} className="mb-4 mt-4">
                    <div className="row">
                        <div className="col-md-7 mb-3">
                            <label htmlFor="name">Name on card</label>
                            <input type="text" className="form-control "
                                   name="name"
                                   required=""
                                   inputMode="text"
                                   onChange={this.handleInputChange}
                                   onFocus={this.handleInputFocus}
                            />

                        </div>
                        <div className="col-md-4 px-6 mb-2">
                            <label htmlFor="number">Credit card number</label>
                            <input className="form-control"
                                   id="number"
                                   type="tel"
                                   inputMode="numeric"
                                   pattern="[0-9\s]{13,19}"
                                   autoComplete="number"
                                   maxLength="19"
                                   name="number"
                                   onChange={this.handleInputChange}
                                   onFocus={this.handleInputFocus}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 mb-2">
                            <label htmlFor="expiry">Expiration</label>
                            <input type="text"
                                   className="form-control"
                                   id="expiry"
                                   name="expiry"
                                   onChange={this.handleInputChange}
                                   onFocus={this.handleInputFocus}
                                   maxLength="4"
                                   inputMode="numeric"
                                   required=""

                            />
                        </div>
                        <div className="col-md-2 mb-2">
                            <label htmlFor="cvc">CVC</label>
                            <input type="text" className="form-control"
                                   id="cvc"
                                   name="cvc"
                                   onChange={this.handleInputChange}
                                   onFocus={this.handleInputFocus}
                                   required=""
                                   inputMode="numeric"
                                   maxLength="3"
                            />
                        </div>
                    </div>
                    <div className="my-2">
                         <span className="font-weight-bold"
                               style={{color: "#233d63", fontSize: "22px"}}>Total: £{this.props.price}
                         </span>
                    </div>
                    <button type="submit" className="btn category_selected"
                            style={{fontWeight: "bold", marginTop: "14px"}} disabled={this.state.loading}>
                        {this.state.loading &&
                        <span className="spinner-border spinner-border-sm mx-1" role="status" aria-hidden="true"/>}
                        {this.state.loading ? "Proceeding..." : "Proceed payment"}
                    </button>

                </form>
            </div>
        );
    }
}


export default connect(null, {errorPayment, successPayment, clearCart})(PaymentForm);


