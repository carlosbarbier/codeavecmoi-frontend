import React from 'react';
import Message from "../commons/Message";

const CourseWrapper = ({children, message, size, alert}) => {
    return (
        <div className="my-courses-area padding-top-30px padding-bottom-90px">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="my-course-content-wrap">
                            <div className="tab-content">
                                <div
                                    role="tabpanel"
                                    className="tab-pane fade active show"
                                    id="all-courses"
                                >
                                    {size > 0 ? "" : <Message message={message} alert={alert}/>}

                                    <div className="my-course-content-body">
                                        <div className="my-course-container"
                                             style={{minHeight: "40vh"}}>{children}</div>

                                        <p className="font-size-16 text-center ">
                                            {size === 1 ? "one course" : `${size} courses`}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CourseWrapper;
