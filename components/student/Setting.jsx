import React, {useState} from 'react';
import Input from "../commons/Input";
import {changePassword, updateProfile} from "../../actions/auth";

const Setting = ({user}) => {
    const [state, setState] = useState({
        password: "",
        current_password: "",
        password_confirmation: "",
        name: user.name,
        email: user.email,
        loading: false,
        errors: null,
    });

    const [show, setShow] = useState(false);

    const {password, email, name, password_confirmation, current_password, errors} = state;

    const handleChangeProfile =  (e) => {
        e.preventDefault();
        updateProfile({email,name}).then(response=>{
            if (response.errors) {
                setState({...state, errors: response.errors})
            } else {
                setState({...state, errors: null, email: "", name: ""})
            }
        })

    };
    const handleChangePassword = (e) => {
        e.preventDefault();
         changePassword({password, password_confirmation, current_password})
            .then(response => {
                if (response.errors) {
                    setState({...state, errors: response.errors})
                } else {
                    setState({...state, errors: null, password: "", password_confirmation: "", current_password: ""})
                }
            })

    };
    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value});
    };
    const handleDeleteAccount = () => {
        setShow(true);
    };
    const handleDeleteAccountInput = async (e) => {
        if (e.target.value === "delete") {

        }
    };
    return (
        <div className="container">
            <div className="row my-3 mx-2">
                <div className="card col-lg-7 my-3 mx-auto">
                    <div className="card-body ">
                        <div>
                            {(errors || []).map((error, index) => (
                                <p key={index} style={{color: "red", marginLeft: "14px"}}>
                                    {error.message}
                                </p>
                            ))}
                        </div>

                        <form onSubmit={handleChangeProfile}>
                            <div className="col-md-12">
                                <Input
                                    label="Change Name"
                                    type="text"
                                    name="name"
                                    value={name}
                                    onchange={handleChange("name")}
                                />
                                <Input
                                    label="Change Email"
                                    type="email"
                                    name="email"
                                    value={email}
                                    onchange={handleChange("email")}
                                />
                            </div>
                            <button type="submit" className="btn btn-light btn-sm mt-2"> Update profile</button>
                        </form>
                        <hr style={{borderTop: "3px solid black"}}/>
                        <form onSubmit={handleChangePassword}>
                            <div className="col-md-12">
                                <Input
                                    label="Current Password"
                                    type="password"
                                    name="current_password"
                                    onchange={handleChange("current_password")}
                                    value={current_password}

                                />
                                <Input
                                    label="New Password"
                                    type="password"
                                    name="password"
                                    onchange={handleChange("password")}
                                    value={password}
                                />
                                <Input
                                    label="Password Confirmation"
                                    type="password"
                                    name="password_confirmation"
                                    onchange={handleChange("password_confirmation")}
                                    value={password_confirmation}
                                />
                            </div>
                            <button type="submit" className="btn btn-light btn-sm mt-2">
                                Save Password
                            </button>
                        </form>
                        <hr style={{borderTop: "3px solid black"}}/>
                        <div className="row my-3">
                            <div className="col-md-4">
                                    <span
                                        className="btn btn-outline-danger ml-3"
                                        onClick={handleDeleteAccount}
                                    >
                                      Delete account
                                    </span>
                            </div>
                            {show && (
                                <div className="col-md-8">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="delete"
                                        onChange={handleDeleteAccountInput}
                                    />
                                    <small className="form-text text-muted">
                                        Type{" "}
                                        <span className="font-weight-bold font-italic">
                                                  delete
                                                </span>
                                    </small>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Setting;
