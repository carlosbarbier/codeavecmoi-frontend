const Tab = ({ label, className, handleClick, id }) => {
    return (
        <>
            <li>
                <a data-id={id} className={className} onClick={handleClick}>
                    {label}
                </a>
            </li>
        </>
    );
};

export default Tab;
