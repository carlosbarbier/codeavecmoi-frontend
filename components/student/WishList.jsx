import React from 'react';
import CourseWrapper from "./CourseWrapper";
import Course from "./Course";

const WishList = ({courses=[]}) => {
    return (
        <CourseWrapper
            message="No Wishlist"
            size={courses && courses.length}
            alert="alert-info"
        >
            <div className="row">

                {courses &&
                courses.map((course) => (
                    <Course
                        key={course.id}
                        course={course}
                        archive={false}
                        action="Continue"
                    />
                ))}
            </div>
        </CourseWrapper>
    );
};

export default WishList;
