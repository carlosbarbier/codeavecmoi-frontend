import React from 'react';
import CourseWrapper from "./CourseWrapper";
import Course from "./Course";

const AllCourse = ({courses}) => {
    return (
        <CourseWrapper
            message="No course"
            size={courses && courses.length}
            alert="alert-info"
        >
            <div className="row">

                {courses &&
                courses.map((course) => (
                    <Course
                        key={course.slug}
                        course={course}
                        archive={false}
                        action="Watch Again"
                    />
                ))}
            </div>
        </CourseWrapper>
    );
};

export default AllCourse;
