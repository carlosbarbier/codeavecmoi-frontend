import React from 'react';
import CourseWrapper from "./CourseWrapper";
import Course from "./Course";

const CompletedCourse = ({courses}) => {
    return (
        <CourseWrapper
            message="No Course Completed"
            size={courses && courses.length}
            alert="alert-info"
        >
            <div className="row">

                {courses &&
                courses.map((course) => (
                    <Course
                        key={course.slug}
                        course={course}
                        archive={false}
                        action="Watch Again"
                    />
                ))}
            </div>
        </CourseWrapper>
    );
};

export default CompletedCourse;
