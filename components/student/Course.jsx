import React from 'react';
import MediaQuery, {useMediaQuery} from "react-responsive";
import Link from "next/link";

const Course = ({course, action}) => {
    const isLandscape = useMediaQuery({orientation: "landscape"});
    const isPortrait = useMediaQuery({orientation: "portrait"});
    return (
        <>
            <MediaQuery minDeviceWidth={1224}>
                <div className="col-lg-4 ">
                    <div className="card-item border-0 shadow-sm">
                        <div className="card-image">
                            <Link href="/watch/[slug]" as={`/watch/${course.slug}`}>
                                <a className="card__img">
                                    <img
                                        src={course.url}
                                        alt="image"
                                        style={{height: "220px"}}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="card-content p-4">
                            <h3 className="card__title mt-0">
                                <Link href="/watch/[slug]" as={`/watch/${course.slug}`}>
                                    <a>{course.title}</a>
                                </Link>
                            </h3>
                            <p className="card__author">
                                <span>{course.description}</span>
                            </p>
                            <div className="course-complete-bar-2 mt-2">
                                <div className="progress-item mb-0">
                                    <p className="skillbar-title">Complete:</p>
                                    <div className="skillbar-box mt-1">
                                        <div
                                            className="progress"
                                            style={{height: "6px", backgroundColor: "#eee"}}
                                        >
                                            <div
                                                className="progress-bar"
                                                role="progressbar"
                                                style={{
                                                    width: `${(
                                                        (100 * course.meta.pivot_progress) /
                                                        course.duration
                                                    ).toPrecision(2)}%`,
                                                    backgroundColor: "#008da1",
                                                    opacity: ".5",
                                                }}
                                                aria-valuenow="25"
                                                aria-valuemin="0"
                                                aria-valuemax={course.duration}
                                            />
                                        </div>
                                    </div>
                                    <div className="skill-bar-percent">
                                        {Math.round((100 * course.meta.pivot_progress) / course.duration)}
                                        %
                                    </div>
                                </div>
                            </div>
                            <div className="rating-wrap d-flex mt-3 justify-content-between btn_responsive">
                                <Link href={`/watch/${course.slug}`}>
                                    <a className="btn rating-btn">
                                        <i className="las la-play"/>
                                        {((100 * course.meta.pivot_progress) / course.duration).toPrecision(
                                            2
                                        ) === 0.0
                                            ? "Start"
                                            : `${action}`}
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </MediaQuery>
            <MediaQuery maxDeviceWidth={767}>
                <div className="col-lg-4 ">
                    <div className="card-item">
                        <div className="card-image">
                            <Link href="/watch/[slug]" as={`/watch/${course.slug}`}>
                                <a className="card__img">
                                    <img
                                        src={course.url}
                                        alt="image"
                                        style={{height: "220px"}}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div
                            className="card-content"
                            style={{width: "100%", border: "1px solid #e9ecef"}}
                        >
                            <h3 className="card__title mt-0" style={{height: "30px"}}>
                                <Link href="/watch/[slug]" as={`/watch/${course.slug}`}>
                                    <a>{course.title}</a>
                                </Link>
                            </h3>

                            <div className="course-complete-bar-2 mt-2 ">
                                <div className="progress-item ">
                                    <div className="skillbar-box">
                                          <span
                                              className="skillbar-title"
                                              style={{fontSize: "12px"}}
                                          >
                                          Complete:
                                          </span>
                                        <div
                                            className="progress"
                                            style={{height: "2px", backgroundColor: "#eee"}}
                                        >
                                            <div
                                                role="progressbar"
                                                style={{
                                                    width: `${(
                                                        (100 * course.meta.pivot_progress) /
                                                        course.duration
                                                    ).toPrecision(2)}%`,
                                                    backgroundColor: "#008da1",
                                                    opacity: ".5",
                                                }}
                                                aria-valuemin="0"
                                                aria-valuemax={course.duration}
                                            />

                                        </div>
                                    </div>
                                    <div
                                        className="skill-bar-percent"
                                        style={{fontSize: "12px"}}
                                    >
                                        {((100 * course.meta.pivot_progress) / course.duration).toPrecision(
                                            2
                                        )}
                                        %
                                    </div>
                                </div>
                            </div>

                            <Link href="/watch/[slug]" as={`/watch/${course.slug}`}>
                                <button
                                    className="btn btn-sm"
                                    style={{
                                        fontSize: "12px",
                                        marginTop: "10px",
                                        float: "right",
                                    }}
                                >
                                    <i className="las la-play"/>
                                    {((100 * course.meta.pivot_progress) / course.duration).toPrecision(
                                        2
                                    ) === 0.0
                                        ? "Start"
                                        : `${action}`}
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </MediaQuery>
            <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024}>
                <div
                    className={
                        isLandscape
                            ? "col-lg-8 mx-auto course_ipad"
                            : "col-lg-4 course_ipad"
                    }
                >
                    <div className="card-item card-list-layout">
                        <div className="card-image">
                            <Link href="/watch/[slug]" as={`/watch/${course.slug}`}>
                                <a className="card__img">
                                    <img src={course.url} alt="image"/>
                                </a>
                            </Link>
                        </div>
                        <div className="card-content responsive_card_content">
                            <h3 className="card__title">
                                <Link href="/watch/[slug]" as={`/watch/${course.slug}`}>
                                    <a style={{fontSize: "24px"}}>
                                        {course.title}
                                    </a>

                                </Link>

                            </h3>
                            <div className="course-complete-bar-2 mt-2 ">
                                <div
                                    className="progress-item "
                                    style={{marginBottom: "25px"}}
                                >
                                   <span
                                       className="skillbar-title"
                                       style={{fontSize: "24px"}}
                                   >
                                    Complete:
                                   </span>
                                    <div className="skillbar-box" style={{marginTop: "25px"}}>
                                        <div
                                            className="progress"
                                            style={{backgroundColor: "#eee"}}
                                        >
                                            <div
                                                role="progressbar"
                                                style={{
                                                    width: `${(
                                                        (100 * course.meta.pivot_progress) /
                                                        course.duration
                                                    ).toPrecision(2)}%`,
                                                    backgroundColor: "#008da1",
                                                    opacity: ".5",
                                                }}
                                                aria-valuemin="0"
                                                aria-valuemax={course.duration}
                                            />
                                        </div>
                                    </div>
                                    <div
                                        className="skill-bar-percent"
                                        style={{fontSize: "24px"}}
                                    >
                                        {((100 * course.meta.pivot_progress) / course.duration).toPrecision(2)}
                                        %
                                    </div>
                                </div>
                            </div>
                            <Link href="/watch/[slug]" as={`/watch/${course.slug}`}>
                                <button
                                    className="btn btn-sm"
                                    style={{
                                        fontSize: "24px",
                                        marginTop: "20px",
                                        float: "right",
                                    }}
                                >
                                    <i className="las la-play"/>
                                    {((100 * course.meta.pivot_progress) / course.duration).toPrecision(2) === 0.0
                                        ? "Start"
                                        : `${action}`}
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </MediaQuery>
        </>
    );
};

export default Course;
