import React, {useState} from 'react';
import Tab from "./Tab";
import AllCourse from "./AllCourse";
import CompletedCourse from "./CompletedCourse";
import WishList from "./WishList";
import Setting from "./Setting";

const StudentTabs = ({courses, courses_completed, liked_courses, user}) => {
    const [state, setData] = useState({activeTab: "all-courses"});
    const handleClick = (e) => {
        setData({
            activeTab: e.target.getAttribute("data-id"),
        });
    };
    return (
        <>
            <section className="breadcrumb-area my-courses-bread  my_courses_wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="breadcrumb-content my-courses-bread-content">
                                <div className="section-heading">
                                    <h2 className="section__title text-white">Dashboard</h2>
                                </div>
                            </div>
                            <div className="my-courses-tab">
                                <div className="section-tab section-tab-2">
                                    <ul className="nav nav-tabs" role="tablist" id="review">
                                        <Tab
                                            label="All Courses"
                                            id="all-courses"
                                            className={
                                                state.activeTab === "all-courses"
                                                    ? "active tab-hover "
                                                    : "tab-hover"
                                            }
                                            handleClick={handleClick}
                                        />
                                        <Tab
                                            label="Completed"
                                            id="completed"
                                            className={
                                                state.activeTab === "completed"
                                                    ? "active tab-hover "
                                                    : "tab-hover"
                                            }
                                            handleClick={handleClick}
                                        />
                                        <Tab
                                            label="Wishlist"
                                            id="wishlist"
                                            className={
                                                state.activeTab === "wishlist"
                                                    ? "active tab-hover "
                                                    : "tab-hover"
                                            }
                                            handleClick={handleClick}
                                        />
                                        <Tab
                                            label="Settings"
                                            id="settings"
                                            className={
                                                state.activeTab === "settings"
                                                    ? "active tab-hover "
                                                    : "tab-hover"
                                            }
                                            handleClick={handleClick}
                                        />
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {state.activeTab === "all-courses" && (
                <AllCourse courses={courses}/>
            )}
            {state.activeTab === "completed" && (
                <CompletedCourse courses={courses_completed}/>
            )}
            {state.activeTab === "wishlist" && (
                <WishList courses={liked_courses}/>
            )}
            {state.activeTab === "settings" && <Setting user={user}/>}

        </>
    );
};

export default StudentTabs;
