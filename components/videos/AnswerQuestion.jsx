import React, {useState} from "react";
import moment from "moment";
import {postQuestion} from "../../actions/student/course";
import {useDispatch} from "react-redux";
import {fetchAllQuestions} from "../../redux/actions/questionAction";
import Image from 'next/image'
import Errors from "../commons/Errors";
const AnswerQuestion = ({slug, answer}) => {
    const [show, setShow] = useState(false);
    const [state, setState] = useState({
        message: "",
        errors: null,
        parent_id: null,
        slug: null,
        success: false,
        result: null
    });
    const dispatch = useDispatch();
    const {message, parent_id, errors} = state;
    const handleSubmitReply = async (e) => {
        console.log(message, parent_id)
        e.preventDefault();
        const result = await postQuestion(slug, {body: message, parent_id: parent_id});
        if (result.errors) {
            setState({...state, errors: result.errors});
        } else {
            setState({...state, message: "", errors: null, success: true, result: null});
            setShow(false);
            dispatch(fetchAllQuestions(slug))
        }
    };
    const handleClick = (e) => {
        setState({
            ...state,
            parent_id: e.currentTarget.dataset.parent_id,
        });
        setShow(!show);
    };
    const handleChangeReply = (name) => (e) => {
        setState({...state, [name]: e.target.value});
    };

    return (
        <>
            <div className="comment">
                <div className="comment-avatar d-flex">
                    <Image
                        src="/images/team8.jpg"
                        alt="Picture of the author"
                        width={35}
                        height={35}
                        className={"comment_image"}
                    />
                    <span className={"mx-1 bold"}>{answer.user.name}</span>
                </div>
                <div className=" comment-body">
                    <p className="mx-4">{answer.body}</p>
                    <span
                        className="comment-reply"
                        data-parent_id={answer.parent_id ? answer.parent_id : answer.id}
                        onClick={handleClick}
                    >
                        <span className="la la-mail-reply" style={{fontSize:"11px"}}/>Reply
                    </span>
                    <span style={{marginLeft:"8px"}}>{moment(answer.created_at).fromNow()}</span>
                    {show && (
                        <form onSubmit={handleSubmitReply}>
                            <div style={{marginTop: "4px"}}>
                              <Errors errors={errors}/>
                            </div>
                            <div className="form-group">
                            <textarea
                                className="message-control form-control"
                                name="message"
                                value={message}
                                rows="2"
                                onChange={handleChangeReply("message")}
                            />
                            </div>

                            <button type="submit" className="btn  my-2 btn-sm" style={{backgroundColor:"#3BA6B5",color:"#ffffff"}}>
                                Reply
                            </button>
                        </form>
                    )}
                </div>
            </div>
            {answer.replies && (
                <div className="comment_reply">
                    {answer.replies.map((nestedQa) => (
                        <AnswerQuestion
                            key={nestedQa.id}
                            answer={nestedQa}
                            slug={slug}
                        />
                    ))}
                </div>
            )}
        </>
    );
};

export default AnswerQuestion;
