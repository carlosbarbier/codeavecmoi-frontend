import React, {useEffect, useState} from "react";
import MediaQuery from "react-responsive";
import AnswerQuestion from "./AnswerQuestion";
import Rating from "react-rating";
import {getAllQuestionsAndAnswers, postQuestion, rateCourse} from "../../actions/student/course";
import {useSelector} from "react-redux";

const Questions = ({slug,student}) => {
    const [show, setShow] = useState(false);
    const [showRatingForm, setShowRatingForm] = useState(false);
    const [questions_answers, setQuestionsAnswers] = useState([]);
    const [rating, setRating] = useState(0);
    const [rating_comment, setRatingComment] = useState("");
    const payload = useSelector(state => state.questions)

    const [state, setState] = useState({
        message: "",
        errors: null,
        success: false,
        rate: false,
        rate_value: 0,
        result:null
    });

    const {message, errors, rate, rate_value, result} = state;
    useEffect(() => {
      getAllQuestionsAndAnswers(slug).then(result=>{
         setQuestionsAnswers(result)
      }).catch(e=>{

      })
    }, [payload,result])

    const handleSubmit = async (e) => {
        e.preventDefault();
        const result = await postQuestion(slug,{body: message});
        if (result.errors) {
            setState({...state, errors: result.errors});
        } else {
            setState({...state, message: "", errors: null, success: true, result});
            setShow(false);
        }
    };

    const handleSubmitRating = async (e) => {
        e.preventDefault();
        const result = await rateCourse( slug,{comment_body:rating_comment,rating});
        if (result.errors) {
            setState({...state, errors: result.errors});
        } else {
            setState({...state, message: "", errors: null, success: true, result});
        }
    };

    const handleClickButton = () => {
        setShowRatingForm(false)
        setShow(!show);
    };

    const handleChange = (name) => (e) => {
        setState({...state, [name]: e.target.value});
    };
    const handleRatingComment = (e) => {
        setRatingComment(e.target.value)
    };
    const handleRateCourse = async (value) => {
        setShowRatingForm(true)
        setShow(false)
        setRating(value)
    };
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-9">
                    <div className="question-overview-result-wrap">
                        <div className="lecture-overview-item mb-0">
                            <div
                                className="question-overview-result-header d-flex align-items-center justify-content-between pb-3">
                                <div className="question-result-item">
                                    <h3 className="widget-title font-size-17">

                                        {questions_answers && (questions_answers.length === 1 ? "One" : questions_answers.length)} {""}
                                        question
                                        {questions_answers && questions_answers.length === 1 ? "" : "s"} in this course
                                    </h3>
                                </div>
                                <div className="question-result-item">
                                    <span>{student.rating > 0 || rate_value > 0 ? "Your rate" : "Rate the course"} </span>
                                    <span
                                        className="btn"
                                        style={{border: "none", backgroundColor: "white"}}
                                    >
                                      <Rating

                                          emptySymbol={<i className="las la-star"/>}
                                          fullSymbol={
                                              <i className="las la-star" style={{color: "red"}}/>
                                          }
                                          start={0}
                                          stop={5}
                                          step={1}
                                          initialRating={
                                              student.rating > 0 ? student.rating : 0 || rating > 0 ? rating : 0
                                          }
                                          readonly={
                                              student.rating> 0 || rate === true
                                          }
                                          onClick={handleRateCourse}
                                      />
                                    </span>

                                    <span
                                        className="btn ask-new-question-btn"
                                        onClick={handleClickButton}
                                        style={{marginLeft: "28px"}}
                                    >
                                        Ask a new question
                                      </span>
                                </div>
                            </div>
                            <div className="section-block"/>

                            <div style={{marginTop: "15px"}}>
                                {(errors || []).map((error, index) => (
                                    <p key={index} style={{color: "red"}}>
                                        {error.message}
                                    </p>
                                ))}
                            </div>
                            <div>
                                {show && (
                                    <form onSubmit={handleSubmit} style={{marginTop: "15px"}}>
                                        <div className="form-group">
                                            <MediaQuery minDeviceWidth={1224}>
                                                <textarea
                                                    className="message-control form-control"
                                                    name="message"
                                                    value={message}
                                                    rows="6"
                                                    onChange={handleChange("message")}
                                                />
                                            </MediaQuery>
                                            <MediaQuery maxDeviceWidth={764}>
                                                <textarea
                                                    className="message-control form-control"
                                                    name="message"
                                                    value={message}
                                                    rows="3"
                                                    onChange={handleChange("message")}
                                                />
                                            </MediaQuery>
                                            <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024}>
                                                <textarea
                                                    className="message-control form-control"
                                                    name="message"
                                                    value={message}
                                                    rows="4"
                                                    onChange={handleChange("message")}
                                                />
                                            </MediaQuery>
                                        </div>
                                        <button type="submit" className="btn btn-dark btn-submit">
                                            Add Your message
                                        </button>
                                    </form>
                                )}
                                {showRatingForm && (
                                    <form onSubmit={handleSubmitRating} style={{marginTop: "15px"}}>
                                        <div className="form-group">
                                            <MediaQuery minDeviceWidth={1224}>
                                                <textarea
                                                    placeholder="Enter your review here"
                                                    className="message-control form-control"
                                                    name="rating_comment"
                                                    value={rating_comment}
                                                    rows="4"
                                                    onChange={handleRatingComment}
                                                />
                                            </MediaQuery>
                                            <MediaQuery maxDeviceWidth={764}>
                                                <textarea
                                                    className="message-control form-control"
                                                    name="message"
                                                    value={message}
                                                    rows="3"
                                                    onChange={handleChange("message")}
                                                />
                                            </MediaQuery>
                                            <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024}>
                                                <textarea

                                                    className="message-control form-control"
                                                    name="message"
                                                    value={message}
                                                    rows="3"
                                                    onChange={handleChange("message")}
                                                />
                                            </MediaQuery>
                                        </div>
                                        <button type="submit" className="btn btn-dark btn-submit">
                                            Save your review
                                        </button>
                                    </form>
                                )}
                            </div>
                        </div>
                        <div className="lecture-overview-item mt-0 mx-4">
                            <div className="question-list-container">
                                <div className="question-list-item">
                                    <div className="comments-list">
                                        {questions_answers &&
                                        questions_answers.map(answer => (
                                            <AnswerQuestion
                                                answer={answer}
                                                key={answer.id}
                                                slug={slug}
                                                id={answer.id}
                                            />
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Questions;
