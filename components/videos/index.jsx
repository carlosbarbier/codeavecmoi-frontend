import React, {useEffect, useRef, useState} from "react";
import ReactPlayer from "react-player";
import {secondsMs} from "../../actions/util";
import {finishToWatchVideo, markedCourseAsCompleted} from "../../actions/student/course";

const VideoPlayer = ({course, ids}) => {
    const {videos, title} = course;
    const [state, setState] = useState({
        url: null,
        playing: false,
        video: false,
        id: videos[0].id,
        active: "",
        ended: false,
        hasNext: true,
        videosAlreadyWatchId: [],
    });

    useEffect(() => {
        let array = [];
        ids.map((video_id) => {
            array.push(video_id.id);
        });
        setState({
            ...state,
            videosAlreadyWatchId: array,
            url: videos[0].url,
        });
    }, []);
    const {url, playing, id, hasNext, videosAlreadyWatchId, active} = state;
    const ref = useRef(null);
    const findNextVideo = (id) => {
        const index = videos.findIndex((element) => element.id === id);
        if (videos.length === index + 1) {
            setState({...state, hasNext: false});
            return videos[index];
        } else {
            return videos[index + 1];
        }
    };
    const handleToWatch = (e) => {
        const id = e.currentTarget.dataset.id;
        const url = e.currentTarget.dataset.url;
        setState({
            ...state,
            url,
            id,
            playing: true,
        });
    };
    const handleEnded = async () => {
        const video_id = parseInt(id);
        const video = findNextVideo(id);
        if (!videosAlreadyWatchId.includes(video_id)) {
            let array = videosAlreadyWatchId;
            array.push(video_id);
            setState({
                ...state,
                videosAlreadyWatchId: array,
            });
            await finishToWatchVideo(course.slug, {video_id});
        }
        if (videos.length === videosAlreadyWatchId.length && course.students[0].is_completed === 0) {
            await markedCourseAsCompleted(course.slug);
        }

        if (hasNext) {
            setTimeout(() => {
                setState({
                    ...state,
                    id: video.id,
                    url: video.url,
                    playing: true,
                });
            }, 3000);
        }
    };
    return (
        <div className="row d-flex video-wrapper">
            <div className="col-lg-9 col-sm-12">
                <div className="player-wrapper">
                    <ReactPlayer ref={ref}
                                 className="react-player"
                                 url={url}
                                 width="100%"
                                 height="100%"
                                 controls={true}
                                 muted={false}
                                 playing={playing}
                                 onEnded={handleEnded}
                                 onError={() => console.log("Error")}

                    />
                </div>

            </div>

            <div className="col-lg-3 col-sm-12">

                <div className="sidebar-content">
                    <div className="card-header content-title">{title}</div>

                    {videos && (
                        <>
                            {videos.map((video) => (
                                <div
                                    key={video.id}
                                    onClick={handleToWatch}
                                    data-url={video.url}
                                    data-id={video.id}
                                >
                                    <div className="card-body">
                                        <div className="course-content-list">
                                            <ul className="course-list">
                                                <li
                                                    className={
                                                        parseInt(id) === video.id
                                                            ? "course-item-link video_active"
                                                            : "course-item-link"
                                                    }
                                                >
                                                    <div className="course-item-content-wrap">
                                                        <div className="course-item-content">
                                                            <h4 className="widget-title font-size-15 font-weight-medium">
                                                                {video.title}
                                                            </h4>
                                                            <div className="courser-item-meta-wrap">
                                                                <div className="course-item-meta ">
                                                                    <i
                                                                        className={
                                                                            parseInt(id) === video.id
                                                                                ? "las la-play-circle playing-icon"
                                                                                : "las la-play-circle"
                                                                        }
                                                                    />
                                                                    {secondsMs(video.duration)}
                                                                </div>
                                                                <div className="video-completed">
                                                                    <i
                                                                        className={
                                                                            videosAlreadyWatchId.includes(
                                                                                video.id
                                                                            )
                                                                                ? "las la-check"
                                                                                : ""
                                                                        }
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </>
                    )}
                </div>
            </div>
        </div>
    );
};

export default VideoPlayer;
